#!/bin/sh
#
#Elaborado por Alejandro Rico E. Beta 11/AUG/2017
#///
#set -xv
function sqlrun(){
#Inicio de funcion
#Funcion para extraer datos por registro o ejecutar una consulta a un host en especifico
#Datos del host que tiene la base de datos para los respaldos-----------
host="162.252.87.133" #Si es global cambiar por IP de Desarrollo
userx="fkron"
pap="x1l0f0n0"
db_x="olog_x"
cim="'"
#-----------------------------------------------------------------------
#Recepcion de parametro de funcion
sql=$1
zi=`mysql --host=${host} --user=${userx} --password=${pap} ${db_x} -A<< END 
$sql
END`
if [ $? -eq 0 ] ; then
	echo $zi
else
	echo "Error en la consulta: $zi : $sql"
fi
#fin de funcion
}
#------------------------------------------------------------------------- Main program --------------------------------------------------------------------------+
while :
do 
	hrx=`date +'%Y-%m-%d%H:%M'` #manda la hora en la que se ejecuta el chequeo 
	#Centos6.X
	ip_loc=`ifconfig ${iface} | grep 'Mask:255.255.255' | grep 'inet' | cut -d: -f2 | cut -d " " -f1 | grep -v 127` #obtener ips del server
	ipck=($ip_loc)
	#Centos7.X
	#ip_loc=`ifconfig ${iface} | grep 'netmask 255.255.255' | grep 'inet' | cut -d "t" -f2 | cut -d "n" -f1`
	#ip_loc=${ip_loc//[[:space:]]}
	#ipck=($ip_loc)
	x=0
	for ips in ${ipck[*]} #Obtiene ip y las checa todas para ver concurrencias dentro de la base
		do
			x=$(( $x + 1 )) #Solo para checar que si verifica todas las ips del servidor
			echo "ip: $ips , $x"
			a1=$(sqlrun "Select idserv as 'X' from relup where ipserv = '$ips';")	#Extrae ID de las coincidencias de la base de datos 
			a2=$(echo `expr length "$a1"`)
			a3=${a1:2:a2}  
			ar_sql=($a3) #Llena arreglo con ID del resultado de la consulta
			for item in ${ar_sql[*]} #Checa todos los ids que se encuentran en la base de datos segun la ip del servidor
				do
					echo "Arreglo crel.sh: $item $hrx"
					nohup /utl/relbin/rinfo.sh envio "$item" "$hrx" & #Envia los datos obtenidos para ejecutar los respaldos
			done
	done
sleep 60 #Esto tarda en checar los respaldos, si se modifica corre el peligro de que se ejecute varias veces -->> posiblemente...
done

