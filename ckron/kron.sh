#!/bin/sh
#
#Elaborado por Alejandro Rico E. 2017
#///zzzzz
#set -xv
#---------------------------------------------------------------- Variables Globales ------------------------------------------------------------------+
dM=`date +'%a'`
dm=`date +'%A'`
DY=(L M Mi J V S D) #Array de dias de la semana
dy=(l,m,mi,j,v,s,d) #Array de dias en minusculas por si...-
chh=`date +'%Y%m%d-%H%M%S'`
spin='▄■▀■▄'
#------------------------------------------------------------------------ Funciones ------------------------------------------------------------------+
#-----------------------------------------------------------------------------------------------------------------------------------------------------+

function headx()
{
	#Funcion para crear Promt segun el tipo de aviso
	#inicio de funcion
	dat=$2
	tp=$1
#Colores de texto
	red=`tput setaf 1`
	greenb=`tput setab 2`
    green=`tput setaf 2`
    yellow=`tput setaf 3`
    blue=`tput setaf 4`
    magenta=`tput setaf 5`
    cyan=`tput setaf 6`
    white=`tput setaf 7`
    reset=`tput sgr0`
    alert=`tput bel`
#fin de colores	
	if [ "$tp" == "#" ] ; then
			echo "${green}  #####################################################################################${reset}"
			echo "${yellow}#--[]-> ${green}    $dat ${reset}"
			echo "${green}  #####################################################################################${reset}"
		elif [ "$tp" == "@" ] ; then
			echo "${white}#--[]-> ${cyan}$dat ${reset}"	
		elif [ "$tp" == "&" ] ; then
			echo "${alert}${red}${greenb}#--[]-> $dat ${reset}"	
		else
			echo "${yellow}## --[]-> # $dat ${reset}"
	fi		             
#Fin de funcion	
}

function sqlrun(){
#Inicio de funcion
#Funcion para extraer datos por registro o ejecutar una consulta a un host en especifico
#Datos del host que tiene la base de datos para los respaldos-----------
host="162.252.87.133" #Si es global cambiar por IP de Desarrollo
userx="fkron"
pap="x1l0f0n0"
db_x="olog_x"
#-----------------------------------------------------------------------
#Recepcion de parametro de funcion
sql=$1
zi=`mysql --host=${host} --user=${userx} --password=${pap} ${db_x} -A<< END 
$sql
END`
if [ $? -eq 0 ] ; then
	echo $zi
else
	headx "&" "Error en la consulta: $wcm"
fi
#fin de funcion
}


#Nuevo comprimir mysqldump -u root -pp0rtali70s olog_x | 7z a -si backup.sql.7z
#Nuevo Descomprimir 7z e -so  backup.sql.7z | mysql --host=localhost -u root -pp0rtali70s ologx_7z
#Directorios
#Respaldos
function dbseek(){
	#Inicio de funcion
	#dbseek host usuario password db_name
	seekh=$1
	seeku=$2
	seekp=$3
	seekdb=$4
#funciona para validar existencia de DB segun host
seek=`mysql --host=$seekh --user=$seeku --password=$seekp << END
 use $seekdb -A;
END` 
if [ $? -eq 0 ] ;
then
	echo 0
else
	echo 1
fi

#fin de funcion
}
function slog(){
#Guarda log en utl
#us: slog id_reg "Mensaje"
inx="Usuario:[ Root ]F_Cambio:[ $chh ] -->"
finx="<br>"
sid=$1
deb=$2
preh=$sz3
sqlrun "update bkkron set history = '$preh$finx$inx$deb$finx' where idserv='$sid';"
libre $sid #libera registro
}

function usado(){
#Bloquea registro para que no se utilice asta que se libere por el sistema
#us: slog id_reg "Mensaje"
sid=$1
sqlrun "update bkkron set job = '1' where idserv='$sid';"
}

function libre(){
#Bloquea registro para que no se utilice asta que se libere por el sistema
#us: slog id_reg "Mensaje"
sid=$1
sqlrun "update bkkron set job = '0' where idserv='$sid';"
}

function unoxmes(){	
#unoxmes Archivo con directorio "opcion db o od" "erp o base" "directorio de respaldo"
#unoxmes "$ddbr$rzdb" "db" "$db" "$dresp"
#inicio de funcion
#funcion para crear respaldo por mes durante todo el año
#Variables
sfile=$1
tipo=$2
erp=$3
dirgen=$4
ydirdb=$dirgen
ydircd=$dirgen
hoy=`date +'%d'`
nxd=`date -d "tomorrow"`
yr=`date +'%Y'`
yr=$tipo"_"$yr"_"$erp
mm=`date +'%B'`
nxd=${nxd:8:2}
if [ $nxd == " 1" ] || [ $nxd == "1" ];
then
	headx "#" "Mañana es primero de mes $nxd"
	echo "Tipo: $tipo"
#respaldo de codigo
	if [ $tipo == "cd" ];
	then
		cd $ydirdb
		if [ -d "$yr" ];
		then
		        headx "#" "Se encontro directorio anual $ydirdb$yr"
		     else
		     	headx "&" "No se encontro directorio anual, se procede a crear directorio anual $ydirdb$yr"
		     	mkdir "$yr"
		fi	
		if [ -d $yr ];
		then
				rfile=$erp"_"$mm".7z"
		        headx "@" "Se encontro directorio anual $ydirdb$yr"
		        cd $yr
		        cp -a "$sfile" $rfile
		        	if [ -f $rfile ];
					then
						headx "@" "Se copio $sfile correctamente;"
						headx "@" "ahora es $ydirdb$yr/$rfile"
						#savedb "BackupXmes correcto $sfile" "Todo correcto directorio: $ydirdb$yr archivo: $rfile" "20"
						slog $idglob "BackupXmes correcto $sfile Todo correcto directorio: $ydirdb$yr archivo: $rfile"
						#esto es para que no se borre el directorio cuando sea 2060 si se va a borrar					
						headx "@" "Archivo a subir sftp Remoto -> $rfile "
						# Envio de archivos de respaldo xftp
						if curl --fail -u $mz3 -T $rfile $lz3; then 
							headx "@" "Se subio archivo remoto: $rfile fecha:$chh"
						else
							headx "@" "Error al subir archivo remoto $rfile Fecha: $chh"
						fi
					else
						headx "&" "-[]-> Error"
						#savedb "BackupXmes Error $sfile" "Error al ingresar o grabar en $ydirdb$yr " "Error General" "21"
						slog $idglob "BackupXmes Error $sfile Error al ingresar o grabar en $ydirdb$yr Error General"
					fi 
		else
		     	headx "&" "Se encontro un problema en el directorio $ydirdb$yr"
		     	#savedb "BackupXmes Error $sfile" "Error al ingresar a $ydirdb$yr " "Error General" "21"
		     	slog $idglob "BackupXmes Error $sfile Error al ingresar a $ydirdb$yr Error General"
		fi
	fi	

#respaldo de base de datos
	if [ $tipo == "db" ];
	then
		cd $ydirdb
		if [ -d $yr ];
		then
		        headx "#" "Se encontro directorio anual $ydirdb$yr"
		     else
		     	headx "&" "No se encontro directorio anual, se procede a crear directorio anual $ydirdb$yr"
		     	mkdir "$yr"
		fi	
		if [ -d $yr ];
		then
				rfile=$erp"_"$mm".7z"
		        headx "@" "Se encontro directorio anual $ydirdb$yr"
		        cd $yr
		        cp -a "$sfile" $rfile
		        	if [ -f $rfile ];
					then
						headx "@" "Se copio $sfile correctamente;"
						headx "@" "ahora es $ydirdb$yr/$rfile"
						savedb "BackupXmes correcto $sfile" "Todo correcto directorio: $ydirdb$yr archivo: $rfile" "20"
						#esto es para que no se borre el directorio cuando sea 2060 si se va a borrar					
						headx "@" "Archivo a subir sftp Remoto -> $rfile "
						# Envio de archivos de respaldo xftp
						if curl --fail -u $mz3 -T $rfile $lz3; then 
							headx "@" "Se subio archivo remoto: $rfile fecha:$chh"
						else
							headx "@" "Error al subir archivo remoto $rfile Fecha: $chh"
						fi
					else
						headx "&" "-[]-> Error"
						savedb "BackupXmes Error $sfile" "Error al ingresar o grabar en $ydirdb$yr " "Error General" "21"
					fi 
		else
		     	headx "&" "Se encontro un problema en el directorio $ydirdb$yr"
		     	savedb "BackupXmes Error $sfile" "Error al ingresar a $ydirdb$yr " "Error General" "21"
		fi
	fi	 
else 
	headx "&" "Mañana no es primero de mes es: $nxd"
fi 
#fin de funcion
}

#--------------------------------------------------------------------- Datos de DB olog_x --------------------------------------------------------------+
function bootx(){
#set -x
headx "@" "-> Boot"
idsv=$1
#Ocupamos ---> 
#ipserv = Ip del servidor
#bkxdiaNOM = Dias de respaldo 
#uprod = Respaldo de produccion activo
#dbprodnom = Nombre de la base de datos de produccion
#ucap = Update de produccion a CAPA activado
#dbcapnom = Nombre de la base de datos de CAPA
#udes = Update de base de desarrollo Activa 
#dbdesnom = Nombre de base de datos de Desarrollo
# Dias de actualizacion (* Diario- 1->7 1= Lunes->Domingo)
#seldcap = dia o dias de actiualizacion de CAPA * diario
#selddes = dia de actualizacion de Desarrollo
#
#----------------------- Extraxcion de datos 
ipu=$(sqlrun "Select ipserv as 'X' from bkkron where idserv = '$idsv';")
ipc=$(echo `expr length "$ipu"`)
ipa=${ipu:2:ipc} 
#headx "@" "$ipa"

#--Base de producion

b1=$(sqlrun "Select uprod as 'X' from bkkron where idserv = '$idsv';") 	#Si esta activo el respaldo
b2=$(echo `expr length "$b1"`)
b3=${b1:2:b2}  
#headx "@" "$b3"

c1=$(sqlrun "Select dbprodnom as 'X' from bkkron where idserv = '$idsv';") #Nombre de la base de datos Produccion
c2=$(echo `expr length "$c1"`)
c3=${c1:2:c2}  
#headx "@" "$c3"

a1=$(sqlrun "Select bkxdiaNOM as 'X' from bkkron where idserv = '$idsv';") #Nombre de los dias de respaldo
a2=$(echo `expr length "$a1"`)
a3=${a1:2:a2}  
#headx "@" "$a3"

j1=$(sqlrun "Select timerdia as 'X' from bkkron where idserv = '$idsv';")  #Hora para respaldar
j2=$(echo `expr length "$j1"`)
j3=${j1:2:j2}
j3=${j1:2:5}
#headx "@" "$j3"

jx1=$(sqlrun "Select ipserv as 'X' from bkkron where idserv = '$idsv';")  #Ip del servidor para meter respaldo
jx2=$(echo `expr length "$jx1"`)
jx3=${jx1:2:jx2}  
#headx "@" "$j3"

#--------------------
#----Base de Capa

d1=$(sqlrun "Select ucap as 'X' from bkkron where idserv = '$idsv';")		 #Si esta activo el respaldo
d2=$(echo `expr length "$d1"`)
d3=${d1:2:d2}  
#headx "@" "$d3"

e1=$(sqlrun "Select dbcapanom as 'X' from bkkron where idserv = '$idsv';") #Nombre de la base de datos CAPA
e2=$(echo `expr length "$e1"`)
e3=${e1:2:e2}  
#headx "@" "$e3"

h1=$(sqlrun "Select seldcap as 'X' from bkkron where idserv = '$idsv';")	 #Nombre de los dias de respaldo
h2=$(echo `expr length "$h1"`)
h3=${h1:2:h2}  
#headx "@" "$h3"

k1=$(sqlrun "Select ticapa as 'X' from bkkron where idserv = '$idsv';")  	 #Hora para respaldar
k2=$(echo `expr length "$k1"`)
k3=${k1:2:k2} 
k3=${k1:2:5}
#headx "@" "$k3"

#-----------------------
#----Base de Desarrollo

f1=$(sqlrun "Select udes as 'X' from bkkron where idserv = '$idsv';")		#Si esta activo el respaldo
f2=$(echo `expr length "$f1"`)
f3=${f1:2:f2}  
#headx "@" "$f3"

g1=$(sqlrun "Select dbdesnom as 'X' from bkkron where idserv = '$idsv';")	#Nombre de la base de datos Desarrollo
g2=$(echo `expr length "$g1"`)
g3=${g1:2:g2}  
#headx "@" "$g3"

i1=$(sqlrun "Select selddes as 'X' from bkkron where idserv = '$idsv';")	#Nombre de los dias de respaldo
i2=$(echo `expr length "$i1"`)
i3=${i1:2:i2}  
#headx "@" "$i3"

l1=$(sqlrun "Select tides as 'X' from bkkron where idserv = '$idsv';")  #Hora para respaldar
l2=$(echo `expr length "$l1"`)
l3=${l1:2:l2}
l3=${l1:2:5}
#headx "@" "$i3"

#----Datos extra

z1=$(sqlrun "Select bkcod as 'X' from bkkron where idserv = '$idsv';")	#Activacion de respaldo para codigo 
z2=$(echo `expr length "$z1"`)
z3=${z1:2:z2}  
#headx "@" "$f3"

x1=$(sqlrun "Select ticod as 'X' from bkkron where idserv = '$idsv';")	#horario para respaldar codigo
x2=$(echo `expr length "$x1"`)
x3=${x1:2:x2} 
x3=${x1:2:5}
#headx "@" "$g3"

az1=$(sqlrun "Select lcodigo as 'X' from bkkron where idserv = '$idsv';")	#carpeta (Linea) de codigo
az2=$(echo `expr length "$az1"`)
az3=${az1:2:az2}  
#headx "@" "$i3"

bz1=$(sqlrun "Select bkxmes as 'X' from bkkron where idserv = '$idsv';")  #respaldo por mes
bz2=$(echo `expr length "$bz1"`)
bz3=${bz1:2:bz2} 
#headx "@" "$i3"

cz1=$(sqlrun "Select tixmes as 'X' from bkkron where idserv = '$idsv';")	#tiempo para respaldo x mes
cz2=$(echo `expr length "$cz1"`)
cz3=${cz1:2:cz2} 
cz3=${cz1:2:5}
#headx "@" "$f3"

dz1=$(sqlrun "Select bkrota as 'X' from bkkron where idserv = '$idsv';")	#Activacion de rotacion de respaldos
dz2=$(echo `expr length "$dz1"`)
dz3=${dz1:2:dz2}  
#headx "@" "$g3"

ez1=$(sqlrun "Select bkdir as 'X' from bkkron where idserv = '$idsv';")	#Directorio para respaldos
ez2=$(echo `expr length "$ez1"`)
ez3=${ez1:2:ez2}  
#headx "@" "$i3"

fz1=$(sqlrun "Select bkftps as 'X' from bkkron where idserv = '$idsv';")  #Activar envio por ftp
fz2=$(echo `expr length "$fz1"`)
fz3=${fz1:2:fz2} 
#headx "@" "$i3"

gz1=$(sqlrun "Select bkssh as 'X' from bkkron where idserv = '$idsv';")	#Activar envio por ssh
gz2=$(echo `expr length "$gz1"`)
gz3=${gz1:2:gz2}  
#headx "@" "$f3"

hz1=$(sqlrun "Select ddshtp as 'X' from bkkron where idserv = '$idsv';")	#url/directorio para envio remoto
hz2=$(echo `expr length "$hz1"`)
hz3=${hz1:2:hz2}  
#headx "@" "$g3"

iz1=$(sqlrun "Select ddsftp as 'X' from bkkron where idserv = '$idsv';")	#url/Direccion para sftp
iz2=$(echo `expr length "$iz1"`)
iz3=${iz1:2:iz2}  
#headx "@" "$i3"

jz1=$(sqlrun "Select rotbk as 'X' from bkkron where idserv = '$idsv';")	#Numero de Rotacion de respaldos #Warning----> si se cambia [Del=<!>Nfile-Dif{R}=Del>M-antiguos]
jz2=$(echo `expr length "$jz1"`)
jz3=${jz1:2:jz2}  
#headx "@" "$jz3"

kz1=$(sqlrun "Select up_ftp as 'X' from bkkron where idserv = '$idsv';")	#Usuario y contraseña para subir por sftp
kz2=$(echo `expr length "$kz1"`)
kz3=${kz1:2:kz2}  
#headx "@" "$kz3"

lz1=$(sqlrun "Select pxdir as 'X' from bkkron where idserv = '$idsv';")	#Directorio de respaldos remotos
lz2=$(echo `expr length "$lz1"`)
lz3=${lz1:2:lz2}  
#headx "@" "$lz3"

mz1=$(sqlrun "Select pxxu_p as 'X' from bkkron where idserv = '$idsv';")	#Usuario y contraseña para subir por sftp respaldos remotos
mz2=$(echo `expr length "$kz1"`)
mz3=${mz1:2:mz2}  
#headx "@" "$mz3"

nz1=$(sqlrun "Select wwwdir as 'X' from bkkron where idserv = '$idsv';")	#Directorio web para lineas de codigo
nz2=$(echo `expr length "$nz1"`)
nz3=${nz1:2:nz2}  
#headx "@" "$mz3"


oz1=$(sqlrun "Select us_bk 'X' from bkkron where idserv = '$idsv';")	#Usuario para respaldos //Verificar que el usuario exista en Mysql
oz2=$(echo `expr length "$oz1"`)
oz3=${oz1:2:oz2}  
#headx "@" "$oz3"

pz1=$(sqlrun "Select pass_bk as 'X' from bkkron where idserv = '$idsv';")	#Password para respaldos //Verificar que el usuario exista en Mysql
pz2=$(echo `expr length "$pz1"`)
pz3=${pz1:2:pz2}  
#headx "@" "$pz3"

qz1=$(sqlrun "Select noresp as 'X' from bkkron where idserv = '$idsv';")	#patron para no excluir del respaldo
qz2=$(echo `expr length "$qz1"`)
qz3=${qz1:2:qz2}  
#headx "@" "$qz3"

rz1=$(sqlrun "Select sdesip as 'X' from bkkron where idserv = '$idsv';")	#IP de servidor de desarrollo para envio de updates
rz2=$(echo `expr length "$rz1"`)
rz3=${rz1:2:rz2}  
#headx "@" "$rz3"

sz1=$(sqlrun "Select history as 'X' from bkkron where idserv = '$idsv';")	#IP de servidor de desarrollo para envio de updates
sz2=$(echo `expr length "$sz1"`)
sz3=${sz1:2:sz2}  
#headx "@" "$rz3"

tz1=$(sqlrun "Select job as 'X' from bkkron where idserv = '$idsv';")	#IP de servidor de desarrollo para envio de updates
tz2=$(echo `expr length "$tz1"`)
tz3=${tz1:2:tz2}  
#headx "@" "$rz3"
#set +x
}

#-------------------------------------------------------------------------------------------------------------------------------------------------------+
#-------------------------------------------------------------------------------------------------------------------------------------------------------+
#-------------------------------------------------------------------------------------------------------------------------------------------------------+
#---------------------------------------------------Funciones ejecutivas dentro del core -> kron -------------------------------------------------------+
#--------------------------------------------------------%%%%%%%%%%-------------------------------------------------------------------------------------+
#-------------------------------------------------------------%%%%%%%%%---------------------------------------------------------------------------------+
#-------------------------------------------------------------------%%%%%%$-----------------------------------------------------------------------------+


#------------------------------------------------------------ Volcado de respaldos ---------------------------------------------------------------------+
function volc_resp(){	
#inicio de funcion
#orangebox  id_serv
#------------------Carga de datos de la Base de datos
	#bootx $1 
#------------------Fin de carga de Datos DB
#set -x
db_t=$2 #base de datos para actualizar contenido
u_db=$oz3 #Usuario para respaldos
p_db=$pz3 #Password para respaldos
db_prod=$c3 #Nombre de base de datos produccion
vhost=$jx3
dhost=$rz3
r_dir=$ez3"/Rot_$db_prod/" #Directorio de respaldos de Bases de datos
comodin="dbbkc_"$db_prod"*"
infe=`date +'%Y%m%d-%H%M%S'`
if [ "$db_t" == "$db_prod" ] ; then #comprueba que la base de produccion no sea la que se pone a restaurar...>>>
	headx "&" "No se puede o no se debe, actualizar la base de produccion favor de verificar."
	slog $idglob "No se puede o no se debe, actualizar la base de produccion [$db_prod], favor de verificar. DB configurada [$db_t]"
else	
	#------------------Carga de variables
	#Busca el archivo mas actual de los respaldos para actualizar bases dbbkc_olog_x*
	u_file=`find $r_dir$comodin  -type f | sort -r | head -n1` 
	echo "Respaldo tomado para Capa o Desarrollo: $u_file"
	#set +x
	if [ -f $u_file ] ; then
		if [[ "$db_t" == *"_DES"*  ]] ; then # comprobacion de la base desarrollo exista en desarrollo
			vfx=$(dbseek $dhost $u_db  $p_db $db_t) #Busca la base de datos en el servidor de desarrollo para aplicar el respaldo
			if [ $vfx -eq 0 ];then
				headx "@" "se encontro base $db_t y es de desarrollo"
					if [[ "$db_t" == *"_DES"*  ]] ; then 
						#Si la base tiene terminacion _DES se envia al servidor de desarrollo el update <<<<------
						headx "@" "Update de desarrollo: $db_t"
						#se aplica update de desarrollo en servidor de desarrollo [Probar tiempos]
						u_m=`7z e -so  $u_file | mysql --host=$dhost -u $u_db -p$p_db $db_t`
						if [ $? -eq 0 ] ; then
							infi=`date +'%Y%m%d-%H%M%S'`
							slog $idglob "Se aplico update de $u_file  a la base $db_t desarrollo en servidor $dhost -> $u_m  <Inicio=$infe{}Fin=$infi>"  
							headx "@" "Se aplico el update 7z $u_file -> $u_m"
						else
							slog $idglob "No se aplico update de $u_file a la base $db_t desarrollo --> Error $dhost -> $u_m"  
							headx "@" "No se aplico el update 7z $u_file -> $u_m"
						fi
					fi
			else
				slog $idglob "No se aplico update de $u_file a la base $db_t Desarrollo --> Error $vhost"  
				headx "@" "No se aplico el update 7z $u_file"
			fi
		fi
		
		if [[ "$db_t" == *"_CAPA"*  ]] ; then #comprobacion de la base CAPA exista en home
			vfx=$(dbseek $vhost $u_db  $p_db $db_t) #Busca la base de datos en home para aplicar el respaldo
			if [ $vfx -eq 0 ];then
				headx "@" "se encontro base $db_t y es de CAPA"
					if [[ "$db_t" == *"_CAPA"*  ]] ; then 
						headx "@" "Update de CAPA: $db_t"
						#se aplica update de capa
						u_m=`7z e -so  $u_file | mysql --host=$vhost -u $u_db -p$p_db $db_t` 
						if [ $? -eq 0 ] ; then
							infi=`date +'%Y%m%d-%H%M%S'`
							slog $idglob "Se aplico update de $u_file  a la base $db_t CAPA en servidor $vhost -> $u_m  <Inicio=$infe{}Fin=$infi>"  
							headx "@" "Se aplico el update 7z $u_file -> $u_m"
						else
							slog $idglob "No se aplico update de $u_file a la base $db_t CAPA --> Error $vhost -> $u_m"  
							headx "@" "No se aplico el update 7z $u_file -> $u_m"
						fi
					fi
			else
				slog $idglob "No se aplico update de $u_file a la base $db_t CAPA --> Error $vhost"  
				headx "@" "No se aplico el update 7z $u_file"
			fi
		fi
		
		if [[ "$db_t" == *"_CAPA"*  ]] ; then 
		  echo "update $db_t.companies set coyname=concat('EMPRESA CAPA - ',coyname) "| mysql --user=$u_db --password=$p_db  $db_t -A
		  echo "update $db_t.config set confvalue='FAKE' where confname='Timbrador' "| mysql --user=$u_db --password=$p_db $db_t -A 
		fi
		if [[ "$db_t" == *"_DES"*  ]] ; then
		  #funcion de update de base de datos a desarrollo
		  echo "update $db_t.companies set coyname=concat('EMPRESA DESARROLLO - ',coyname) "| mysql --host=$dhost --user=$u_db --password=$p_db  $db_t -A
		  echo "update $db_t.config set confvalue='FAKE' where confname='Timbrador' "| mysql --host=$dhost --user=$u_db --password=$p_db $db_t -A 
		fi
	else
		headx "&" "Error favor de verificar archivo 7z .$u_file"
		slog $idglob "Error no se aplico update de $db_t, No se encontro Archivo $u_file"
	fi
fi #Sale si la base a actualizar es la misma de produccion
#fin de funcion
}

#------------------------------------------------------------ Creaciones de respaldos ----------------------------------------------------------------+
function crea_resp(){
#Parametros crea_resp id N_db
#Inicio de funcion
#Carga de datos de la Base de datos
	bootx $1 
#Fin de carga de Datos DB
#Carga de variables
#set -x
	db=$2 #Base de datos
	udb=$oz3 #Usuario para respaldos
	pdb=$pz3 #Password para respaldos
	ddbr=$ez3 #Directorio de respaldos	
	sftp=$fz3 #activacion de respaldo por sftp
	sssh=$gz3 #activacion de respaldo por sssh
	dsftp=$iz3 #directorio para subir sftp
	dssh=$hz3 #directorio para subir dssh
	rzdb="dbbkc_$db""_${chh}"".7z"
	svresp=$jz3 #Numero de archivos para rotar
	frota=$dz3 #Activacion de rotacion de archivos
	ftpu_p=$kz3 #directorio para subir por sftp
#Fin de carga de variables
#debug -->
#	echo "1 $db"
#	echo "2 $udb"
#	echo "3 $pdb"
#	echo "4 $ddbr"
#	echo "5 $sftp"
#	echo "6 $sssh"
#	echo "7 $dsftp"
#	echo "8 $dssh"
#	echo "9 $rzdb"
#	echo "10 $1"
#	echo "11 $svresp"
#	echo "12 $ftpu_p"
#Fin de debug <--
infe=`date +'%Y%m%d-%H%M%S'`
headx "@" "<br>######################## Iniciando Respaldo de db: $db ################# $infe<br>"
headx "@" "Iniciando respaldo db -> $db"
dresp=$ddbr
#Crea carpetas de respaldo
if [ -d $ddbr ] ; then 
		echo "OK: $ddbr se crea sub directorio /Rot_$db"
		mkdir $ddbr"/Rot_$db"
	else 
		echo "No_DIR: $ddbr Se crea: $ddbr con sub directorio /Rot_$db"
		mkdir $ddbr"/Rot_$db"
fi
#fin de creacion de carpeta de respaldo
#inicia creacion de respaldo en 7z
ddbr=$ddbr"/Rot_$db/"
mysqldump --user=$udb --password=$pdb $db | 7z a -si $ddbr$rzdb
if [ $? -eq 0 ] ; then
	cd $ddbr 
	#se verifica que se encuentre en directorio de respaldo para subir envio de respaldos
		if [ -f $rzdb ] ; then
			headx "@" "Respaldo comprimido .7z completo $ddbr$rzdb"
			infi=`date +'%Y%m%d-%H%M%S'`
			slog $idglob "Respaldo comprimido .7z completo $ddbr$rzdb <Inicio=$infe{}Fin=$infi>"
			#Rotacion de respaldos
			if [ "$frota" = "1"  ] ; then #Si esta activa la rotacion de respaldos
				Nfiles=$(ls -tr $ddbr | wc -l)
				if [ $Nfiles -gt $svresp ] ; then
					to_del=$(( $Nfiles - $svresp ));
				 	for Dfile in $(ls -tr $ddbr | head -$to_del);
					 do
				 		rm -f $ddbr$Dfile;
				 		echo "se borro: $ddbr$Dfile por rotacion de $svresp"
				 	done
	  			fi
	  		fi
	  		#fin de rotacion de respaldos
	  		echo "$rzdb"
			if [ -f $rzdb ] ; then
				#validacion para guardar respaldo por mes
				# unoxmes Archivo con directorio "opcion db o od" "erp o base"
				unoxmes "$ddbr$rzdb" "db" "$db" "$dresp/" "$"
				# Subida de archivos por diferentes metodos..
				cd $ddbr #se verifica que se encuentre en directorio de respaldo para subir envio de respaldos
				headx "@" "Respaldo de db: $rzdb completado"
				
				# Comprobacio si esta activo el subir por ftp 
				if [ "$sftp" = "1" ] ; then
					headx "@" "Archivo a subir sftp -> $rzdb " 
					if curl --fail -u $ftpu_p -T $rzdb $dsftp; then # Envio de archivos de respaldo xftp
						headx "@" "Se subio archivo: $lfile fecha:$chh"
						slog $idglob "Se subio archivo: $lfile fecha:$chh"
					else
						headx "@" "Error al subir archivo $lfile Fecha: $chh"
						slog $idglob "Error al subir archivo $lfile Fecha: $chh"
					fi
				else
					headx "&" "No se selecciono subir archivo FTP"
				fi
				
				# Comprobacio si esta activo el subir por SSH
#set -x
				if [ "$sssh" = "1" ] ; then
					echo "$rzdb"
					echo "$dssh"
					headx "@" "Archivo a subir -> $rzdb Opcion: SSH"
					#rsync --partial --progress  $rzdb $dssh #Envio de archivos de respaldo xssh
					rsync_r =`rsync --partial --progress $rzdb $dssh`
					if [ ${PIPESTATUS[0]} -ne 0 ] ; then
				  		headx "@" "Error con rsync $rzdb"
				  		slog $idglob "Error con rsync $rzdb"
				  	else
				  		headx "@" "rsync OK $rzdb"
				  		slog $idglob "rsync OK $rzdb"
         			fi
#set +x
		 		else
				  	headx "&" "No se selecciono subir archivo SSH"
				fi
			else
				headx "&" "No, no existe respaldo .7z para realizar uno por mes : $rzdb " 
				slog $idglob "No, no existe respaldo .7z para realizar uno por mes : $rzdb "
		    fi
		else
			headx "&" "No, no existe respaldo .7z: no se creo o fallo base de datos $db; $rzdb  " 
			slog $idglob "No, no existe respaldo .7z: no se creo o fallo base de datos $db; $rzdb  " 
		fi		
else
headx "&" "Error al generar respaldo $db o error general"
slog $idglob "Error al generar respaldo $db o error general"
fi
#set +x
#savedb "Respaldo de DB: $1" "$cc" "${putres}" "$ret"
#savedb "BackupXmes Error $sfile" "Error al ingresar a $ydircd$yr " "Error General" "21"
#Fin de funcion
}

function crea_resp_cod(){

#Parametros crea_resp_cod id dir_lineaCodigo 
#Inicio de funcion
#Carga de datos de la Base de datos
	#bootx $1 
#Fin de carga de Datos DB
#Carga de variables
	lc=$az3 #linea de codigo erpZXZ
	ddbr=$ez3 #Directorio de respaldos
	cdbr=$nz3 #Directorio www para lineas de codigo
	sftp=$fz3 #activacion de respaldo por sftp
	sssh=$gz3 #activacion de respaldo por sssh
	dsftp=$iz3 #directorio para subir sftp
	dssh=$hz3 #directorio para subir dssh
	rzdbini="bkc_"$lc".7z"
	rzdb="bkc_"$lc"_"${chh}".7z"
	svresp=$jz3 #Numero de archivos para rotar
	frota=$dz3 #Activacion de rotacion de archivos
	ftpu_p=$kz3 #directorio para subir por sftp
	book="Contenedor.txt"
	lstdir=$cdbr$lc
#Fin de carga de variables
#set -x
infe=`date +'%Y%m%d-%H%M%S'`
echo "<br>######################## Iniciando Respaldo de Linea de codigo: $lc ################# $chh<br>"
echo "Iniciando respaldo Linea de codigo -> $lc"
dresp=$ddbr
#Crea carpetas de respaldo
if [ -d $ddbr ] ; then 
		echo "OK: $ddbr, se crea sub directorio /Rot_"$lc
		mkdir $ddbr"/Cod_"$lc
	else 
		echo "No_DIR: $ddbr Se crea: $ddbr con sub directorio /Rot_"$lc
		mkdir $ddbr
		mkdir $ddbr"/Cod_"$lc
fi
#fin de creacion de carpeta de respaldo
#inicia creacion de respaldo en 7z
ddbr=$ddbr"/Cod_"$lc"/"  #directorio www + carpeta de respaldos /respaldo/data/Cod_testw/
exc=$qz3 #Datos de exclusion de archivos
echo "Directorio origen $cdbr$lc  ::: Directorio Destino  $ddbr"
echo "Archivo a generar: $rzdbini o incremental $rzdb "
fdec=$ddbr$rzdbini #Archivo 7z inicial
echo "Directorio de respaldos $ddbr archivo $rzdbini"
fill=$cdbr$lc"/"$book #directorio para colocar el listado de archivos actualizados Directorio linea de codigo + archivo contenedor
echo "Directorio de libro $fill "
fdeci=$ddbr$rzdb #Directorio + Archivo incremental
echo "Directorio para respaldo incremental $fdeci"
if [ -f $fdec ] ; then #verifica si se encuentra creado el archivo inicial
	echo "Se encontro archivo inicial $rzdbini en el directorio $ddbr se creara archivo incremental"
	#Busca archivos con cambios -24 horas antes contando de la hora de ejecucion
	find $lstdir"/" -type f -mtime -1 > $fill 
	#vacia el contenido del archivo en una variable
	for line in $(cat $fill); 	
	do 
		7z u $fdeci $line  $exc 		#Comprime el incremental
	done
	if [ $? -eq 0 ] ; then #verifica que no se genere error al comprimir el codigo
			if [ -f $fdeci ] ; then #Verifica que se creo el archivo incremental
				echo "Respaldo comprimido Codigo .7z completo $fdeci"
				infi=`date +'%Y%m%d-%H%M%S'`
				slog $idglob "Respaldo comprimido Codigo .7z completo $fdeci --> Inicio=$infe <--> Fin=$infi"
				cd 	$ddbr	
				rzdb=$fdeci	
				if [ "$sftp" == "1" ] ; then
					headx "@" "Archivo a subir sftp -> $rzdb"
					# Envio de archivos de respaldo xftp 
					if curl --fail -u $ftpu_p -T $rzdb $dsftp; then 
						echo "Se subio archivo: $lfile fecha:$chh"
						slog $idglob "Se subio archivo: $lfile fecha:$chh"
					else
						echo "Error al subir archivo $lfile Fecha: $chh"
						slog $idglob "Error al subir archivo $lfile Fecha: $chh"
					fi
				else
					echo "No se selecciono subir archivo FTP"
				fi
				
				if [ "$sssh" == "1" ] ; then
					echo "$rzdb"
					echo "$dssh"
					echo "Archivo a subir -> $rzdb Opcion: SSH"
					rsync --partial --progress  $rzdb $dssh #Envio de archivos de respaldo xssh
					if [ ${PIPESTATUS[0]} -ne 0 ] ; then
				  		echo "Error while scp $rzdb"
				  		slog $idglob "Error while scp $rzdb"
				  	else
				  		echo "Successful scp $rzdb"
				  		slog $idglob "Successful scp $rzdb"
         			fi
		 		else
				  	echo "No se selecciono subir archivo SSH"
			    fi
			else
				echo "No se encontro archivo incremental $fdeci "    
			fi	
	else
		echo "Error al crear archivo incremental $fdeci "    
	fi
	
else
	#comprime respaldo inicial
	echo "No se encontro archivo inicial:$rzdbini Dir. Completo: $fdec "
	7z u $fdec $lstdir $exc #comprime el archivo inicial de respaldo
	if [ $? -eq 0 ] ; then #si no ocurren problemas
			if [ -f $fdec ] ; then #Verifica que se creo respaldo
				echo "Respaldo comprimido Codigo .7z completo $fdec"
				infi=`date +'%Y%m%d-%H%M%S'`
				slog $idglob "Respaldo comprimido Codigo .7z completo $fdec --> Inicio=$infe{}Fin=$infi"
				cd 	$ddbr	
				rzdb=$fdec	
				if [ "$sftp" == "1" ] ; then
					echo "Archivo a subir sftp -> $rzdb " 
					if curl --fail -u $ftpu_p -T $rzdb $dsftp; then # Envio de archivos de respaldo xftp
						echo "Se subio archivo: $lfile fecha:$chh"
						slog $idglob "Se subio archivo: $lfile fecha:$chh"
					else
						echo "Error al subir archivo $lfile Fecha: $chh"
						slog $idglob "Error al subir archivo $lfile Fecha: $chh"
					fi
				else
					echo "No se selecciono subir archivo FTP"
				fi
				
				if [ "$sssh" == "1" ] ; then
					echo "$rzdb"
					echo "$dssh"
					echo "Archivo a subir -> $rzdb Opcion: SSH"
					#Envio de archivos de respaldo xssh
					rsync --partial --progress --rsh=ssh $rzdb $dssh 
					if [ ${PIPESTATUS[0]} -ne 0 ] ; then
				  		echo "Error while scp $rzdb"
				  		slog $idglob "Error while scp $rzdb"
				  	else
				  		echo "Successful scp $rzdb"
				  		slog $idglob "Successful scp $rzdb"
         			fi
		 		else
					echo "No se selecciono subir archivo SSH"
				fi
			else
				echo "No encontro archivo $fdec "
			fi
			    
	else
		echo "Error al generar archivo .7z inicial"
		slog $idglob "Error al generar respaldo $lc o error general"	
	fi
fi
#set +x
}
#Fin de funcion
#----------------------------------------------------------------- Inicio del programa ---------------------------------------------------------------+
#------------------------------------------------------------------------		----------------------------------------------------------------------+
#------------------------------------------------------------------------		----------------------------------------------------------------------+
#------------------------------------------------------------------------		----------------------------------------------------------------------+
#------------------------------------------------------------------------		----------------------------------------------------------------------+
#---------------------------------------------------------------------		       -------------------------------------------------------------------+
#-----------------------------------------------------------------------		 ---------------------------------------------------------------------+
#-------------------------------------------------------------------------      ----------------------------------------------------------------------+
#---------------------------------------------------------------------------  ------------------------------------------------------------------------+
#-----------------------------------------------------------------------------------------------------------------------------------------------------+

#Procesamiento de Base de Produccion
function mainc (){
#Arranque de obtencio de informacion
	idglob=$1
	hrx=$2
	bootx $idglob
	echo "Inicio de Procesamiento: [$chh]--> $idglob , $c3"
	#slog $idglob "inicia respaldo"
#Fin
if [ "$b3" == "1" ] ; then #----Respaldos Activos??? [si,no]
		headx "@" "Respaldo de PRODUCCION activo:$b3 Dia :$a3 vs $dM[$dm] ; HB $j3 VS hora:$hrx ; Nombre de la base de datos: $c3" 
#----Comprobacion de Dia para ejecucion de respaldo
#-----------------------------------------------------------------------------------------------------------------------------------------------------+
		if [ -n "$a3" ] ; then  #Si es el dia y no esta vacio
			z=$a3
			echo "x"
			echo "$z"
			if [[ "$z" == *"A"*  ]] || [[ "$z" == *"a"* ]] ; then   
						headx "@" "Diario Produccion" 
						if [ "$j3" == "$hrx" ] ; then  #Si es el horario
							headx "@" "Hoy si se realiza el respaldo $j3" 
								if [ -n "$c3" ] ; then  #comprovacion de campo vacio nombre de base de datos
									headx "@" "Se ejecuta respaldo con la base:  $c3"
									if [ $tz3 = "0" ] ; then
										usado $idglob 
										crea_resp $idglob $c3 #corre el respaldo de la base de datos <<<<<------
									else
										echo "ocupado"
										slog $idglob "El registro se encuentra ocupado Produccion: $z,$c3,$j3"
									fi
								fi
						fi
			else	
			#-----Para dias
			#----Mon-Tue-Wed-Thu-Fri-Sat-Sun 
					if [[ "$z" == *"L"*  ]] || [[ "$z" == *"l"* ]] ; then  
						headx "@" "L" 
						if [ "$dM" == "Mon" ] ;  then 
						headx "@" "Hoy es lunes"
							if [ "$j3" == "$hrx" ] ; then  #Si es el horario
								headx "@" "Hoy si se realiza el respaldo $j3" 
									if [ -n "$c3" ] ; then  #comprovacion de campo vacio nombre de base de datos
										headx "@" "Se ejecuta respaldo con la base:  $c3"
										if [ $tz3 = "0" ] ; then
											usado $idglob 
											crea_resp $idglob $c3 #corre el respaldo de la base de datos <<<<<------
										else
											echo "ocupado"
											slog $idglob "El registro se encuentra ocupado Produccion: $z,$c3,$j3"
										fi
									fi
							fi	
						fi
					fi
					
					if [[ "$z" == *"M"*  ]] || [[ "$z" == *"m"* ]] ; then   
						headx "@" "M" 
						if [ "$dM" == "Tue" ] ;  then 
							headx "@" "Hoy es Martes"
							if [ "$j3" == "$hrx" ] ; then  #Si es el horario
							headx "@" "Hoy si se realiza el respaldo $j3" 
								if [ -n "$c3" ] ; then  #comprovacion de campo vacio nombre de base de datos
									headx "@" "Se ejecuta respaldo con la base:  $c3"
									if [ $tz3 = "0" ] ; then
											usado $idglob 
											crea_resp $idglob $c3 #corre el respaldo de la base de datos <<<<<------
										else
											echo "ocupado"
											slog $idglob "El registro se encuentra ocupado Produccion: $z,$c3,$j3"
										fi
								fi
							fi
						fi
					fi
					if [[ "$z" == *"Mi"*  ]] || [[ "$z" == *"mi"* ]] ; then  
						headx "@" "MI"
						if [ "$dM" == "Wed" ] ;  then 
							headx "@" "Hoy es Miercoles"
							if [ "$j3" == "$hrx" ] ; then  #Si es el horario
							headx "@" "Hoy si se realiza el respaldo $j3" 
								if [ -n "$c3" ] ; then  #comprovacion de campo vacio nombre de base de datos
									headx "@" "Se ejecuta respaldo con la base:  $c3"
									if [ $tz3 = "0" ] ; then
											usado $idglob 
											crea_resp $idglob $c3 #corre el respaldo de la base de datos <<<<<------
									else
											echo "ocupado"
											slog $idglob "El registro se encuentra ocupado Produccion: $z,$c3,$j3"
									fi								
								fi
							fi
						fi	
					fi
					if [[ "$z" == *"J"*  ]] || [[ "$z" == *"j"* ]] ; then   
						headx "@" "J"
						if [ "$dM" == "Thu" ] ;  then 
							headx "@" "Hoy es Jueves"
							if [ "$j3" == "$hrx" ] ; then  #Si es el horario
							headx "@" "Hoy si se realiza el respaldo $j3" 
								if [ -n "$c3" ] ; then  #comprovacion de campo vacio nombre de base de datos
									headx "@" "Se ejecuta respaldo con la base:  $c3"
									if [ $tz3 = "0" ] ; then
											usado $idglob 
											crea_resp $idglob $c3 #corre el respaldo de la base de datos <<<<<------
									else
											echo "ocupado"
											slog $idglob "El registro se encuentra ocupado Produccion: $z,$c3,$j3"
									fi								
								fi
							fi
						fi	
					fi
					if [[ "$z" == *"V"*  ]] || [[ "$z" == *"v"* ]] ; then   
						headx "@" "V" 
						if [ "$dM" == "Fri" ] ;  then 
							headx "@" "Hoy es Viernes"
							if [ "$j3" == "$hrx" ] ; then  #Si es el horario
							headx "@" "Hoy si se realiza el respaldo $j3" 
								if [ -n "$c3" ] ; then  #comprovacion de campo vacio nombre de base de datos
									headx "@" "Se ejecuta respaldo con la base:  $c3"
									if [ $tz3 = "0" ] ; then
											usado $idglob 
											crea_resp $idglob $c3 #corre el respaldo de la base de datos <<<<<------
									else
											echo "ocupado"
											slog $idglob "El registro se encuentra ocupado Produccion: $z,$c3,$j3"
									fi
								fi
							fi
						fi	
					fi
					if [[ "$z" == *"S"*  ]] || [[ "$z" == *"s"* ]] ; then    
						headx "@" "S"
						if [ "$dM" == "Sat" ] ;  then 
							headx "@" "Hoy es Sabado"
							if [ "$j3" == "$hrx" ] ; then  #Si es el horario
							headx "@" "Hoy si se realiza el respaldo $j3" 
								if [ -n "$c3" ] ; then  #comprovacion de campo vacio nombre de base de datos
									headx "@" "Se ejecuta respaldo con la base:  $c3"
									if [ $tz3 = "0" ] ; then
											usado $idglob 
											crea_resp $idglob $c3 #corre el respaldo de la base de datos <<<<<------
									else
											echo "ocupado"
											slog $idglob "El registro se encuentra ocupado Produccion: $z,$c3,$j3"
									fi
								fi
							fi
						fi	
					fi
					if [[ "$z" == *"D"*  ]] || [[ "$z" == *"d"* ]] ; then   
						headx "@" "D"
						if [ "$dM" == "Sun" ] ;  then 
							headx "@" "Hoy es Domingo"
							if [ "$j3" == "$hrx" ] ; then  #Si es el horario
							headx "@" "Hoy si se realiza el respaldo $j3" 
								if [ -n "$c3" ] ; then  #comprovacion de campo vacio nombre de base de datos
									headx "@" "Se ejecuta respaldo con la base:  $c3"
									if [ $tz3 = "0" ] ; then
											usado $idglob 
											crea_resp $idglob $c3 #corre el respaldo de la base de datos <<<<<------
									else
											echo "ocupado"
											slog $idglob "El registro se encuentra ocupado Produccion: $z,$c3,$j3"
									fi
								fi
							fi
						fi	
					fi
			fi
		fi				
else
		headx "@" "Respaldo de Produccion inactivo" 
fi
#--------------------------------------------------------- Procesamiento de Base de CAPA --------------------------------------------------------------+
#------------------------------------------------------------------------------------------------------------------------------------------------------+
#------------------------------------------------------------------------------------------------------------------------------------------------------+
#------------------------------------------------------------------------------------------------------------------------------------------------------+
#------------------------------------------------------------------------------------------------------------------------------------------------------+
#Procesamiento de Base de CAPA
if [ "$d3" == "1" ] ; then #----Respaldos Activos??? [si,no]
		headx "@" "Respaldo de CAPA activo:$d3 Dia :$h3 vs $dM[$dm] ; HB $k3 VS hora:$hrx ; Nombre de la base de datos: $e3" 
#----Comprobacion de Dia para ejecucion de respaldo
#-----------------------------------------------------------------------------------------------------------------------------------------------------+
		if [ -n "$h3" ] ; then  #Si es el dia y no esta vacio
			z=$h3
			echo "x"
			echo "$z"
			if [[ "$z" == *"0"*  ]] || [[ "$z" == *"0"* ]] ; then   
						headx "@" "Diario CAPA" 
						if [ "$k3" == "$hrx" ] ; then  #Si es el horario
							headx "@" "Hoy si se realiza Update $k3" 
								if [ -n "$e3" ] ; then  #comprovacion de campo vacio nombre de base de datos
									headx "@" "Se ejecuta Update con la base:  $e3"
									volc_resp $idglob $e3 #corre el respaldo de la base de datos <<<<<------
								fi
						fi
			else	
			#-----Para dias
					if [[ "$z" == *"1"*  ]] || [[ "$z" == *"1"* ]] ; then  
						headx "@" "L"
						if [ "$dM" == "Mon" ] ;  then 
						headx "@" "Hoy es lunes" 
							if [ "$k3" == "$hrx" ] ; then  #Si es el horario
							headx "@" "Hoy si se realiza Update $k3" 
								if [ -n "$h3" ] ; then  #comprovacion de campo vacio nombre de base de datos
									headx "@" "Se ejecuta Update con la base:  $e3"
									volc_resp $idglob $e3 #corre el respaldo de la base de datos <<<<<------
								fi
							fi
						fi
					fi
					
					if [[ "$z" == *"2"*  ]] || [[ "$z" == *"2"* ]] ; then   
						headx "@" "M" 
						if [ "$dM" == "Tue" ] ;  then 
							headx "@" "Hoy es Martes"
							if [ "$k3" == "$hrx" ] ; then  #Si es el horario
							headx "@" "Hoy si se realiza Update $k3" 
								if [ -n "$h3" ] ; then  #comprovacion de campo vacio nombre de base de datos
									headx "@" "Se ejecuta Update con la base:  $e3"
									volc_resp $idglob $e3 #corre el respaldo de la base de datos <<<<<------
								fi
							fi
						fi	
					fi
					if [[ "$z" == *"3"*  ]] || [[ "$z" == *"3"* ]] ; then  
						headx "@" "MI" 
						if [ "$dM" == "Wed" ] ;  then 
							headx "@" "Hoy es Miercoles"
							if [ "$k3" == "$hrx" ] ; then  #Si es el horario
							headx "@" "Hoy si se realiza Update $k3" 
								if [ -n "$h3" ] ; then  #comprovacion de campo vacio nombre de base de datos
									headx "@" "Se ejecuta Update con la base:  $e3"
									volc_resp $idglob $e3 #corre el respaldo de la base de datos <<<<<------
								fi
							fi
						fi
					fi
					if [[ "$z" == *"4"*  ]] || [[ "$z" == *"4"* ]] ; then   
						headx "@" "J" 
						if [ "$dM" == "Thu" ] ;  then 
							headx "@" "Hoy es Jueves"
							if [ "$k3" == "$hrx" ] ; then  #Si es el horario
							headx "@" "Hoy si se realiza Update $k3" 
								if [ -n "$h3" ] ; then  #comprovacion de campo vacio nombre de base de datos
									headx "@" "Se ejecuta Update con la base:  $e3"
									volc_resp $idglob $e3 #corre el respaldo de la base de datos <<<<<------
								fi
							fi
						fi
					fi
					if [[ "$z" == *"5"*  ]] || [[ "$z" == *"5"* ]] ; then   
						headx "@" "V"
						if [ "$dM" == "Fri" ] ;  then 
							headx "@" "Hoy es Viernes" 
							if [ "$k3" == "$hrx" ] ; then  #Si es el horario
							headx "@" "Hoy si se realiza Update $k3" 
								if [ -n "$h3" ] ; then  #comprovacion de campo vacio nombre de base de datos
									headx "@" "Se ejecuta Update con la base:  $e3"
									volc_resp $idglob $e3 #corre el respaldo de la base de datos <<<<<------
								fi
							fi
						fi					
					fi
					if [[ "$z" == *"6"*  ]] || [[ "$z" == *"6"* ]] ; then    
						headx "@" "S"
						if [ "$dM" == "Sat" ] ;  then 
							headx "@" "Hoy es Sabado"
							if [ "$k3" == "$hrx" ] ; then  #Si es el horario
							headx "@" "Hoy si se realiza Update $k3" 
								if [ -n "$h3" ] ; then  #comprovacion de campo vacio nombre de base de datos
									headx "@" "Se ejecuta Update con la base:  $e3"
									volc_resp $idglob $e3 #corre el respaldo de la base de datos <<<<<------
								fi
							fi
						fi
					fi
					if [[ "$z" == *"7"*  ]] || [[ "$z" == *"7"* ]] ; then   
						headx "@" "D"
						if [ "$dM" == "Sun" ] ;  then 
							headx "@" "Hoy es Domingo"
							if [ "$k3" == "$hrx" ] ; then  #Si es el horario
							headx "@" "Hoy si se realiza Update $k3" 
								if [ -n "$h3" ] ; then  #comprovacion de campo vacio nombre de base de datos
									headx "@" "Se ejecuta Update con la base:  $e3"
									volc_resp $idglob $e3 #corre el update de la base de datos <<<<<------
								fi
							fi
						fi
					fi
			fi
		fi				
else
		headx "@" "Update de CAPA inactivo" 
fi
#--------------------------------------------------------- Procesamiento de Base de Desarrollo --------------------------------------------------------+
#-----------------------------------------------------------------------------------------------------------------------------------------------------+
#-----------------------------------------------------------------------------------------------------------------------------------------------------+
#-------------------------------------------------------------------$$$$$$$$$$-------------------------------------------------------------------------+
#--------------------------------------------------------------------$$$$$$$$--------------------------------------------------------------------------+
#---------------------------------------------------------------------$$$$$$---------------------------------------------------------------------------+
#-----------------------------------------------------------------------$$-----------------------------------------------------------------------------+
#-----------------------------------------------------------------------------------------------------------------------------------------------------+

if [ "$f3" == "1" ] ; then #----Respaldos Activos??? [si,no]
		headx "@" "Respaldo de Desarrollo activo:$f3 Dia :$i3 vs $dM[$dm] ; HB $l3 VS hora:$hrx ; Nombre de la base de datos: $g3" 
#----Comprobacion de Dia para ejecucion de respaldo
#-----------------------------------------------------------------------------------------------------------------------------------------------------+
		if [ -n "$i3" ] ; then  #Si es el dia y no esta vacio
			z=$i3
			echo "x"
			echo "$z"
			if [[ "$z" == *"0"*  ]] || [[ "$z" == *"0"* ]] ; then   
						headx "@" "Diario Desarrollo"
						if [ "$l3" == "$hrx" ] ; then  #Si es el horario
							headx "@" "Hoy si se realiza el update $l3" 
								if [ -n "$g3" ] ; then  #comprovacion de campo vacio nombre de base de datos
									headx "@" "Se ejecuta update con la base:  $g3"
									volc_resp $idglob $g3 #corre el update de la base de datos <<<<<------
								fi
						fi
			else	
			#-----Para dias
					if [[ "$z" == *"1"*  ]] || [[ "$z" == *"1"* ]] ; then  
						headx "@" "L" 
						if [ "$dM" == "Mon" ] ;  then 
						headx "@" "Hoy es lunes"
							if [ "$l3" == "$hrx" ] ; then  #Si es el horario
								headx "@" "Hoy si se realiza el update $l3" 
									if [ -n "$g3" ] ; then  #comprovacion de campo vacio nombre de base de datos
										headx "@" "Se ejecuta update con la base:  $g3"
										volc_resp $idglob $g3 #corre el update de la base de datos <<<<<------
									fi
							fi
						fi
					fi
					
					if [[ "$z" == *"2"*  ]] || [[ "$z" == *"2"* ]] ; then   
						headx "@" "M" 
						if [ "$dM" == "Tue" ] ;  then 
							headx "@" "Hoy es Martes"
							if [ "$l3" == "$hrx" ] ; then  #Si es el horario
								headx "@" "Hoy si se realiza el update $l3" 
									if [ -n "$g3" ] ; then  #comprovacion de campo vacio nombre de base de datos
										headx "@" "Se ejecuta update con la base:  $g3"
										volc_resp $idglob $g3 #corre el update de la base de datos <<<<<------
									fi
							fi
						fi
					fi
					if [[ "$z" == *"3"*  ]] || [[ "$z" == *"3"* ]] ; then  
						headx "@" "MI"
						if [ "$dM" == "Wed" ] ;  then 
							headx "@" "Hoy es Miercoles"
							if [ "$l3" == "$hrx" ] ; then  #Si es el horario
								headx "@" "Hoy si se realiza el update $l3" 
									if [ -n "$g3" ] ; then  #comprovacion de campo vacio nombre de base de datos
										headx "@" "Se ejecuta update con la base:  $g3"
										volc_resp $idglob $g3 #corre el update de la base de datos <<<<<------
									fi
							fi
						fi
					fi
					if [[ "$z" == *"4"*  ]] || [[ "$z" == *"4"* ]] ; then   
						headx "@" "J" 
						if [ "$dM" == "Thu" ] ;  then 
							headx "@" "Hoy es Jueves"
							if [ "$l3" == "$hrx" ] ; then  #Si es el horario
								headx "@" "Hoy si se realiza el update $l3" 
									if [ -n "$g3" ] ; then  #comprovacion de campo vacio nombre de base de datos
										headx "@" "Se ejecuta update con la base:  $g3"
										volc_resp $idglob $g3 #corre el update de la base de datos <<<<<------
									fi
							fi
						fi
					fi
					if [[ "$z" == *"5"*  ]] || [[ "$z" == *"5"* ]] ; then   
						headx "@" "V" 
						if [ "$dM" == "Fri" ] ;  then 
							headx "@" "Hoy es Viernes"
							if [ "$l3" == "$hrx" ] ; then  #Si es el horario
								headx "@" "Hoy si se realiza el update $l3" 
									if [ -n "$g3" ] ; then  #comprovacion de campo vacio nombre de base de datos
										headx "@" "Se ejecuta update con la base:  $g3"
										volc_resp $idglob $g3 #corre el update de la base de datos <<<<<------
									fi
							fi	
						fi				
					fi
					if [[ "$z" == *"6"*  ]] || [[ "$z" == *"6"* ]] ; then    
						headx "@" "S"
						if [ "$dM" == "Sat" ] ;  then 
							headx "@" "Hoy es Sabado"
							if [ "$l3" == "$hrx" ] ; then  #Si es el horario
								headx "@" "Hoy si se realiza el update $l3" 
									if [ -n "$g3" ] ; then  #comprovacion de campo vacio nombre de base de datos
										headx "@" "Se ejecuta update con la base:  $g3"
										volc_resp $idglob $g3 #corre el update de la base de datos <<<<<------
									fi
							fi
						fi
					fi
					if [[ "$z" == *"7"*  ]] || [[ "$z" == *"7"* ]] ; then   
						headx "@" "D"
						if [ "$dM" == "Sun" ] ;  then 
							headx "@" "Hoy es Domingo"
							if [ "$l3" == "$hrx" ] ; then  #Si es el horario
								headx "@" "Hoy si se realiza el update $l3" 
									if [ -n "$g3" ] ; then  #comprovacion de campo vacio nombre de base de datos
										headx "@" "Se ejecuta update con la base:  $g3"
										volc_resp $idglob $g3 #corre el update de la base de datos <<<<<------
									fi
							fi
						fi
					fi
			fi
		fi				
else
		headx "@" "Update de Desarrollo inactivo" 
fi
#-------------------------------------------------------------------------------------------------------------------------------------------------------+
#-------------------------------------------------------------------------------------------------------------------------------------------------------+
#--------------------------------------------------------- Procesamiento de Respaldo de codigo --------------------------------------------------------+
#------------------------------------------------------------------------------------------------------------------------------------------------------+
#-----------------------------------------------------------------------@@@@@@@-------------------------------------------------------------------------+
#-------------------------------------------------------------------------@@@--------------------------------------------------------------------------+
#--------------------------------------------------------------------------@---------------------------------------------------------------------------+
if [ "$z3" == "1" ] ; then #----Respaldo de codigo Activo??? [si,no]
		headx "@" "Respaldo de Linea de codigo activo:$az3 Diario; HB $x3 VS hora:$hrx ; Dir codigo: $nz3" 
#----Comprobacion de Dia para ejecucion de respaldo
#-----------------------------------------------------------------------------------------------------------------------------------------------------+
		if [ -n "$az3" ] ; then  #Si la linea de codigo no esta vacia
			wwdir=$nz3 # Directorio de la linea de codigo
			echo "x"
			echo "$nz3"
			echo "$az3"
			if [ -n "$wwdir" ] ; then   
						headx "@" "Respaldo de codigo activo"
						if [ "$x3" == "$hrx" ] ; then  #Si es el horario a respaldar
							headx "@" "Hoy si se realiza el respaldo $x3" 
							headx "@" "Se ejecuta respaldo de la linea:  $az3"
							crea_resp_cod $idglob $az3 #corre el respaldo de la linea de codigo <<<<<------
						fi
			else
						headx "&" "Error Directorio de codigo vacio"
			fi
		fi
else
		headx "@" "Respaldo de Desarrollo inactivo" 
fi
#-------------------------------------------------------------------------------------------------------------------------------------------------------+
#-------------------------------------------------------------------------------------------------------------------------------------------------------+
#-------------------------------------------------------------------------------------------------------------------------------------------------------+
#-------------------------------------------------------------------------------------------------------------------------------------------------------+
#--------------------------------------------------------- Procesamiento de Respaldo Mensual -----------------------------------------------------------+
#-------------------------------------------------------------------------    --------------------------------------------------------------------------+
#-------------------------------------------------------------------------    --------------------------------------------------------------------------+
#-------------------------------------------------------------------------    --------------------------------------------------------------------------+
#-----------------------------------------------------------------------        ------------------------------------------------------------------------+
#-------------------------------------------------------------------------    --------------------------------------------------------------------------+
#--------------------------------------------------------------------------  ---------------------------------------------------------------------------+
#-------------------------------------------------------------------------------------------------------------------------------------------------------+
if [ "$bz3" == "1" ] ; then #----Respaldo mensual Activo??? [si,no]
	#Verifica que sea primero de mes
	texe=$cz3 #Tiempo de activacion de respaldo
	rbak=$ez3 #Ruta de respaldos
	henv=$hrx #horario enviado
	dbnm=$c3 #nombre de la base de datos
	rotrs=$jz3 #Numero de rotacion de respaldos para obtener el actual
	infi=`date +'%Y%m%d-%H%M%S'`
	yr=`date +'%Y'`
	mms=`date +'%B'`
	yr="db_"$yr"_"$dbnm #Directorio de respaldo mensual por base de datos
    dirb=$rbak"/"$yr  #Directorio de respaldo mensual
    dirn=$rbak"/Rot_"$dbnm #Directorio de respaldos diarios
    nxd=`date -d "tomorrow"`
	nxd=${nxd:8:2}
	cpfi=$dbnm"_"$mms".7z"
	headx "#" "Respaldo mensual activado"
  if [ "$texe" = "$hrx"  ] ; then #Si la hora concuerda para no pasar por todo el chow
	if [ -d $dirb ] ; then #Verificacion de directorio de respaldo mensual
		headx "@" "Se encontro directorio $dirb mensual"
		if [ -d $dirn ] ; then #Verificacion de directorio de respaldos normal
			#Rotacion de respaldos
			headx "@" "Se encontro directorio $dirn normal"
			if [ $nxd == " 1" ] || [ $nxd == "1" ];
				then
					headx "#" "Mañana es primero de mes $nxd"
					if [ "$texe" = "$hrx"  ] ; then #Si la hora concuerda
						cd $dirn
						zfilez=`find $i -type f -printf "%p\n" | sort -r | head -n1` #Archivo mas actual del directorio dejando una copia si este existe
						zf2=$(echo `expr length "$zfilez"`)
						zf3=${zfilez:2:zf2} 
						#headx "&" "$zf3"
						cp --backup $zf3 $dirb"/"$cpfi #copia el archivo a respaldos mensuales
						cd $dirb
							if [ -f $cpfi ] ; then #Verifica que el archivo se copio con exito al directorio
								headx "@" "Archivo $cpfi en la ruta $dirb se creo archivo mensual. con copia de archivo $zf3"
								infe=`date +'%Y%m%d-%H%M%S'`
								slog $idglob "Archivo $cpfi en la ruta $dirb se creo archivo mensual. con copia de archivo $zf3 ->Inicio=$infi{}Fin=$infe"
								
							else
								headx "&" "Error al copiar o verificar"
								infe=`date +'%Y%m%d-%H%M%S'`
								slog $idglob "Error al copiar o verificar ->Inicio=$infi{}Fin=$infe"
							fi	
					else
						headx "&" "no corresponde la hora:Enviada $hrx;Hora DB $texe"
	  				fi
	  		else
	  			headx "&" "Mañana no es 1ro de mes es $nxd del mes $mms."
	  		fi
	  	else 
	  		headx "&" "No se encontro directorio normal: $dirn "
	  	fi
	else
		headx "&" "Directorio no encontrado:$dirb"	
		infe=`date +'%Y%m%d-%H%M%S'`
		slog $idglob "Directorio no encontrado:$dirb ->Inicio=$infi{}Fin=$infe"
	fi
  else
        headx "&" "no corresponde la hora:Enviada $hrx;Hora DB $texe"		
        #infe=`date +'%Y%m%d-%H%M%S'`
		#slog $idglob "Directorio no encontrado:$dirb ->Inicio=$infi{}Fin=$infe"
  fi		
fi
#-------------------------------------------------------------------------------------------------------------------------------------------------------+
#-------------------------------------------------------------------------------------------------------------------------------------------------------+
}


#Pruebas de funciones
case $1 in
	orangebox)
	 #Entrada volcado respaldo
	 volc_resp $2 $3 $4 $5 $6 $7 $8
	 exit 3
	 ;;
	blackbox)
	 #Entrada para crear respaldo
	 crea_resp $2 $3 $4 $5 $6 $7
	 exit 3
	 ;; 
	 bboxcod)
	 #Entrada para crear respaldo
	 crea_resp_cod $2 $3 $4 $5 $6 $7
	 exit 3
	 ;;
	 whitebox)
	 #Entrada para crear respaldo
	 volc_resp $2 $3 $4 $5 $6 $7
	 exit 3
	 ;; 
	 datos)
	 #Entrada para crear respaldo 
	 bootx $2 $3 $4 $5 $6
	 exit 3
	 ;;
	 start)
	 #Entrada para crear respaldo 
	 mainc $2 $3 $4 $5 $6
	 exit 3
	 ;; 
	*)
	 #Entrada de Modo de uso
	  echo "*"
	  echo "Favor de ejecutar [ nohup ./core.sh & ]"
      exit 3 
	  ;;

esac





