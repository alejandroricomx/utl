#!/bin/bash  
# description: relin Start Stop Restart  
# processname: crel.sh -> rinfo.sh  
# chkconfig: 234 20 80  
chkcore=`ps aux | grep "crel.sh"  | grep -v grep  | wc -l`
cordet=`ps aux | grep "crel.sh"  | grep -v grep` 

case $1 in  
	start)  
		if [ "$chkcore" = "0" ] ; then 
			nohup /utl/relbin/crel.sh &
			if [ $? -eq 0 ] ; then
				echo "Se inicio el servicio.."
				ps aux | grep "crel.sh"  | grep -v grep
			else
				echo "No se pudo iniciar el servicio.."
				ps aux | grep "crel.sh"  | grep -v grep
			fi  
		else 
			echo "Ya se esta ejecutando: $cordet"
		fi
	;;   
	stop)     
		if [ "$chkcore" = "1" ] ; then 
			killall crel.sh   
			if [ $? -eq 0 ] ; then
				echo "Se paro el servicio.."
				ps aux | grep "crel.sh"  | grep -v grep
			else
				echo "No se pudo parar el servicio.."
				ps aux | grep "crel.sh"  | grep -v grep
			fi
		else 
			echo "No se encontro proceso: $cordet"
		fi	;;   
	restart)  
		if [ "$chkcore" = "1" ] ; then
			killall crel.sh 
			if [ $? -eq 0 ] ; then
				echo "se paro el servicio.."
				ps aux | grep "crel.sh"  | grep -v grep
			else
				echo "no se pudo parar el servicio.."
				ps aux | grep "crel.sh"  | grep -v grep
			fi
			echo "Espere 10 sec.."
			sleep 10
			echo "Iniciando el servicio.."
			nohup /utl/relbin/crel.sh &
			if [ $? -eq 0 ] ; then
				echo "Se inicio el servicio.."
				ps aux | grep "crel.sh"  | grep -v grep
			else
				echo "No se pudo iniciar el servicio.."
				ps aux | grep "crel.sh"  | grep -v grep
			fi   
		else 
			echo "El servicio no se esta ejecutando: $cordet utilice; relin.sh start"
			ps aux | grep "crel.sh"  | grep -v grep
		fi 
	;;   
	status)  		
		echo "--------------------------------------------------------------------->"
		echo "-----  Ejecutando numero de instancias: [ $chkcore ] de crel.sh  ----"
		echo "-----  Listado de procesos en ejecucion.                         ----"
		echo "--[]->  $cordet"
		echo "Se mostrara log en 10sec"
		sleep 10
		tail -200 /utl/relbin/nohup.out
		echo "---------------------------------------------------------------------"
		echo "---------------------------------------------------------------------<"
		;;   
esac      
exit 0
