#!/bin/sh
#
#Elaborado por Alejandro Rico E. 2017
#set -xv
#Check de eventos programados
#Arreglo0: ID
#Arreglo1: idserv
#Arreglo2: Onupda
#Arreglo3: ipserv
#Arreglo4: namserv
#Arreglo5: l_cod
#Arreglo6: vernow
#Arreglo7: tagvers
#Arreglo8: fxinstal
#Arreglo9: fxlastins
#Arreglo10: instlog
#Arreglo11: valido
#Arreglo12: updt
#Arreglo13: onloff
#Arreglo14: reinstalar
#Arreglo15: debug
#---------------------------------------------------------------- Variables Globales ------------------------------------------------------------------+
dM=`date +'%a'`
dm=`date +'%A'`
DY=(L M Mi J V S D) #Array de dias de la semana
dy=(l,m,mi,j,v,s,d) #Array de dias en minusculas por si...-
chh=`date +'%Y%m%d-%H%M%S'`
spin='▄■▀■▄'
#------------------------------------------------------------------------ Funciones ------------------------------------------------------------------+
#-----------------------------------------------------------------------------------------------------------------------------------------------------+

function headx()
{
	#Funcion para crear Promt segun el tipo de aviso
	#inicio de funcion
	dat=$2
	tp=$1
#Colores de texto
	red=`tput setaf 1`
	greenb=`tput setab 2`
    green=`tput setaf 2`
    yellow=`tput setaf 3`
    blue=`tput setaf 4`
    magenta=`tput setaf 5`
    cyan=`tput setaf 6`
    white=`tput setaf 7`
    reset=`tput sgr0`
    alert=`tput bel`
#fin de colores	
	if [ "$tp" == "#" ] ; then
			echo "${green}  #####################################################################################${reset}"
			echo "${yellow}#--[]-> ${green}    $dat ${reset}"
			echo "${green}  #####################################################################################${reset}"
		elif [ "$tp" == "@" ] ; then
			echo "${white}#--[]-> ${cyan}$dat ${reset}"	
		elif [ "$tp" == "&" ] ; then
			echo "${alert}${red}${greenb}#--[]-> $dat ${reset}"	
		else
			echo "${yellow}## --[]-> # $dat ${reset}"
	fi		             
#Fin de funcion	
}

function sqlrun(){
#Inicio de funcion
#Funcion para extraer datos por registro o ejecutar una consulta a un host en especifico
#Datos del host que tiene la base de datos para los respaldos-----------
host="162.252.87.133" #Si es global cambiar por IP de Desarrollo
userx="fkron"
pap="x1l0f0n0"
db_x="olog_x"
#-----------------------------------------------------------------------
#Recepcion de parametro de funcion
sql=$1
zi=`mysql --host=${host} --user=${userx} --password=${pap} ${db_x} -A<< END 
$sql
END`
if [ $? -eq 0 ] ; then
	echo $zi
else
	echo "1"
fi
#fin de funcion
}


#Nuevo comprimir mysqldump -u root -pp0rtali70s olog_x | 7z a -si backup.sql.7z
#Nuevo Descomprimir 7z e -so  backup.sql.7z | mysql --host=localhost -u root -pp0rtali70s ologx_7z
#Directorios
#Respaldos
function dbseek(){
	#Inicio de funcion
	#dbseek host usuario password db_name
	seekh=$1
	seeku=$2
	seekp=$3
	seekdb=$4
#funciona para validar existencia de DB segun host
seek=`mysql --host=$seekh --user=$seeku --password=$seekp << END
 use $seekdb -A;
END` 
if [ $? -eq 0 ] ;
then
	echo 0
else
	echo 1
fi

#fin de funcion
}

function slog(){
#Guarda log en utl
#us: slog id_reg "Mensaje"
inx="Usuario Log:[ Root ]F_Cambio:[ $chh ] -->"
finx="<br>"
sid=$1
deb=$2
preh=${info[10]}
sqlrun "update relup set instlog = '$preh$finx$inx$deb$finx' where idserv='$sid';"
}

function uok(){
#Guarda log en utl
#us: uok id_reg "Mensaje"
inx="Usuario Updt:[ Root ]F_Cambio:[ $chh ] -->"
finx="<br>"
sid=$1
deb=$2
preh=${info[12]}
#echo "preh: $preh"
#echo ">>>>: update relup set updt = '$preh$finx$inx$deb$finx' where idserv='$sid';"
sqlrun "update relup set updt = '$preh$finx$inx$deb$finx' where idserv='$sid';"
}

function dbug(){
#Guarda log en utl
#us: dbug id_reg "Mensaje"
inx="Usuario Debug:[ Root ]F_Cambio:[ $chh ] -->"
finx="<br>"
sid=$1
deb=$2
preh=${info[15]}
sqlrun "update relup set debug = '$preh$finx$inx$deb$finx' where idserv='$sid';"
}


function onoff(){
#Guarda desactiva por actualizar
#us: onoff id_reg 
sid=$1
sqlrun "update relup set Onupda = '0' where idserv='$sid';"
}




#--------------------------------------------------------------------- Datos de DB olog_x --------------------------------------------------------------+
#-------------------------------------------------------------------------------------------------------------------------------------------------------+
#-------------------------------------------------------------------------------------------------------------------------------------------------------+
#-------------------------------------------------------------------------------------------------------------------------------------------------------+

function bootx(){
#set -x
#Extrae todos los datos del id correspondiente y los almacena en un arreglo para utilizarlos despues
headx "@" "-> Boot"
idsv=$1
div=116 #divicion entre encabezados e informacion [Cada que cambia la base checar este parametro]

#----------------------- Extraxcion de encabezados 
b1=$(sqlrun "Select * from relup where idserv = '$idsv';")
b3=${b1:0:div} 
enca=($b3)

#----------------------- Extraxcion de datos 
i=0
for item2 in ${enca[*]} 	
do		
	a1=$(sqlrun "Select $item2 as 'X' from relup where idserv = '$idsv';")
	#echo "a1: $a1 "
	a2=$(echo `expr length "$a1"`)
	#echo "a2: $a2 "
	a3=${a1:2:a2}
	#echo "a3: $a3 " 
	info[$i]=$a3
	#echo "Arreglo >>>: $item2" 
	#echo "Arreglo [<]: ${info[$i]}"
	i=$((i+1))
done

#for item2 in ${info[*]} 	
#do		
#	echo "Arreglo2: $item2" 
#done

#for item in ${enca[*]} 
#do		
#	echo "Arreglo: $item" 
#done
#asi se llama// echo ${info[2]}
}

#-------------------------------------------------------------------------------------------------------------------------------------------------------+
#-------------------------------------------------------------------------------------------------------------------------------------------------------+
#-------------------------------------------------------------------------------------------------------------------------------------------------------+
#---------------------------------------------------Funciones ejecutivas dentro del crel -> rinfo ------------------------------------------------------+
#--------------------------------------------------------%%%%%%%%%%-------------------------------------------------------------------------------------+
#-------------------------------------------------------------%%%%%%%%%---------------------------------------------------------------------------------+
#-------------------------------------------------------------------%%%%%%$-----------------------------------------------------------------------------+

#------------------------------------------------------------ Inserta la info del servidor en la BD ----------------------------------------------------+
function sendrel(){	
#inicio de funcion
#Envia informacion del servidor
#envio id_serv
#------------------Carga de datos de la Base de datos
#bootx $1 
#------------------Fin de carga de Datos DB
#set -x
#variables
ids=$1
infe=`date +'%Y%m%d-%H%M%S'`
insr=`date +'%Y-%m-%d %H:%M:%S'`
lcode=${info[5]} #Directorio - Linea de codigo
#fin de variables
if [ -d  $lcode ] ; then #comprueba la linea de codigo
	cd $lcode
	headx "@" "Se encontro linea de codigo $lcode"
	v_act=`git describe`
	if [ $? -eq 0 ] ; then
		headx "@" "Version actual: $v_act"
		nine=$(sqlrun "update olog_x.relup set ${enca[6]}='$v_act' where idserv=$ids;") #actualiza version actual instalada
		if [ "$nine" == "1" ] ; then
			headx "&" "Error al agregar version actual $v_act, $nine"
		fi
		nine1=$(sqlrun "update olog_x.relup set ${enca[9]}='$insr' where idserv=$ids;") #Actualiza fecha de engreso de version actual
		if [ "$nine1" == "1" ] ; then
			headx "&" "Error al agregar fecha de instalacion actual $infe, $nine"
		fi

	else
		headx "&" "Error en extraccion de datos $v_act"
	fi	
else	
	headx "&" "no se encontro directorio: $lcode-${info[5]}"
		#slog $idglob "Error no se aplico update de $db_t, No se encontro Archivo $u_file"
fi 
#fin de funcion
}

#------------------------------------------------------------ Procesa Nuevo release  -------------------------------------------------------------------+
function insrel(){	
#inicio de funcion
#Envia informacion del servidor
#update id_serv
#------------------Carga de datos de la Base de datos
	#bootx $1 
#------------------Fin de carga de Datos DB
#set -x
#variables
ids=$1
infe=`date +'%Y%m%d-%H%M%S'`
lcode=${info[5]} #Directorio - Linea de codigo
relins=${info[7]} #Reinstalacion de version
reint_ver=${info[14]}
#fin de variables
if [ -d  $lcode ] ; then #comprueba la linea de codigo
	cd $lcode #entramos a la linea de codigo
	unset SSH_ASKPASS
	headx "@" "Se encontro linea de codigo $lcode"
	v_act=`git describe` #>Git@obtenemos version actual de la linea de codigo
	if [ $? -eq 0 ] ; then
		#si no ahi problemas al obtener la version actual de la linea de codigo
		headx "@" "Version actual: $v_act"
		if [ "$v_act" != "$relins"  ] ; then #comprobamos que la linea actual no sea la misma que se envio a instalar
			headx "&" "instalacion de release seleccionada" 
			g_fet=`git fetch` #>Git@actualiza solo referencias sin cambial la version
			if [ $? -eq 0 ] ; then
				#si no ocurre problemas
				headx "@" "Version actual: $v_act"
				g_fet=`git checkout $relins` #>Git@Actualiza la linea de codigo a la version puesta en la base de datos
				if [ $? -eq 0 ] ; then
					#si no ocurre problemas
					v_now=`git describe` #>Git@obtenemos version actual de la linea de codigo
					if [ "$v_now" == "$relins"  ] ; then
						headx "@" "Version actual: $v_now Version Anterior: $v_act Version a instalar: $relins"
						sendrel $idglob #Se envia info a la db con la version actualizada
						slog $ids "Se realizo actualizacion de codigo a version $v_now"
						uok $ids "Version $v_now"
						onoff $ids
					else
						headx "&" "Error en versiones, Actual: $v_now Version Anterior: $v_act Version a instalar: $relins"
						g_fet=`git checkout $v_act` #>Git@Actualiza la linea de codigo a la version puesta en la base de datos
						v_now=`git describe` #>Git@obtenemos version actual de la linea de codigo
						sendrel $idglob #Se envia info de version actual
						dbug $ids ">>>>Error en versiones, Actual: $v_now Version Anterior: $v_act Version a instalar: $relins<<<<<<<"
					fi
				else
					#si ocurre un problema no actualizar 
					v_now=`git describe` #>Git@Obtenemos la version actual de la linea de codigo
					headx "&" "Error al realizar checkout Version actual: $v_now, Version Anterior: $v_act Version a instalar: $relins "
					sendrel $idglob #Se envia info de version actual
					dbug $ids ">>>>>>Error al realizar checkout Version actual: $v_now, Version Anterior: $v_act Version a instalar: $relins<<<<<<<"
				fi
			fi
		else
			headx "&" "Error la version actual: $v_act es igual a la Version a instalar: $relins"
			sendrel $idglob #se envia info de version actual
			dbug $ids ">>>>>>>Error la version actual: $v_act es igual a la Version a instalar: $relins<<<<<<<"
		fi
	fi	
	
	#------------------------------------------------- Re-instalacion de la version de la linea de codigo -----------------------------------------+					
	
	if [ "$v_act" == "$relins"  ] && [ "$reint_ver" == "1"  ] ; then #comprobamos que la linea actual sea la misma que se envio a re-instalar
		headx "&" "Re-instalacion de release seleccionada"
			#comandos para git 
			g_fet=`git fetch` #>Git@actualiza solo referencias sin cambial la version
			if [ $? -eq 0 ] ; then
				#si no ocurre problemas
				headx "@" "Version actual re-intalar: $v_act"
				g_fet=`git checkout $relins` #>Git@Re instalamos version de linea de codigo
				if [ $? -eq 0 ] ; then
					#si no ocurre problemas
					v_now=`git describe` #obtenemos version actual de la linea de codigo
					if [ "$v_now" == "$relins"  ] ; then
						headx "@" "Version actual re-intalar: $v_now Version Anterior: $v_act Version a instalar: $relins"
						sendrel $idglob #enviamos version actual a la DB
						slog $ids "Se realizo re-instalacion de codigo a version $v_now"
						uok $ids "Re-instalacion Version $v_now"
						onoff $ids
					else
						headx "&" "Error en versiones re-intalar , Actual: $v_now Version Anterior: $v_act Version a instalar: $relins"
						sendrel $idglob #manda la version actual de la linea de codigo
						dbug $ids "Error en versiones re-intalar , Actual: $v_now Version Anterior: $v_act Version a instalar: $relins"
					fi
				else
					#si ocurre un problema no actualizar 
					v_now=`git describe`
					headx "&" "Error al realizar checkout re-intalar Version actual: $v_now, Version Anterior: $v_act Version a instalar: $relins "
					sendrel $idglob #como esta desactivada manda la version actual de la linea de codigo
					dbug $ids "Error al realizar checkout re-intalar Version actual: $v_now, Version Anterior: $v_act Version a instalar: $relins "
				fi
			fi
	
	fi
else	
	headx "&" "no se encontro directorio: $lcode-${info[5]}"
	sendrel $idglob #como esta desactivada manda la version actual de la linea de codigo
	dbug $ids "no se encontro directorio: $lcode-${info[5]}"
	slog $ids "Error --->>no se encontro directorio: $lcode-${info[5]}"
fi 
#fin de funcion
}
#----------------------------------------------------------------- Inicio del programa ---------------------------------------------------------------+
#------------------------------------------------------------------------		----------------------------------------------------------------------+
#------------------------------------------------------------------------		----------------------------------------------------------------------+
#------------------------------------------------------------------------		----------------------------------------------------------------------+
#------------------------------------------------------------------------		----------------------------------------------------------------------+
#---------------------------------------------------------------------		       -------------------------------------------------------------------+
#-----------------------------------------------------------------------		 ---------------------------------------------------------------------+
#-------------------------------------------------------------------------      ----------------------------------------------------------------------+
#---------------------------------------------------------------------------  ------------------------------------------------------------------------+
#-----------------------------------------------------------------------------------------------------------------------------------------------------+

function mainc (){
#Variables de funcion
	idglob=$1
	hrx=$2 
#obtencion de datos	
	bootx $idglob #resive el -i_d del demonio para procesar 
#Fin de obtencion de datos
#Variables de paso
	v_updon=${info[2]} #Si se marco para ser actualizado
	v_fins=${info[8]} #Fecha y hora para la instalacion
	v_fins=${v_fins//[[:space:]]} #quita espacion de la fecha para comparar con la enviada por crel.sh
	fh=$(echo `expr length "$v_fins"`)
	x=$(( $fh - 3 ))
	fb=${v_fins:0:x}
	v_finsF=$fb #Separacion de Fecha 
#Fin de variables
	echo "Fecha de BD: "$v_finsF
	echo "Fecha recibida crel; " $hrx
	echo "Inicio de Procesamiento: [$chh]--> $idglob , ${info[4]}"
	#slog $idglob "inicia respaldo"
if [ "$v_updon" == "1" ] ; then #----Actualizacion activa [si,no]
	headx "@" "Actualizacion activo:[$v_updon] Dia :$v_finsF vs $hrx" 
#---------------------------------------------- Comprobacion de datos para correr la actualizacion ---------------------------------------------------+
#-----------------------------------------------------------------------------------------------------------------------------------------------------+
		if [ -n "$v_finsF" ] ; then  #Si la fecha de instalacion no esta vacia
			headx "@" "x: $v_finsF"
			if [[ "$v_finsF" == *"$hrx"*  ]] ; then  #Si la fecha de instalacion es igual a la fecha recibida por crel.sh 
					headx "@" "Coninciden las fechas para aplicar release" 
					insrel $idglob #corre el update <---------------------------------------------------------------
			else
					echo "Fech de instalacion, DB:$v_finsF -> Fecha peticion: $hrx"
					sendrel $idglob #como esta desactivada manda la version actual de la linea de codigo
			fi
		else
			headx "&" "Campo de fecha vacio: $v_finsF "
			sendrel $idglob #como esta desactivada manda la version actual de la linea de codigo
		fi
else
	headx "@" "Actualizacion des-activada: [$v_updon]"
	sendrel $idglob #como esta desactivada manda la version actual de la linea de codigo
fi			

#-------------------------------------------------------------------------------------------------------------------------------------------------------+
#-------------------------------------------------------------------------------------------------------------------------------------------------------+
#Fin de funcion
}

#Pruebas de funciones
case $1 in
	envio)
	 #Entrada volcado respaldo
	 mainc $2 $3 $4 $5 $6 $7 $8
	 exit 3
	 ;;
	 update)
	 #Entrada volcado respaldo
	 insrel $2 $3 $4 $5 $6 $7 $8
	 exit 3
	 ;;	 
	 tw)
	 #Entrada para pruebas de funciones
	 bootx $2 
	 uok $2 $3 $4 $5 $6 $7
	 exit 3
	 ;; 
	*)
	 #Entrada de Modo de uso
	  echo "*"
	  echo "Favor de ejecutar [ ./relin.sh (start-stop-restart) ]"
      exit 3 
	  ;;
esac





