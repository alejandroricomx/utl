<?php
/* $Revision: 1.67 $ */

/* ARCHIVO MODIFICADO POR: GONZALO ALVAREZ*/
/* FECHA DE MODIFICACION: 10-NOV-2009 */
/* CAMBIOS:*/
/* 1.- TRADUCCION A ESPA�OL */
/* FIN DE CAMBIOS*/
error_reporting(E_ALL);
date_default_timezone_set("America/Mexico_City");

if (!isset($PathPrefix)) {
	$PathPrefix='';
}
if (!file_exists($PathPrefix . 'config.php')){
	$rootpath = dirname($_SERVER['PHP_SELF']);
	if ($rootpath == '/' OR $rootpath == "\\") {
		$rootpath = '';
	}
	header('Location:' . $rootpath . '/install/index.php');
}

include($PathPrefix . 'config.php');

if (isset($SessionSavePath)){
	session_save_path($SessionSavePath);
}

ini_set('session.gc_Maxlifetime',$SessionLifeTime);
ini_set('max_execution_time'    ,$MaximumExecutionTime);

session_start();

include($PathPrefix . 'includes/LanguageSetup.php');
include($PathPrefix . 'includes/ConnectDB.inc');
include($PathPrefix . 'includes/DateFunctions.inc');

include($PathPrefix .'webIntelligencePortalito/includes/GeneralFunctions.inc');

if (!isset($_SESSION['AttemptsCounter'])){
	$_SESSION['AttemptsCounter'] = 0;
}

/* iterate through all elements of the $_POST array and DB_escape_string them
to limit possibility for SQL injection attacks and cross scripting attacks
*/

if (isset($_SESSION['DatabaseName'])){
	foreach ($_POST as $key => $value) {
		if (gettype($value) != "array") {
			$_POST[$key] = DB_escape_string($value);
		} else {
			foreach ($value as $key1 => $value1) {
				$value[$key1] = DB_escape_string($value1);
			}
		}
	}

	/* iterate through all elements of the $_GET array and DB_escape_string them
	to limit possibility for SQL injection attacks and cross scripting attacks
	*/
	foreach ($_GET as $key => $value) {
		if (gettype($value) != "array") {
			$_GET[$key] = DB_escape_string($value);
		}
	}
}

if (!isset($AllowAnyone)){ /* only do security checks if AllowAnyone is not true */

	if (!isset($_SESSION['AccessLevel']) OR $_SESSION['AccessLevel'] == '' OR
		(isset($_POST['UserNameEntryField']) AND $_POST['UserNameEntryField'] != '')) {

	/* if not logged in */

		$_SESSION['AccessLevel'] = '';
		$_SESSION['CustomerID'] = '';
		$_SESSION['UserBranch'] = '';
		$_SESSION['SalesmanLogin'] = '';
		$_SESSION['Module'] = '';
		$_SESSION['PageSize'] = '';
		$_SESSION['UserStockLocation'] = '';
		$_SESSION['AttemptsCounter']++;
		$_SESSION['DefaultArea'] = '';
		$_SESSION['DefaultUnidad'] = '';
		$_SESSION['discount1'] = 0;
		$_SESSION['discount2'] = 0;
		$_SESSION['discount3'] = 0;
		$_SESSION['creditlimit'] = 0;
		//$_SESSION['XSA']="http://173.205.254.10/";
		$theme = 'silverwolf';
		
		// Show login screen
		if (!isset($_POST['UserNameEntryField']) or $_POST['UserNameEntryField'] == '') {
			include($PathPrefix . 'includes/Login.php');
			exit;
		}


		$sql = "SELECT www_users.fullaccess,
				www_users.customerid,
				www_users.lastvisitdate,
				www_users.pagesize,
				www_users.defaultarea,
				www_users.branchcode,
				www_users.modulesallowed,
				www_users.blocked,
				www_users.realname,
				www_users.theme,
				www_users.displayrecordsmax,
				www_users.userid,
				www_users.language,
				www_users.salesman,
				www_users.defaultunidadNegocio ,
				www_users.discount1 ,
				www_users.discount2 ,
				www_users.discount3 ,
				www_users.creditlimit,
				 www_users.email,
				www_users.ShowIndex
			FROM www_users
			WHERE www_users.userid='" . DB_escape_string($_POST['UserNameEntryField']) . "'
			AND (www_users.password='" . CryptPass(DB_escape_string($_POST['Password'])) . "'
			OR  www_users.password='" . DB_escape_string($_POST['Password']) . "')";
		$Auth_Result = DB_query($sql, $db);
		// Populate session variables with data base results
		if (DB_num_rows($Auth_Result) > 0) {

			$myrow = DB_fetch_row($Auth_Result);
			if ($myrow[7]==1){
			//the account is blocked
				die(include($PathPrefix . 'includes/FailedLogin.php'));
			}
			/*reset the attempts counter on successful login */
			$_SESSION['AttemptsCounter'] = 0;
			$_SESSION['AccessLevel'] = $myrow[0];
			$_SESSION['CustomerID'] = $myrow[1];
			$_SESSION['UserBranch'] = $myrow[5];
			$_SESSION['DefaultPageSize'] = $myrow[3];
			
			//$_SESSION['UserStockLocation'] = $myrow[4]; // era el campo defaultlocation
			$_SESSION['ModulesEnabled'] = explode(",", $myrow[6]);
			$_SESSION['UsersRealName'] = $myrow[8];
			$_SESSION['Theme'] = $myrow[9];
			$_SESSION['UserID'] = $myrow[11];
			$_SESSION['Language'] = $myrow[12];
			$_SESSION['SalesmanLogin'] = $myrow[13];
			$_SESSION['DefaultUnidad'] = $myrow[14];
			$_SESSION['discount1'] = $myrow[15];
			$_SESSION['discount2'] = $myrow[16];
			$_SESSION['discount3'] = $myrow[17];
			$_SESSION['creditlimit'] = $myrow[18];
			$_SESSION['usremail'] = $myrow[19];
			$_SESSION['ShowIndex'] = $myrow[20];
			
			
			if ($myrow[10] > 0) {
				$_SESSION['DisplayRecordsMax'] = $myrow[10];
			} else {
				$_SESSION['DisplayRecordsMax'] = 15; # $_SESSION['DefaultDisplayRecordsMax'];  // default comes from config.php
			}
			
			//Seleccion de nuevo men� o viejo menu
			if($_SESSION['ShowIndex']!=0){
				$sec_functions = "sec_functions_new";
			}else{
				$sec_functions = "sec_functions";
			}
			
			$sql= "SELECT b.id_boletin,b.funcion,smd.moduleid,smd.title as modulo,s.submoduleid,sm.moduleid,
				sm.title as submodulo,s.categoryid,sc.name as categoria,b.estado,b.usuario,b.fecha,b.descripcion,
				s.submoduleid,s.shortdescription,s.submoduleid,sp.userid
				FROM boletines b
				inner join $sec_functions s on b.funcion=s.functionid
				inner join sec_submodules sm on s.submoduleid=sm.submoduleid
				inner join sec_categories sc on s.categoryid=sc.categoryid
				inner join sec_modules smd on sm.moduleid=smd.moduleid
				inner join sec_funxprofile sf on s.functionid=sf.functionid
				inner join sec_profilexuser sp on sf.profileid=sp.profileid
				where b.fecha <= now()
				and b.fecha >= date_sub(now(), interval 1 month)
				and sp.userid='".$_SESSION['UserID']."'";
				$result = DB_query($sql, $db);
			
			if(DB_num_rows($result)>0){
				echo '<meta http-equiv="Refresh" content="0; url=' . $rootpath . '/ReportBoletines.php?">';
				
?>
			
		
<?php
			}
			$_SESSION['DefaultArea'] = $myrow[4];
			
			$sql = "UPDATE www_users SET lastvisitdate='". date("Y-m-d H:i:s") ."'
					WHERE www_users.userid='" . DB_escape_string($_POST['UserNameEntryField']) . "'";
			$Auth_Result = DB_query($sql, $db);

			/*get the security tokens that the user has access to */
			$sql = 'SELECT tokenid FROM securitygroups
					WHERE secroleid =  ' . $_SESSION['AccessLevel'];
			$Sec_Result = DB_query($sql, $db);

			$_SESSION['AllowedPageSecurityTokens'] = array();
			if (DB_num_rows($Sec_Result)==0){
				$title = _('Reporte de Error de Cuenta de Usuario');
				include($PathPrefix . 'includes/header.inc');
				echo '<br /><br /><br />';
				prnMsg(_('El Perfil de tu Usuario no tiene ningun acceso definido para webERP. Existe un error en la configuracion de esta cuenta de usuario'),'error');
				include($PathPrefix . 'includes/footer.inc');
				exit;
			} else {
				$i=0;
				while ($myrow = DB_fetch_row($Sec_Result)){
					$_SESSION['AllowedPageSecurityTokens'][$i] = $myrow[0];
					$i++;
				}
			}
			
			
			echo '<meta http-equiv="refresh" content="0" url="' . $_SERVER['PHP_SELF'] . '?' . SID . '">';
			exit;
		} else {     // Incorrect password
			
			// 5 login attempts, show failed login screen
			if (!isset($_SESSION['AttemptsCounter'])) {
				$_SESSION['AttemptsCounter'] = 0;
				
			} elseif ($_SESSION['AttemptsCounter'] >= 5 AND isset($_POST['UserNameEntryField'])) {
				/*User blocked from future accesses until sysadmin releases */
				
				/* COMENTADO PARA INICIO DE OPERACIONES, DESPUES ANALIZAR LA CONVENIENCIA DE DESBLOQUEARLO */
				$sql = "UPDATE www_users
						SET blocked=0
					WHERE www_users.userid='" . $_POST['UserNameEntryField'] . "'";
					
				$Auth_Result = DB_query($sql, $db);
				die(include($PathPrefix . 'includes/FailedLogin.php'));
			}
			$demo_text = '<font size="3" color="red"><b>' .  _('Contrase&ntilde;a incorrecta') . '</b></font><br /><b>' . _('La combinacion de Usuario/Contrase&ntilde;a') . '<br />' . _('no es un usuario valido en el sistema') . '</b>';
			die(include($PathPrefix . 'includes/Login.php'));
		}
	}		// End of userid/password check
	// Run with debugging messages for the system administrator(s) but not anyone else

	if (in_array(15, $_SESSION['AllowedPageSecurityTokens'])) {
		$debug = 1;
	} else {
		$debug = 0;
	}

} /* only do security checks if AllowAnyone is not true */

/*User is logged in so get configuration parameters  - save in session*/
include($PathPrefix . 'includes/GetConfig.php');

if(isset($_SESSION['DB_Maintenance'])){
	if ($_SESSION['DB_Maintenance']!=0)  {
		if (DateDiff(Date($_SESSION['DefaultDateFormat']),
				ConvertSQLDate($_SESSION['DB_Maintenance_LastRun'])
				,'d')	> 	$_SESSION['DB_Maintenance']){

			/*Do the DB maintenance routing for the DB_type selected */
			DB_Maintenance($db);
			//echo "entra";
			//purge the audit trail if necessary
			if (isset($_SESSION['MonthsAuditTrail'])){				
				// Actualizar  nombre de tabla de audittrail por mes
				$nombremes=glsnombremeslargo(date(m)-$_SESSION['MonthsAuditTrail']);
				//Verifica si la tabla existe la tabla de log por mes
				$sql="show tables   from ".$_SESSION['DatabaseName']." where Tables_in_".$_SESSION['DatabaseName']."='audittrail".$nombremes.date(Y)."'";
				$result = DB_query($sql,$db);
				if (DB_num_rows($result)==0){
				// CREA TABLA EN CASO DE QUE NO EXISTA
					
					$sql="CREATE  TABLE IF NOT EXISTS `audittrail".$nombremes.date(Y)."`";
					$sql=$sql."(
					  `transactiondate` datetime NOT NULL default '0000-00-00 00:00:00',
					  `userid` varchar(20) NOT NULL default '',
					  `querystring` text,
					  KEY `UserID` (`userid`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8";
					
					$Auth_TablaResult = DB_query($sql, $db);
					//INSERTA DATOS EN TABLA DE MES 
					$sql = " INSERT `audittrail".$nombremes.date(Y)."`
						SELECT * FROM audittrail
							WHERE  transactiondate <= '" . Date('Y-m-d', mktime(0,0,0, Date('m')-$_SESSION['MonthsAuditTrail'])) . "'";
					$result = DB_query($sql,$db);
					//echo $sql;
					//exit;
					//ELIMINA REGISTROS DE LA TABLA PRINCIPAL DEL AUDITTRAIL.
					
					 $sql = "DELETE FROM audittrail
							WHERE  transactiondate <= '" . Date('Y-m-d', mktime(0,0,0, Date('m')-$_SESSION['MonthsAuditTrail'])) . "'";
					$ErrMsg = _('Hubo un problema eliminando historia de logs de auditoria expirados');
					$result = DB_query($sql,$db);
					
					$sql="UPDATE config set confvalue=date_format(now(),'%Y-%m-%d') where confname='DB_Maintenance_LastRun'";
					$ErrMsg = _('Hubo un problema eliminando historia de logs de auditoria expirados');
					$result = DB_query($sql,$db);
					
				}
			}
			$_SESSION['DB_Maintenance_LastRun'] = Date('Y-m-d');
		}
	}
}

/*Check to see if currency rates need to be updated */
if (isset($_SESSION['UpdateCurrencyRatesDaily'])){
	if ($_SESSION['UpdateCurrencyRatesDaily']!=0)  {
		if (DateDiff(date($_SESSION['DefaultDateFormat']),
				ConvertSQLDate($_SESSION['UpdateCurrencyRatesDaily'])
				,'d')> 0){

			$CurrencyRates = GetECBCurrencyRates(); // gets rates from ECB see includes/MiscFunctions.php
			/*Loop around the defined currencies and get the rate from ECB */
			$CurrenciesResult = DB_query('SELECT currabrev FROM currencies',$db);
			while ($CurrencyRow = DB_fetch_row($CurrenciesResult)){
				if ($CurrencyRow[0]!=$_SESSION['CompanyRecord']['currencydefault'] and GetCurrencyRate ($CurrencyRow[0],$CurrencyRates)!=''){
					/*$UpdateCurrRateResult = DB_query('UPDATE currencies SET
											rate=' . GetCurrencyRate ($CurrencyRow[0],$CurrencyRates) . "
											WHERE currabrev='" . $CurrencyRow[0] . "'",$db);
					$InsertTipoCambio= "INSERT INTO tipocambio(currency,rate,fecha)
									 VALUES ('".$CurrencyRow[0]."','".GetCurrencyRate ($CurrencyRow[0],$CurrencyRates)."',now())";
					$result = DB_query($InsertTipoCambio,$db,$ErrMsg,$DbgMsg);
					*/
					//echo "insert".$UpdateCurrRateResult;
				}
			}
		
			$tiposCambio=GetAllCurrencyRatesFromPeriodDLS(ConvertSQLDate($_SESSION['UpdateCurrencyRatesDaily']),ConvertSQLDate(Date('Y-m-d')));
			
			if (count($tiposCambio) > 0) {
				for ($loopid=1;$loopid<=count($tiposCambio);$loopid++) {
					
					if (is_numeric($tiposCambio[$loopid]['rate'])) {
						if (checkdate(substr($tiposCambio[$loopid]['date'],3,2),substr($tiposCambio[$loopid]['date'],0,2),substr($tiposCambio[$loopid]['date'],6,4))) {
							$InsertTipoCambio= "INSERT INTO tipocambio(currency,rate,origrate,fecha)
										VALUES ('USD','".(1/$tiposCambio[$loopid]['rate'])."','".(1/$tiposCambio[$loopid]['rate'])."','".substr($tiposCambio[$loopid]['date'],6,4).'/'.substr($tiposCambio[$loopid]['date'],3,2).'/'.substr($tiposCambio[$loopid]['date'],0,2)."')";
							
							$ErrMsg = _('Hubo un problema actualizando el tipo de cambio para '.$tiposCambio[$loopid]['date']);
							$result = DB_query($InsertTipoCambio,$db,$ErrMsg,$DbgMsg);
							
							$UpdateCurrRateResult = DB_query('UPDATE currencies SET
											rate=' . (1/$tiposCambio[$loopid]['rate']) . "
											WHERE currabrev='USD'",$db);

							$sql="UPDATE config set confvalue=date_format(now(),'%Y-%m-%d') where confname='UpdateCurrencyRatesDaily'";
							$ErrMsg = _('Hubo un problema actualizando la ultima fecha de actualizacion de tipo de cambio!');
							$result = DB_query($sql,$db,$ErrMsg,$DbgMsg);
							    
							//para que no siga actualizando dentro de la misma session
							$_SESSION['UpdateCurrencyRatesDaily'] = Date('Y-m-d');
							
							if ($_SESSION['TipoCambioDiaAnterior']==1){
								
								//obtengo la ultima actualizacion del TC
								$sql="Select origrate, currency from tipocambio 
										where fecha < '".$_SESSION['UpdateCurrencyRatesDaily']."'
										Order by fecha DESC
										limit 1";
								$rstc = DB_query($sql,$db);
								$row = DB_fetch_array($rstc);
								
								$sql = "UpDate tipocambio
										Set rate = ".$row['origrate']."
										WHERE fecha = '".$_SESSION['UpdateCurrencyRatesDaily']."'
										and currency = '".$row['currency']."'";
								$r = DB_query($sql,$db);
							}
							
							
							prnMsg('Se actualizo el tipo de cambio del dia '.$tiposCambio[$loopid]['date'].' con valor '.$tiposCambio[$loopid]['rate'].' de acuerdo al diario oficial de la federacion','success');						
						} else {
							/*if(date('N') != DOMINGO) {
								prnMsg('No fue posible actualizar el tipo de cambio del dia '.$tiposCambio[$loopid]['date'].' porque la fecha no es valida !, consultar con el administrador del sistema!','error');
							}*/
						}
						
					} else {
						/*if(date('N') != DOMINGO) {
							prnMsg('No fue posible actualizar el tipo de cambio del dia '.$tiposCambio[$loopid]['date'].' valor no numerico:'.$tiposCambio[$loopid]['rate'].' !, consultar con el administrador del sistema!','error');
						}*/
					}
					
				}	
			} else {
				
				/*if(date('N') != DOMINGO) {
					prnMsg('No fue posible actualizar el tipo de cambio, resultado sin valores !, DE:'.ConvertSQLDate($_SESSION['UpdateCurrencyRatesDaily']).' HASTA:'.ConvertSQLDate(Date('Y-m-d')).' consultar con el administrador del sistema!','error');
				}*/
			}
			
			
			
			//$_SESSION['UpdateCurrencyRatesDaily'] = Date('Y-m-d');
			//$UpdateConfigResult = DB_query("UPDATE config SET confvalue = '" . Date('Y-m-d') . "' WHERE confname='UpdateCurrencyRatesDaily'",$db);
		}
	}
}

If (isset($_POST['Theme']) && ($_SESSION['UsersRealName'] == $_POST['RealName'])) {
	$_SESSION['Theme'] = $_POST['Theme'];
	$theme = $_POST['Theme'];
} elseif (!isset($_SESSION['Theme'])) {
	$theme = $_SESSION['DefaultTheme'];
	$_SESSION['Theme'] = $_SESSION['DefaultTheme'];

} else {
	$theme = $_SESSION['Theme'];
}

/* Set the logo if not yet set.
 * will be done only once per session and each time
 * we are not in session (i.e. before login)
 */
if (empty($_SESSION['LogoFile'])) {
	/* find a logo in companies/$CompanyDir
	 * (nice side effect of function:
	 * variables are local, so we will never
	 * cause name clashes)
	 */

	function findLogoFile($CompanyDir, $PathPrefix) {
		$dir = $PathPrefix.'companies/' . $CompanyDir . '/';
		$DirHandle = dir($dir);
		while ($DirEntry = $DirHandle->read() ){
			if ($DirEntry != '.' AND $DirEntry !='..'){
				$InCompanyDir[] = $DirEntry; //make an array of all files under company directory
			}
		} //loop through list of files in the company directory 
		if ($InCompanyDir !== FALSE) {
			foreach($InCompanyDir as $logofilename)
				if (strncasecmp($logofilename,'logo.jpg',8) === 0 AND
				is_readable($dir . $logofilename) AND
					is_file($dir . $logofilename)) {
				$logo = $logofilename;
				break;
			}

			if (empty($logo)) {
				return null;
			} else {
				return 'companies/' .$CompanyDir .'/'. $logo;
			}
		} //end listing of files under company directory is not empty
	}

	/* Find a logo in companies/<company of this session> */
	if (!empty($_SESSION['DatabaseName'])) {
		$_SESSION['LogoFile'] = findLogoFile($_SESSION['DatabaseName'], $PathPrefix);
	}

}

if ($_SESSION['HTTPS_Only']==1){
	if ($_SERVER['HTTPS']!='on'){
		prnMsg(_('webERP PORTALITO esta configurado para permitir solo conecciones encriptadas SSL seguras.'). '<br>Intentar entrar cambiando a <a href="' . str_replace("https","http",$rootpath) . '">'. _('pulse aqui') .'</a>'. _('para continuar'),'error');
		exit;
	}
}



// Now check that the user as logged in has access to the page being called. The $PageSecurity
// value must be set in the script before header.inc is included. $SecurityGroups is an array of
// arrays defining access for each group of users. These definitions can be modified by a system admin under setup


/*

if (!is_array($_SESSION['AllowedPageSecurityTokens']) AND !isset($AllowAnyone)) {
	$title = _('Account Error Report');
	include($PathPrefix . 'includes/header.inc');
	echo '<br /><br /><br />';
	prnMsg(_('Su cuenta de usuario no a sido configurada con parametros de acceso de seguridad. Favor de notificar al administrador del sistema. Tambien podria ser que exista un problema de sesion con su servidor de internet PHP'),'error');
	include($PathPrefix . 'includes/footer.inc');
	exit;
}



if (!isset($AllowAnyone)){
	if ((!in_array($PageSecurity, $_SESSION['AllowedPageSecurityTokens']) OR !isset($PageSecurity))) {
		$title = _('Problema de Permisos de Seguridad');
		include($PathPrefix . 'includes/header.inc');
		echo '<tr>
			<td class="menu_group_items">
				<table width="100%" class="table_index">
					<tr><td class="menu_group_item">';
		echo '<b><font style="size:+1; text-align:center;">' . _('ccLa configuracion de seguridad de su cuenta no permite el acceso a esta funcion') . '</font></b>';

		echo '</td>
		</tr>
		</table>
		</td>';

		include($PathPrefix . 'includes/footer.inc');
		exit;
	}


 }

 */

function CryptPass( $Password ) {
    	global $CryptFunction;
    	if ( $CryptFunction == 'sha1' ) {
    		return sha1($Password);
    	} elseif ( $CryptFunction == 'md5' ) {
    		return md5($Password);
	} else {
    		return $Password;
    	}
 }
?>