<title>Respaldos v5</title>
<head>
<link href="vcss/stilo.css" rel="stylesheet">
</head>
<?php
include("db.php");
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
//echo ("Debug->");
if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$idl=mysql_real_escape_string($_POST['iddel']);		//IP para borrar
	$q1=mysql_real_escape_string($_POST['ip_ser']);		//Ip del servidor
	$q2=mysql_real_escape_string($_POST['id_ser']);   	// ID de servidor
	$q3=mysql_real_escape_string($_POST['nam_ser']); 	//Nombre del servidor
	$q4=mysql_real_escape_string($_POST['rdia_chkac']); //Checkbox Ractivar respaldo produccion
	$q5=mysql_real_escape_string($_POST['ndb_prod']); 	//Nombre de DB Produccion
	$w7=mysql_real_escape_string($_POST['RD_nom']); 	//Dias de respaldo Produccion
	$w4=mysql_real_escape_string($_POST['time_rdia']); //Horario de respaldo diario Produccion

	$q6=mysql_real_escape_string($_POST['rcap_chkac']);	  // Activar y desactivar Capa
	$q7=mysql_real_escape_string($_POST['ndb_cap']); 	  // Nombre de DB CAPA
	$q8=mysql_real_escape_string($_POST['date_bkcapa']);  //Dia de respaldo CAPA
	$w5=mysql_real_escape_string($_POST['time_rcap']);    //Horario de respaldo de Capa

	$q9=mysql_real_escape_string($_POST['rdes_chkac']); //Activar y desactivar respaldo de desarrollo
	$w1=mysql_real_escape_string($_POST['ndb_des']);    //Nombre de la base de datos Desarrollo
	$w3=mysql_real_escape_string($_POST['date_bkdes']); //Dia de respaldo de Base desarrollo
	$w6=mysql_real_escape_string($_POST['time_rdes']);  //Horario de ejecucion de Desarrollo
	$w8=mysql_real_escape_string($_POST['hist']);	//Historial
	$w2=mysql_real_escape_string($_POST['txt_com']); //Comentarios
	//Datos extras
	$xt1=mysql_real_escape_string($_POST['bkcod']); //Activacion de respaldos de codigo
	$xt2=mysql_real_escape_string($_POST['time_ticod']); //Horario de respaldos de codigo
	$xt3=mysql_real_escape_string($_POST['lcodigo']); //Linea de codigo (Directorio dentro de html/)
	$xt4=mysql_real_escape_string($_POST['bkxmes']); //Activacion de bakup por mes
	$xt5=mysql_real_escape_string($_POST['tixmes']); //Horario de respaldo mensual
	$xt6=mysql_real_escape_string($_POST['bkrota']); //Rotacion de respaldos diarios (Default 7 dias y se borran)/nunca se borran
	$xt7=mysql_real_escape_string($_POST['bkdir']); //Directorio de respaldos
	$xt8=mysql_real_escape_string($_POST['bkftps']); //Activacion de envio de respaldos por sftp
	$xt9=mysql_real_escape_string($_POST['bkssh']); //Activacion de envio de respaldos por ssh
	$xt10=mysql_real_escape_string($_POST['ddshtp']); //URL para envio de ssh
	$xt11=mysql_real_escape_string($_POST['ddsftp']); //URL para envio de ftp
	//Fin de datos extra
	//Variables finales ***Cuidado con cambios a estas variables ya que contienen informacion importante
	$x1=mysql_real_escape_string($_POST['wwwdir']); //Directorio www [Para respaldos de codigo]
	$x2=mysql_real_escape_string($_POST['rotbk']); //Numero de respaldos antes de realizar la rotacion
	$x3=mysql_real_escape_string($_POST['up_ftp']); //Usuario y contraseña para coneccion por ftp
	$x4=mysql_real_escape_string($_POST['up_ssh']); //Usuario y contraseña para coneccion por ssh
	$x5=mysql_real_escape_string($_POST['us_bk']); //Usuario base de datos (Genera la coneccion para el respaldo)
	$x6=mysql_real_escape_string($_POST['pass_bk']); //contraseña base de datos (Genera la coneccion para el respaldo)
	$x7=mysql_real_escape_string($_POST['pxdir']); //Directorio para respaldo remoto
	$x8=mysql_real_escape_string($_POST['pxxu_p']); //Usuario y password para conexion a respaldo remoto
	$x9=mysql_real_escape_string($_POST['sdesip']); //IP del servidor de desarrollo [Para update de bases de desarrollo]
	$x10=mysql_real_escape_string($_POST['noresp']); //Patron para omitir respaldos de cierto tipo de archivos segun patron
	
	$accion=mysql_real_escape_string($_POST['que']);    //Estatus de operacion 
	$us_h=mysql_real_escape_string($_POST['usact']);    //Estatus de operacion 
	$timer=date('H:i:s'); 
	$hoy=date('H:i:s');
	$fxc=date('Y-m-d H:i:s');
	$w4=str_replace(' ', '',$w4);
	$w5=str_replace(' ', '',$w5);
	$w6=str_replace(' ', '',$w6);
	$xt2=str_replace(' ', '',$xt2);
	$xt5=str_replace(' ', '',$xt5);

if(strlen($q1)>0 || strlen($q2)>0 || strlen($q3)>0 || strlen($idl)>0)
{	
	
	if ($accion == "mod")
	{
		$htc='Usuario:[ '.$us_h.' ]'.'F_Cambio:[ '.$fxc.' ] --> '.$w2.'<br>'.$w8.'<br>';
		mysql_query("update bkkron set ipserv='$q1', `imple`='$q5', sname='$q3',idserv='$q2',`bkxdiaNOM`='$w7', `uprod`='$q4', `dbprodnom`='$q5', `ucap`='$q6',`seldcap`='$q8',`selddes`='$w3', `dbcapanom`='$q7', `udes`='$q9', `dbdesnom`='$w1', `f_alta`='$fxc',`timerdia`='$w4', `ticapa`='$w5', `tides`='$w6',`coment`='$w2',`history`='$htc', `bkcod`='$xt1', `ticod`='$xt2', `lcodigo`='$xt3', `bkxmes`='$xt4', `tixmes`='$xt5', `bkrota`='$xt6', `bkdir`='$xt7', `bkftps`='$xt8', `bkssh`='$xt9', `ddshtp`='$xt10', `ddsftp`='$xt11', `wwwdir`='$x1', `rotbk`='$x2', `up_ftp`='$x3', `up_ssh`='$x4', `us_bk`='$x5', `pass_bk`='$x6', `pxdir`='$x7', `pxxu_p`='$x8', `sdesip`='$x9', `noresp`='$x10' where `idserv` = '$idl'");
		$est="Modificada";	
	}

	if ($accion == "save")
	{
		$htcN='Usuario:[ '.$us_h.' ]'.'F_Nuevo:[ '.$fxc.' ] --> '.$w2.'<br> >> Creacion de Registro << <br>';
		mysql_query("INSERT INTO bkkron ( `id`,`idserv`, `ipserv`, `imple`, `sname`, `bkxdiaNOM`, `uprod`, `dbprodnom`, `ucap`, `dbcapanom`, `udes`, `dbdesnom`, `coment`, `f_alta`, `timerdia`, `history`, `ticapa`, `tides`, `seldcap`, `selddes`, `bkcod`, `ticod`, `lcodigo`, `bkxmes`, `tixmes`, `bkrota`, `bkdir`, `bkftps`, `bkssh`, `ddshtp`, `ddsftp`, `wwwdir`, `rotbk`, `up_ftp`, `up_ssh`, `us_bk`, `pass_bk`, `pxdir`, `pxxu_p`, `sdesip`, `noresp`) values ('','$q2','$q1','$q5','$q3','$w7','$q4','$q5','$q6','$q7','$q9','$w1','$w2','$fxc','$w4','$htcN','$w5','$w6','$q8','$w3','$xt1','$xt2','$xt3','$xt4','$xt5','$xt6','$xt7','$xt8','$xt9','$xt10','$xt11','$x1','$x2','$x3','$x4','$x5','$x6','$x7','$x8','$x9','$x10')");
		$est="Creado";
	}
	
	if ($accion == "delete")
	{
		// sending query
		$sql= "INSERT INTO bkkron_del (`idserv`, `ipserv`, `imple`, `sname`, `bkxdiaNOM`, `uprod`, `dbprodnom`, `ucap`, `dbcapanom`, `udes`, `dbdesnom`, `coment`, `f_alta`, `timerdia`, `history`, `ticapa`, `tides`, `seldcap`, `selddes`, `bkcod`, `ticod`, `lcodigo`, `bkxmes`, `tixmes`, `bkrota`, `bkdir`, `bkftps`, `bkssh`, `ddshtp`, `ddsftp`, `rotbk`, `up_ftp`, `up_ssh`, `pxdir`, `pxxu_p`, `wwwdir`, `us_bk`, `pass_bk`, `noresp`, `sdesip`, `job`) 
SELECT `idserv`, `ipserv`, `imple`, `sname`, `bkxdiaNOM`, `uprod`, `dbprodnom`, `ucap`, `dbcapanom`, `udes`, `dbdesnom`, `coment`, `f_alta`, `timerdia`, `history`, `ticapa`, `tides`, `seldcap`, `selddes`, `bkcod`, `ticod`, `lcodigo`, `bkxmes`, `tixmes`, `bkrota`, `bkdir`, `bkftps`, `bkssh`, `ddshtp`, `ddsftp`, `rotbk`, `up_ftp`, `up_ssh`, `pxdir`, `pxxu_p`, `wwwdir`, `us_bk`, `pass_bk`, `noresp`, `sdesip`, `job` 
FROM bkkron WHERE idserv = '$idl'";
		$prev=mysql_query($sql, $bd); //Mueve registro a otra tabla para el log de borrados
		mysql_query("DELETE FROM bkkron WHERE idserv='$idl'") or die(mysql_error());
		$est="Se borro el registro.";
	}
?>
<table style="width:100%; height:400px;" border="0">
  <tbody>
    <tr>
      <td height="30px" colspan="10" align="right"><div id="divgua" name="divgua" style="display:block; margin-left: 5px; margin-bottom: 5px; margin-top: 5px; " align="right"><!--<button class="submit" type="button" name="preclose" id="preclose" onClick="prev();">Cerrar</button>--></div>
	      <img src="css/8934.png" id="preclose"  name="preclose" width="30px" height="30px" style="margin-right: 2px; margin-top: 2px; margin-bottom: 2px; margin-left: 92%;" onClick="prev();" alt=""/></td>
    </tr>
    <tr>
		<td height="30px" colspan="10" class="titi"><font class="tititext">Estatus: </font><font class="tititextw"><?php echo($est); ?> </font> </td>
    </tr>
    <tr>
      <td width="30" height="322">&nbsp;</td>
      <td colspan="8" class="texd"><?php echo ("Datos enviados:<br>Ip del servidor:$q1:q1<br>ID de servidor:$q2:q2<br>Nombre del servidor:$q3:q3<br>"); ?>
      <?php echo ("<h2>Se envio la informacion!</h2>"); } else {?>
	<table style="width:100%; height:1000px;" border="0">
  <tbody>
    <tr>
      <td height="30px" colspan="10" align="right"><div id="divgua" name="divgua" style="display:block; margin-left: 5px; margin-bottom: 5px; margin-top: 5px; " align="right"><!--<button class="submit" type="button" name="preclose" id="preclose" onClick="prev();">Cerrar</button>--></div>
	      <img src="css/8934.png" id="preclose"  name="preclose" width="30px" height="30px" style="margin-right: 2px; margin-top: 2px; margin-bottom: 2px; margin-left: 92%;" onClick="prev();" alt=""/></td>
    </tr>
    <tr>
		<td height="30px" colspan="10" class="titi"><font class="tititext">Estatus: </font><font class="tititextw"><?php echo($est); ?> </font> </td>
    </tr>
    <tr>
      <td width="30" height="322">&nbsp;</td>
      <td colspan="8" class="texd">
	<?php echo ("<br>Error:<br>Datos enviados:<br>Ip del servidor:$q1<br>ID de servidor:$q2<br>Nombre del servidor:$q3<br>Checkbox R-activar respaldo produccion:$q4<br>Nombre de DB Produccion:$q5<br>Activar y desactivar Capa:$q6<br>Nombre de DB CAPA:$q7<br>Dia de respaldo CAPA:$q8<br>Activar y desactivar respaldo de desarrollo:$q9<br>Nombre de la base de datos Desarrollo:$w1<br>Comentarios:$w2<br>Dia de update CAPA:$w3<br>Dia de respaldo de DES:$q8<br>Historial::HTC<br>Dia de respaldo de Base desarrollo:$w3<br>Horario de respaldo diario Produccion:$w4<br>Horario de respaldo de Capa:$w5<br>Horario de ejecucion de Desarrollo:$w6<br>Dias de respaldo Produccion:$w7<br>ID Para borrar:$idl<br>-$est<br>-$accion,$x1>$x2>$x3>$x4>$x5>$x6>$x7>$x8>$x9>$x10"); echo ("<br><h2>No se envio la informacion!</h2>"); }}?></td>
      <td width="30">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td width="360">&nbsp;</td>
      <td width="28">&nbsp;</td>
      <td width="28">&nbsp;</td>
      <td width="28">&nbsp;</td>
      <td width="66">&nbsp;</td>
      <td width="50">&nbsp;</td>
      <td width="46">&nbsp;</td>
      <td width="26">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </tbody>
</table>
