<?php 
$PageSecurity = 10;
//Archivo para graficar las bases de datos segun tamaño incremental
//Se requiere colocar cron en cada servidor de la siguiente manera
//@hourly /utl/respaldo/bbck.sh runq erpmg  1  [bbck.sh runq (Implementacion)  [Datos de acceso, los toma de olog_x Tabla:slist]]
//Lee la base olog_x y la tabla DBZ para realizar la grafica
//Metodologia de programacion ...xxxxx
$funcion = 1947;
include ('includes/session.inc'); 
	$title = _ ( 'Gráficas de Companies V0.1.100' );
include ('includes/header.inc'); 
include ('includes/SecurityFunctions.inc');
	echo '<p class="page_title_text"><img src="images/20100806111056cog_32x32.gif" title="' . _ ( 'Autores' ) . '" alt="">' . ' ' . $title . '</p>';	
?>

<?php $resp=$_POST["db_N"]; ?>
<link href="css/stilo.css" rel="stylesheet">
	<form method="post" action="#">
<?php
require_once("RandomClass.php");
$rand = new RandomTable();
$rawdata = $rand->getAllInfoHD($resp);
$valoresArray;
$timeArray;
for($i = 0 ;$i<count($rawdata);$i++){
    $valoresArray[$i]= $rawdata[$i][2];
    $time= $rawdata[$i][3];
    $date = new DateTime($time);
    $timeArray[$i] = $date->getTimestamp()*1000;
}
$zz = new RandomTable();
$combi = $zz -> gocombHD();
$xx;
?> 
<table class="tablesec" >
  <tbody class="tablesec">
    <tr class="trfg">
      <td class="tdtext" ><label>Sistema de gráficas Companies: <?php echo($resp);?></label></td>
      <td class="tdrig" >
	  <div class="select-style">
	  	<select name="db_N" onchange="this.form.submit()">
	  		<option value="" >Seleccione una DB</option> 
	  			<?php for($i = 0 ;$i<count($combi);$i++){ $xx[$i]= $combi[$i][1]; ?>
	  				<option value="<?php echo $xx[$i]?>" > <?php echo $xx[$i]?></option> 
	  			<?php echo $xx[$i]; } ?>
		</select> 
		</div>
      </td>
	  </tr>
	   <tr>
      <td class="tdtext" >Fecha</td>
      <td class="tdrig" >Valor actual</td>
    </tr>
	  </tr>
<?php for($i = count($rawdata)-1 ;$i<count($rawdata);$i++){ ?>
    <tr>
      <td class="tdtext" ><?php echo date('d/m/Y',$timeArray[$i]/1000);?></td>
      <td class="tdrig" ><?php echo number_format($valoresArray[$i], 2, ',', ' ');?> MB</td>      
     </tr>      
<?php } ?>
    <tr>
      <td class="tdtext" ></td>
      <td class="tdrig" ></td>
    </tr>
  </tbody>
</table>

</form>
<div id="contenedor"></div>
<script src="includes/jquery.js"></script>
<script src="includes/highstock.js"></script>
<script src="includes/exporting.js"></script>
<script>
Highcharts.setOptions({
    global: {
        useUTC: false
    }
});
chartCPU = new Highcharts.StockChart({
    chart: {
        renderTo: 'contenedor'
    },
    rangeSelector : {
        enabled: false
    },
    title: {
        text: 'Gráfica:#<?php echo($resp);?>#'
    },
    xAxis: {
        type: 'datetime'
    },
    yAxis: {
        minPadding: 0.2,
        maxPadding: 0.2,
        title: {
            text: 'Valores MB.',
            margin: 10
        }
    },
    series: [{
        name: 'valor',
        data: (function() {
                var data = [];
                <?php
                    for($i = 0 ;$i<count($rawdata);$i++){
                ?>
                data.push([<?php echo $timeArray[$i];?>,<?php echo $valoresArray[$i];?>]);
                <?php } ?>
                return data;
            })()
    }],
    credits: {
            enabled: false
    }
});
</script>   
<?php
include ('includes/footer.inc');
?>