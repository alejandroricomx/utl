#!/bin/sh
#
#Elaborado por Alejandro Rico E.
#Beta V1.0 23-24 de agosto Creacion de instalacion de servidor compartido
#Beta V1.0.1 26 de Agosto Funciones para instalacion de Servidor Privado (Se prueban funciones por separado)
#Beta V1.0.2 7 septiembre Se modifica funcion de verificacion de servicios por que en algunnos servidores no los encuentra por status. se utiliza ps
#set -xv
chh=`date +'%Y%m%d-%H%M%S'`
fh=`date +'%Y-%m-%d'`
hc=`date +'%H:%M'`
ipsv=`ip neighbor show`
#--Directorio de respaldo
#--Directorio de temporal
tmpcap="/utl/tmp/" 
#--Variables genericas
ologdir="/var/www/html/olog/"
ologmak="/var/www/html/"
putres=""
#--Variables de coneccion SQL
host="erpdesarrollo.portalito.com"
userx="desarrollo"
pap="p0rtAli70s"
db_x="olog_x"
#Directorio para archivo de respaldos
bdir="/weberp/procesos/backups/"
lpp=$bdir"lista_base_datos.txt"
lcc=$bdir"lista_base_datos_capa.txt"
lrr=$bdir"lista_base_datos_DES.txt"

function insded()
{
op=$1
nerp=$2
re='^[0-9]+$'
#inicio de funcion
#instalacion dedicada
headx "#" "Instalacion de Weberp Dedicado"
if ! [[ $op =~ $re ]] ; then
   headx "&" "No es valida la opcion, pruebe del 1 al 15 o escriba ./inst.sh para ver su uso -e1" >&2; exit 1
else
if [ $op -ge 1 ] && [ $op -le 15 ] ; then
	if [ $op -eq 1 ] ; then
		#Instala paqueteria base en servidor
		paq 
	fi
	if [ $op -eq 2 ] ; then
		#instala PHP
		iphp
	fi
	if [ $op -eq 3 ] ; then
		#instala ssl y ssh
		sslssh
	fi
	if [ $op -eq 4 ] ; then
		#instala smtp
		csmt $nerp
	fi
	if [ $op -eq 5 ] ; then
		#instala java
		ijava
	fi
	if [ $op -eq 6 ] ; then
		#instala Tomcat
		itom
	fi
	if [ $op -eq 7 ] ; then
		#instala IPTABLES
		inip
	fi
	if [ $op -eq 8 ] ; then
		#instala SNMP
		insnm
	fi
	if [ $op -eq 9 ] ; then
		#instala MYSQL
		inmy
	fi
	if [ $op -eq 10 ] ; then
		#instala FTP
		inftp
	fi
	if [ $op -eq 11 ] ; then
		#instala Ambiente de respaldos
		inback
	fi
	if [ $op -eq 12 ] ; then
		#modifica y verifica servicios
		invserv
	fi
	if [ $op -eq 13 ] ; then
		#instala SAT
		insat
	fi
	if [ $op -eq 14 ] ; then
		#instala Cron inicial <Posible uso de esta funcion en GUI para respaldos>
		incron
	fi
	if [ $op -eq 15 ] ; then
	#Instala Todo
	#Instala paqueteria base en servidor
	pausa "Precione [cualquier tecla] para continuar"
	paq 
	pausa "Precione [cualquier tecla] para continuar"
	#instala PHP
	iphp
	pausa "Precione [cualquier tecla] para continuar"
	#instala ssl y ssh
	sslssh
	pausa "Precione [cualquier tecla] para continuar"
	#instala smtp
	csmt $nerp
	pausa "Precione [cualquier tecla] para continuar"
	#instala java
	ijava
	pausa "Precione [cualquier tecla] para continuar"
	#instala Tomcat
	itom
	pausa "Precione [cualquier tecla] para continuar"
	#instala IPTABLES
	inip
	pausa "Precione [cualquier tecla] para continuar"
	#instala SNMP
	insnm
	pausa "Precione [cualquier tecla] para continuar"
	#instala MYSQL
	inmy
	pausa "Precione [cualquier tecla] para continuar"
	#instala FTP
	inftp
	pausa "Precione [cualquier tecla] para continuar"
	#instala Ambiente de respaldos
	inback
	pausa "Precione [cualquier tecla] para continuar"
	#modifica y verifica servicios
	invserv
	pausa "Precione [cualquier tecla] para continuar"
	#instala SAT
	insat
	pausa "Precione [cualquier tecla] para continuar"
	#instala Cron inicial <Posible uso de esta funcion en GUI para respaldos>
	incron
	fi
#Fin de funcion	
else
 headx "&" " no es valida la opcion, pruebe del 1 al 15 o escriba ./inst.sh para ver su uso"
fi
fi
}
function inscomp()
{
clear
erp=$1
iph=$2
#inicio de funcion
#instalacion compartida
headx "#" "Instalacion de Weberp Compartido"
#Buscar servicios activos http,Mysql,tomcat (Basicos)
headx "@" "Verificando Servicios activos" 
servs
#Crea las bases de datos en los servidores (Bases vacias)
dbinit $erp
########LLenado de las bases de datos Produccion y CAPA
mkdb $erp $iph
#Verificar carpetas de instalacion
headx "#" "Creando carpeta inicial de compañia"
makdir $erp "dis"
makdir $erp "discap"
makdir $erp "disdes"
#agrega a sistema de respaldos modificar por ologx
echo "$erp" >> $lpp
echo $erp"_CAPA" >> $lcc
exerem "echo $erp >> $lrr"
headx "#" "Se termino de realizar la instalacion de ERP en Servidor compartido nombre: $erp"
headx "@" "Pasos Siguientes:"
headx "#" "Ingresa a:desarrollo /controlcambios/sync_companies.php"
headx "#" "SERVIDORES>Implementaciones>Nueva"
headx "#" "Configurar 3 veces , 1 para produccion, 1 para _CAPA, 1 para Funciones, ejemplo"
headx "#" "userDB: soporte passDB: pr*mysql"
headx "#" "en capa se modifica el path por erpdistribucion_CAPA y Se crea DNS para acceso directo"
#Fin de funcion	
}

function mkdb ()
{
erp=$1
iph=$2
dbdes=$erp"_DES"
headx "#" "inicializa las bases de datos $erp: Producccion,Capa,Desarrollo"
#Verificar si las bases estan creadas

#Volcado de template de la base de datos
    headx "@" "Creando Contenido inicial de DB's"
	/utl/respaldo/bbck.sh rback /utl/newc/tp_in.tar.gz $erp "targz" "localhost" "root" "p0rtali70s"
	/utl/respaldo/bbck.sh rback /utl/newc/tp_in.tar.gz $erp"_CAPA" "targz" "localhost" "root" "p0rtali70s" 
#Esto crea el contenido inicial en la base de desarrollo, verificar que se tenga bbck.sh en /utl/respaldo/
#Puede tardar por los componentes de desarrollo
	#exerem "screen -dm -S trmx /utl/respaldo/bbck.sh udes $erp $iph $dbdes ${userx} ${pap}"
	exerem "screen -ls"
	headx "@" "Se creo proceso en screen en servidor de desarrolo, verifica ologx o el screen -r trmx para verificar el proceso"
#fin de llenado de la base de desarrollo
    headx "@" "Verificando usuario inicial de la base de datos $erp US:Admin"
#verifica el usuario en Admin (Comprobacion de volcado de datos a DB)
#verus host usuario password erp
verus "localhost" "root" "p0rtali70s" $erp
verus "localhost" "root" "p0rtali70s" $erp"_CAPA"
verus "162.252.87.133" "desarrollo" "p0rtAli70s" $erp"_DES"
headx "@" "Se creo contenido inicial verificar Base de desarrollo"
}

function dbinit()
{
#Funcion para crear bases de datos iniciales en Servidor de Produccion y Desarrollo
erpin=$1
#dbseek host usuario password db_name (regresa 0 o 1 si es 0 no existe la base)
#sqlex Crea las bases de datos en el servidor indicado
verf=$(dbseek "localhost" "root" "p0rtali70s" $erpin)
if [ $verf -eq 0 ] ; then
		headx "@" "DB produccion Existe"
		else
		headx "@" "Creando bases de datos"
		sqlex "localhost" "root" "p0rtali70s" $erpin
fi
verfc=$(dbseek "localhost" "root" "p0rtali70s" $erpin"_CAPA")
if [ $verfc -eq 0 ] ; then
			headx "@" "DB CAPA Existe"
			else
			headx "@" "Creando bases de datos"
			sqlex "localhost" "root" "p0rtali70s" $erpin"_CAPA"
fi
verfd=$(dbseek "162.252.87.133" "desarrollo" "p0rtAli70s" $erpin"_DES")
if [ $verfd -eq 0 ] ; then
			headx "@" "DB Desarrollo Existe"
			else
			headx "@" "Creando bases de datos"
			sqlex "162.252.87.133" "desarrollo" "p0rtAli70s" $erpin"_DES"
fi
}

function makdir()
{
# Funcion para crear directorio de y carpetas de codigo segun el tipo de instalacion y requerimientos, solo crea erpdistribucion
# en caso de requerir que se llame diferente hacer manual la instalacion o renombrar la instalacion
erp=$1
op=$2
cd /var/www/html/
dire="erpdistribucion"
direc="erpdistribucion_CAPA"
comp="companies"
headx "@" "Verificando Directorios"
if [ "$op" == "dis" ]; then
if [ -d $dire ]; then
	headx "@" "Se encontro directorio de instalacion [erpdistribucion]"
	cd "erpdistribucion"
	else
	headx "@" "No se encontro directorio de instalacion [erpdistribucion] desea crearlo s/n o [all] para cargar toda la estrutura de erpdistribucion "
	read rsp
	case $rsp in
			s)
			mkdir erpdistribucion
			headx "@" "Creado"
			cd erpdistribucion
			;; 
			all)
			tar -xvzf /utl/newc/erpdistribucion_base.tar.gz
			mv erpdistribucion_base erpdistribucion
			cd erpdistribucion
			headx "@" "Instalacion base completa"
			;;
			n)
			exit 3
			;;
			*)
			headx "&" "Opcion no valida"
			exit 3
			;;
	esac
fi
		if [ -d $comp ]; then
			headx "@" "Se encontro directorio de instalacion [companies]"
			cd "companies"
			else
			headx "@" "No se encontro directorio de instalacion [companies] desea crearlo s/n o [x] salir"
			read rsp
				case $rsp in
					s)
					mkdir companies
					headx "@" "Creado"
					cd companies
					;;
					n)
					exit 3
					;;
						*)
			headx "&" "Opcion no valida"
			exit 3
			;;
				esac
		fi
		if [ -d $erp ]; then
			headx "@" "Se encontro directorio de instalacion [$erp] desea sobre escribir s/n"
			read rspd
				case $rspd in
					s)
					headx "@" "Copiando carpeta inicial $erp"
					cp -rp /utl/newc/inicp/* /var/www/html/erpdistribucion/companies/$erp
					headx "@" "Se realizo la copia verificar"
					cd $erp
					ls -lh
					;; 
					n)
					headx "@" "No se modifica carpeta"
					cd $erp
					ls -lh
					;;
					*)
						headx "&" "Opcion no valida"
						exit 3
						;;
				esac
			else
			headx "@" "No se encontro directorio de instalacion [$erp] desea crearlo s/n o [x] salir"
			read rsp
				case $rsp in
					s)
					mkdir $erp
					headx "@" "Copiando carpeta inicial"
					cp -rp /utl/newc/inicp/* /var/www/html/erpdistribucion/companies/$erp
					headx "@" "Se realizo la copia verificar"
					cd $erp
					ls -lh
					;;
					n)
					exit 3
					;;
						*)
			headx "&" "Opcion no valida"
			exit 3
			;;
				esac
		fi
fi
if [ "$op" == "discap" ]; then		
if [ -d $direc ]; then
	headx "@" "Se encontro directorio de instalacion [erpdistribucion_CAPA]"
	cd "erpdistribucion_CAPA"
	else
		headx "@" "No se encontro directorio de instalacion [erpdistribucion_CAPA] desea crearlo s/n o [all] para cargar toda la estrutura de erpdistribucion_CAPA"
	read rsp
	case $rsp in
			s)
			mkdir erpdistribucion_CAPA
			headx "@" "Creado"
			cd erpdistribucion_CAPA
			;;
			all)
			tar -xvzf /utl/newc/erpdistribucion_base.tar.gz
			mv erpdistribucion_base erpdistribucion_CAPA
			cd erpdistribucion_CAPA
			headx "@" "Instalacion base_CAPA completa"
			;;
			n)
			exit 3
			;;
				*)
			headx "&" "Opcion no valida"
			exit 3
			;;
	esac
fi
		if [ -d $comp ]; then
			headx "@" "Se encontro directorio de instalacion [companies]"
			cd companies
			else
			headx "@" "No se encontro directorio de instalacion [companies] desea crearlo s/n o [x] salir"
			read rsp
				case $rsp in
					s)
					mkdir companies
					headx "@" "Creado"
					cd companies
					;; 
					n)
					exit 3
					;;
						*)
			headx "&" "Opcion no valida"
			exit 3
			;;
				esac
		fi
		if [ -d $erp"_CAPA" ]; then
			headx "@" "Se encontro directorio de instalacion [$erp"_CAPA"] desea sobre escribir s/n"
				read rspc
				case $rspc in
					s)
					headx "@" "Copiando carpeta inicial $erp"_Capa""
					cp -rp /utl/newc/inicp/* /var/www/html/erpdistribucion_CAPA/companies/$erp"_CAPA"
					headx "@" "Se realizo la copia verificar"
					cd $erp"_CAPA"
					ls -lh
					;; 
					n)
					headx "@" "No se modifica carpeta"
					cd $erp"_CAPA"
					ls -lh
					;;
					*)
					headx "&" "Opcion no valida"
					exit 3
					;;
				esac
			else
			headx "@" "No se encontro directorio de instalacion [$erp"_CAPA"] desea crearlo s/n o [x] salir"
			read rsp
				case $rsp in
					s)
					mkdir $erp"_CAPA"
					headx "@" "Copiando carpeta inicial"
					cp -rp /utl/newc/inicp/* /var/www/html/erpdistribucion_CAPA/companies/$erp"_CAPA"
					headx "@" "Se realizo la copia verificar"
					cd $erp"_CAPA"
					ls -lh
					;; 
					n)
					exit 3
					;;
						*)
			headx "&" "Opcion no valida"
			exit 3
			;;
				esac
		fi
fi
if [ "$op" == "disdes" ]; then		
	headx "@" "Copiando carpeta inicial"
	exerem "mkdir /var/www/html/erpdistribucion/companies/$erp"_DES""
	exerem "cp -rp /utl/newc/inicp/* /var/www/html/erpdistribucion/companies/$erp"_DES""
	exerem "ls -lh /var/www/html/erpdistribucion/companies/$erp"_DES""
	headx "@" "Se realizo la copia favor de verificar"
fi

	
	
}


function servs()
{
#Buscar servicios activos http,Mysql,tomcat (Basicos)
stom=$(ps aux | grep "tomcat" | grep -v grep | grep -v bbck.sh | wc -l)
#Estos servicios se leen diferente a tomcat por que tomcat no regresa estatus desde init.d
httst=$(/etc/init.d/httpd status | grep "running" | grep -v grep | wc -l)
misst=$(/etc/init.d/mysqld status | grep "running" | grep -v grep | wc -l)
if [ $stom -eq 0 ] || [ $httst -eq 0 ] || [ $misst -eq 0 ] ; then
	misst=$(ps -e | grep "mysql" | grep -v grep | grep -v inst.sh | wc -l)
	httst=$(ps -e | grep "http" | grep -v grep | grep -v inst.sh | wc -l)
	stom=$(ps aux | grep "tomcat" | grep -v grep | grep -v inst.sh | wc -l)
	if [ $stom -eq 0 ] || [ $httst -eq 0 ] || [ $misst -eq 0 ] ; then	
			 headx "@" "Algun servicio basico no se esta ejecutando favor de verificar el que esta en 0 "
			 headx "&" "Servicios en ejecucion: [Tomcat:$stom, Http:$httst, Mysql:$misst]"
			 headx "&" "se detiene la instalacion"
			 exit 3
		else
			 headx "@" "Servicios OK"
			 headx "&" "Servicios en ejecucion: [Tomcat:$stom, Http:$httst, Mysql:$misst]"
			 headx "&" "continua la instalacion"
	fi
else
			 headx "@" "Servicios OK"
			 headx "&" "Servicios en ejecucion: [Tomcat:$stom, Http:$httst, Mysql:$misst]"
			 headx "&" "continua la instalacion"
fi
}

function headx()
{
	#Funcion para crear Promt segun el tipo de aviso
	#inicio de funcion
	dat=$2
	tp=$1
#Colores de texto
	red=`tput setaf 1`
	greenb=`tput setab 2`
    green=`tput setaf 2`
    yellow=`tput setaf 3`
    blue=`tput setaf 4`
    magenta=`tput setaf 5`
    cyan=`tput setaf 6`
    white=`tput setaf 7`
    reset=`tput sgr0`
    alert=`tput bel`
#fin de colores	
	if [ "$tp" == "#" ] ; then
			echo "${green}  #####################################################################################${reset}"
			echo "${yellow}#--[]-> ${green}    $dat ${reset}"
			echo "${green}  #####################################################################################${reset}"
		elif [ "$tp" == "@" ] ; then
			echo "${white}#--[]-> ${cyan}$dat ${reset}"	
		elif [ "$tp" == "&" ] ; then
			echo "${alert}${red}${greenb}#--[]-> $dat ${reset}"	
		else
			echo "${yellow}## --[]-> # $dat ${reset}"
	fi		             
#Fin de funcion	
}

function dbseek(){
#Inicio de funcion
#dbseek host usuario password db_name
#funcion para verificar la existencia de DB
seekh=$1
seeku=$2
seekp=$3
seekdb=$4
seek=`mysql --host=$seekh --user=$seeku --password=$seekp << END
 use $seekdb -A;
END` 
if [ $? -eq 0 ] ;
then
	echo 0
else
	echo 1
fi

#fin de funcion
}

sqlex(){
#Inicio de funcion
#sqlex host usuario password erp
#Funcion para crear las bases de datos por host
s_h=$1
s_u=$2
s_p=$3
s_erp=$4
xoff=`mysql --host=$s_h --user=$s_u --password=$s_p << END
 create database $s_erp;
END` 
if [ $? -eq 0 ] ;
then
	headx "#" "Se creo correctamente la base de datos $s_erp en $s_h"
else
	headx "&" "No se creo correctamente la base de datos $s_erp en $s_h verificar"
fi
#fin de funcion
}

sqlrun(){
#Inicio de funcion
#sqlrun host usuario password erp
#Funcion para crear las bases de datos por host
wcm=$1
runn=`misql << END
  $wcm
END` 
if [ $? -eq 0 ] ;
then
	headx "#" "Se ejecuto correctamente la consulta: $wcm"
else
	headx "&" "Error en la consulta: $wcm"
fi
#fin de funcion
}


function verus(){
#Inicio de funcion
#verus host usuario password erp
s_h=$1
s_u=$2
s_p=$3
s_erp=$4
#funciona para validar existencia de DB segun host extrae usuario admin para verificar acceso
xoff=`mysql --host=$s_h --user=$s_u --password=$s_p << END
 use $s_erp -A;
 select userid,password,realname,email from www_users where userid like "admin"
END` 
if [ $? -eq 0 ] ;
then
	headx "#" "Datos: de base $s_erp"
	headx "@" "$xoff"
	headx "#" "Fin datos $s_erp"
else
	headx "&" "Error al ingresar a DB $s_erp"
	headx "@" "$xoff"
	headx "&" "Favor de verificar"
fi
#fin de funcion
}
function exerem ()
{
	#funcion para ejecutar udes remoto al servidor de desarrollo
	#exerem  [erp] [iplocal]
HOST="162.252.87.133"
CMD=$1
USER="root"
PASS="e9adc26832"
VAR=$(expect -c "
match_max 10000000
spawn ssh -o StrictHostKeyChecking=no $USER@$HOST $CMD
expect \"*?assword:*\"
send -- \"$PASS\r\"
send -- \"\r\"
expect eof
")
echo "==============="
echo "$VAR"
}
#Instalacion en servidor dedicado de aqui para abajo
#Se separo en funciones por posibles errores y para agregar pausas en la instalacion
function paq ()
{
	#Instalacion de paqueteria base
	#Horario
	headx "#" "Configuracion de horario"
	TZ='America/Mexico_City'; export TZ
	/usr/sbin/ntpdate -s north-america.pool.ntp.org
	date ; TZ='America/Mexico_City'; export TZ ; rm -f /etc/localtime ; ln -s /usr/share/zoneinfo/America/Mexico_City /etc/localtime ; /usr/sbin/ntpdate -s north-america.pool.ntp.org ; hwclock --systohc ; date ; hwclock
	#Paqueteria sistema operativo
	headx "@" "Actualizacion del sistema"
	yum clean all
	yum update
	#Paqueteria Para servidor web
	headx "#" "Instalacion de paqueteraias"
	yum install -y php 
	yum install -y php-mysql 
	yum install -y php-snmp 
	yum install -y php-gd 
	yum install -y php-gettext
	yum install -y php-xml
 	yum install -y php-pecl-http 
	yum install -y php-pecl-apc-devel 
	yum install -y libmysqlclient15-dev 
	yum install -y rrdtool 
	yum install -y librrds-perl 
	yum install -y libconfig-inifiles-perl 
	yum install -y libcrypt-des-perl 
	yum install -y libdigest-hmac-perl 
	yum install -y torrus-common 
	yum install -y libgd-gd2-perl 
	yum install -y snmp 
	yum install -y snmpd 
	yum install -y libnet-snmp-perl 
	yum install -y libsnmp-perl 
	yum install -y libgd2-xpm 
	yum install -y libgd2-xpm-dev 
	yum install -y libpng12-dev 
	yum install -y phpMyAdmin 
	yum install -y subversion 
	yum install -y ftp 
	yum install -y nmap 
	yum install -y cpan  
	yum install -y net-snmp-utils 
	yum install -y net-snmp leafpad 
	yum install -y chromium-browser 
	yum install -y lsb-release 
	yum install -y sendmail 
    yum install -y postfix 
	yum install -y mailutils 
	yum install -y build-essential 
	yum install -y apache2 
	yum install -y apache2-mpm-prefork 
	yum install -y php-xml 
	yum install -y php-pear 
	yum install -y php-ldap 
	yum install -y php-soap 
	yum install -y mysql-server  
	yum install -y wget 
	yum install -y httpd  
	yum install -y htop  
	yum install -y gcc 
	yum install -y screen 
	yum install -y git
	yum install -y git
	yum install -y nano
	yum install -y ntp 
	yum install -y ntpdate 
	yum install -y ntp-doc
	yum install -y postfix 
	yum install -y cyrus-sasl-plain 
	yum install -y mailx
	yum install -y htop
	yum install -y mtr
	yum install -y expect
	yum install -y iftop
	yum install -y p7zip p7zip-plugins
	yum install -y iptables-services
	scp -rp pkg.portalito.com:/respaldo/data/instalacion/weberp /.
#fin de funcion 
}


function iphp ()
{
	#instalacion de PHP
	headx "@" "Instalacion de PHP"
	wget http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm 
	rpm -ivh epel-release-6-8.noarch.rpm
	wget http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
	rpm -Uvh remi-release-6*.rpm
	yum --enablerepo=remi upgrade php-mysql php-devel php-gd php-pecl-memcache php-pspell php-snmp php-xmlrpc php-xml
	#instalacion de SSL
}
function sslssh()
{
	headx "@" "Instalacion de SSL"
	yum install openssl openssl-devel mod_ssl
	pecl install stomp
	#Ajuste SSH
	headx "@" "SSH"
	cp /etc/ssh/sshd_config /etc/ssh/sshd_config_BAK
	rm -f /etc/ssh/sshd_config
	scp -P5555 root@162.252.87.133:/etc/ssh/sshd_config /etc/ssh/sshd_config
	/etc/init.d/sshd restart
	#Ajuste verificar nombre de host
	headx "@" "	Verifica nombre de host 127.0.0.1 localhost.localdomain -> 127.0.0.1 localhost"
	less /etc/hosts
}
function csmt()
{
	inerp=$1
	headx "@" "Configuracion de SMTP"
	/etc/init.d/sendmail stop
	chkconfig --del sendmail
	cp /etc/postfix/main.cf /etc/postfix/main.cf_BAK
	pot="/etc/postfix/main.cf"
	echo "relayhost             = [smtp.gmail.com]:587" >> $pot
	echo "smtp_use_tls                    = yes" >> $pot
	echo "smtp_sasl_auth_enable           = yes" >> $pot
	echo "smtp_sasl_password_maps         = hash:/etc/postfix/sasl_passwd" >> $pot
	echo "smtp_tls_CAfile                 = /etc/ssl/certs/ca-bundle.crt" >> $pot
	echo "smtp_sasl_security_options      = noanonymous" >> $pot
	echo "smtp_sasl_tls_security_options  = noanonymous" >> $pot
	echo "[smtp.gmail.com]:587 notificaciones@portalito.com:notificaciones01" > /etc/postfix/sasl_passwd
	postmap /etc/postfix/sasl_passwd
	chown root:postfix /etc/postfix/sasl_passwd*
	chmod 640 /etc/postfix/sasl_passwd*
	postfix stop
    postfix start
    echo "Mail de prueba $inerp se instalo y configuro postfix." | mail -s "Servidor nuevo $inerp" alejandro.rico@onaxis.mx
 }
 
function ijava()
 {	
	headx "@" "Instalacion de JAVA"
	cd /weberp/java
	tar -xzf jre1.7.0_40.tar.gz
	ln -s /weberp/java /usr/java
	cd /usr/java
	JAVA_HOME=/usr/java/jre1.7.0_40
	export JAVA_HOME 
	PATH=$JAVA_HOME/bin:$PATH 
	export PATH
	headx "@" "Instalacion de terminada"
}

function itom()
{
	headx "@" "Instalacion de Apache Tomcat"
	cd /weberp/
	tar -xzf apache-tomcat-7.0.42.tar.gz
	ln -s /weberp/apache-tomcat-7.0.42 /usr/share/apache-tomcat-7.0.42
	cd /usr/share/apache-tomcat-7.0.42/
	rm -rf /usr/share/apache-tomcat-7.0.42/logs*
	cd /etc/init.d
	groupadd tomcat
	useradd -g tomcat -d /usr/share/apache-tomcat-7.0.42/webapps tomcat
	chown -Rf tomcat:tomcat /usr/share/apache-tomcat-7.0.42
	cp tomcat tomcat_BAK
	touch tomcat
	tomfile="tomcat"
	pesos="$"
	echo "#!/bin/bash" >> $tomfile
	echo "# description: Tomcat Start Stop Restart " >> $tomfile
	echo "# processname: tomcat MOD:Inst1" >> $tomfile
	echo "# chkconfig: 234 20 80" >> $tomfile
	echo "JAVA_HOME=/usr/java/jre1.7.0_40" >> $tomfile
	echo "export JAVA_HOME" >> $tomfile
	echo "PATH="$pesos"JAVA_HOME/bin:"$pesos"PATH" >> $tomfile
	echo "export PATH" >> $tomfile
	echo "CATALINA_HOME=/usr/share/apache-tomcat-7.0.42/bin" >> $tomfile
	echo "case "$pesos"1 in" >> $tomfile
	echo "start) " >> $tomfile
	echo "/bin/su tomcat" $pesos"CATALINA_HOME/startup.sh " >> $tomfile
	echo ";;" >> $tomfile
	echo "stop)" >> $tomfile
	echo "/bin/su tomcat "$pesos"CATALINA_HOME/shutdown.sh " >> $tomfile
	echo ";;" >> $tomfile
	echo "restart)" >> $tomfile
	echo "/bin/su tomcat "$pesos"CATALINA_HOME/shutdown.sh" >> $tomfile
	echo "/bin/su tomcat "$pesos"CATALINA_HOME/startup.sh " >> $tomfile
	echo ";;" >> $tomfile
	echo "esac" >> $tomfile
	echo "exit 0" >> $tomfile
	mkdir /var/log/tomcat
	chown tomcat:tomcat /var/log/tomcat
	chmod 775 /var/log/tomcat 
	cd /usr/share/apache-tomcat-7.0.42/
	ln -s /var/log/tomcat logs
	tomlog="/usr/share/apache-tomcat-7.0.42/logs/catalina.out"
	echo "#Log Tomcat" >> $tomlog
}

function inip()
{
	headx "@" "Configuracion de IP Tables (Firewall)"
	systemctl stop firewalld
	systemctl mask firewalld
	systemctl enable iptables
	iptables -I INPUT -p tcp --dport 80 -j ACCEPT
	iptables -I INPUT -p tcp --dport 81 -j ACCEPT
	iptables -I INPUT -p tcp --dport 8080 -j ACCEPT
	iptables -I INPUT -p tcp --dport 22 -j ACCEPT
	iptables -I INPUT -p tcp --dport 522 -j ACCEPT
	iptables -I INPUT -p tcp --dport 21 -j ACCEPT
	iptables -I INPUT -p tcp --dport 61616 -j ACCEPT
	iptables -I INPUT -p tcp --dport 5672 -j ACCEPT
	iptables -I INPUT -p tcp --dport 61613 -j ACCEPT
	iptables -I INPUT -p tcp --dport 1883 -j ACCEPT
	iptables -I INPUT -p tcp --dport 61614 -j ACCEPT
	iptables -I INPUT -p tcp --dport 8161 -j ACCEPT
	iptables -I INPUT -p tcp --dport 443 -j ACCEPT
	iptables -I INPUT -p tcp --dport 199 -j ACCEPT
	iptables -I INPUT -p tcp --dport 161 -j ACCEPT
	iptables -I INPUT -p tcp --dport 162 -j ACCEPT
	iptables -I INPUT -p tcp --dport 3306 -j ACCEPT
	iptables -I INPUT -p tcp --dport 10000 -j ACCEPT
	iptables -I INPUT -p tcp --dport 26514 -j ACCEPT
	iptables -I INPUT -p tcp --dport 8161 -j ACCEPT
	service iptables save
	/usr/libexec/iptables/iptables.init save
}

function insnm()
{
	headx "@" "Configuracion de SNMP Centreon"
	smfile="/etc/snmp/snmpd.conf"
	clm='"'
	Nser=$2
	mv /etc/snmp/snmpd.conf /etc/snmp/snmpd.conf.org
	touch /etc/snmp/snmpd.conf
	echo "rocommunity  portalito" >> $smfile
	echo "syslocation $clm$Nser, Hivelocity$clm" >> $smfile
	echo "syscontact  soporte@onaxis.mx" >> $smfile
}

function inmy()
{
	headx "@" "Configuracion de Mysql"
	touch /bin/misql
	miq="/bin/misql"
	mc='"'
	peso="$"
	echo "mysql -u root --password=$mcp0rtali70s$mc --max_allowed_packet=2048M " $peso"1" $peso"2" $peso"3" $peso"4" $peso"5" >> $miq
	touch /bin/misqldump
	mip="/bin/misqldump"
	echo "mysqldump -u root --password=$mcp0rtali70s$mc --max_allowed_packet=2048M" $peso"1" $peso"2" $peso"3" $peso"4" $peso"5" >> $mip
	chmod +x /bin/misql*
	mkdir /var/log/weberp
	ln -s /var/log/weberp/ /logs
	mkdir /logs/mysql
	chown mysql:mysql /logs/mysql
	chmod 755 /logs/mysql
	mkdir /var/lib/tmp
	chmod 777 /var/lib/tmp
	mv /etc/my.cnf /etc/my.cnf_org
	headx "#" "Crear archivo my.cnf percona https://tools.percona.com/sign-in"
	headx "@" "incluir logs en el archivo /etc/my.cnf"
	headx "@" "log-error = /logs/mysql/mysql-error.log
				slow-query-log-file = /logs/mysql/mysql-slow.log"
	headx "@" "para guardar en vi preciona [esq][:][qw][enter]"
	pausa "Preciona enter para continuar y editar my.cnf"
	vi /etc/my.cnf
	pausa "Preciona enter para continuar con la configuracion de Mysql"
	chkconfig --add mysqld 
	chkconfig --level 234 mysqld on
	chkconfig --list mysqld
	/etc/init.d/mysqld start
	sqlrun "GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' IDENTIFIED BY 'p0rtali70s';GRANT ALL PRIVILEGES ON *.* TO 'soporte'@'%' IDENTIFIED BY 'pr*mysql';GRANT ALL PRIVILEGES ON *.* TO 'desarrollo'@'%' IDENTIFIED BY 'p0rtAli70s';GRANT ALL PRIVILEGES ON *.* TO 'root'@'ec2-54-200-224-143.us-west-2.compute.amazonaws.com' IDENTIFIED BY 'pr*soport3';flush privileges;"
}

function inftp()
{
	headx "#" "Instalacion y configuracion de FTP"
	scp -rp pkg.portalito.com:/etc/init.d/erp-pure-ftpd /etc/init.d/erp-pure-ftpd
	ln -s /etc/init.d/erp-pure-ftpd /bin/erp-pure-ftpd
	scp -rp pkg.portalito.com:/bin/erp-ftp* /bin/.
	chmod +x /bin/erp-ftp*
	cd /weberp/
	tar zxf pure-ftpd-1.0.21.tar.gz
	cd pure-ftpd-1.0.21
	mkdir -p /work/ftp/home
	groupadd weberp
	useradd ftpvirtual -g weberp -s /bin/false
	chown root:weberp -R /work/ftp/home
	cd /work/ftp/home
	cd /weberp/pure-ftpd-1.0.21/
	./configure --with-puredb --with-ratios --with-language=spanish --with-ftpwho --without-banner --with-tls
	make;make install;make install-strip
	chmod -x /etc/init.d/erp-pure-ftpd
	chmod +x /bin/erp-ftp-*
	chmod +x /etc/init.d/erp-pure-ftpd
	mkdir -p /etc/ssl/private/
	openssl req -x509 -nodes -newkey rsa:1024 -keyout   /etc/ssl/private/pure-ftpd.pem   -out /etc/ssl/private/pure-ftpd.pem
	/etc/init.d/erp-pure-ftpd start
	headx "@" "Prueba de configuracion de ftp"
	erp-ftp-useradd test test test
	headx "@" "Elimina el usuario FTP-Test"
	erp-ftp-userdel test /work/ftp/home/test/
	headx "#" "Configuracion de Sftp"
	headx "@" "copia lo siguiente y sin el simbolo modifica el archivo:"
	headx "@" " #line 132:Comenta esta linea con #:
				#Subsystem sftp /usr/libexec/openssh/sftp-server
				#Agrega esta linea debajo de la que comentaste 
				Subsystem sftp internal-sftp
				#Agrega esto al final del archivo
				Match Group weberp
				X11Forwarding no
				AllowTcpForwarding no
				ChrootDirectory /work/ftp/home
				ForceCommand internal-sftp"
	cp /etc/ssh/sshd_config /etc/ssh/sshd_config_BAK
	pausa "Preciona [Enter] para modificar /etc/ssh/sshd_config"
	vi /etc/ssh/sshd_config
	pausa "Preciona [Enter] para reiniciar el servicio sshd"
	/etc/rc.d/init.d/sshd restart
}

function inback()
{
	headx "#" "Configuracion de entorno de respaldo 2015"
	mkdir -p /respaldo/data/db/
	mkdir -p /respaldo/data/codigo/
	mkdir -p /respaldo/data/cfds/
sqlrun "CREATE DATABASE controlc; use controlc; CREATE TABLE BACKUPS (idBACKUP int(10) unsigned NOT NULL auto_increment,DB varchar(255) NOT NULL default '' ,FECHA timestamp NOT NULL default CURRENT_TIMESTAMP,ESTADO varchar(255) NOT NULL,RUTA varchar(255) NOT NULL,TAMANIO int(10) unsigned default NULL,HORA_GENERACION time default '00:00:00',PRIMARY KEY (idBACKUP)) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
headx "#" "Configur acion de PHP.ini"
headx "@" "Copia esto y modificalo en el php.ini"
headx "@" "		short_open_tag = On
			max_execution_time = -1
			max_input_vars = 45000
			memory_limit = 1024M
			post_max_size = 550M
			upload_max_filesize = 16M
			allow_url_include = On
			extension=stomp.so
			date.timezone = America/Mexico_City
			upload_tmp_dir="/tmp""
pausa "Preciona [enter] despues de copiar para modificar el php {No copies y pegues, modifica los valores de las variables o agrega en caso de que no esten}"
vi /etc/php.ini
headx "#" "Variables de Perl"
cd ~
touch .bash_profile
bh=".bash_profile"
echo "export LANGUAGE=en_US.UTF-8" >> $bh
echo "export LANG=en_US.UTF-8" >> $bh
echo "export LC_ALL=en_US.UTF-8" >> $bh
}

function invserv()
{
headx "#" "Verificacion de servicios"
chmod 755 /etc/init.d/postfix
chkconfig --add postfix
chkconfig --level 234 postfix on
chkconfig --list postfix
/etc/init.d/postfix restart
 
chmod 755 /etc/init.d/httpd
chkconfig --add httpd
chkconfig --level 234 httpd on
chkconfig --list httpd
/etc/init.d/httpd start
 
chmod 755 /etc/init.d/tomcat
chkconfig --add tomcat
chkconfig --level 234 tomcat on
chkconfig --list tomcat
/etc/init.d/tomcat start
 
chkconfig --add snmpd 
chkconfig --level 234 snmpd on
chkconfig --list snmpd
/etc/init.d/snmpd start
}

function insat()
{
headx "#" "Configuracion de SAT"
erp-ftp-useradd SAT passSAT01 SAT
mkdir -p /work/ftp/home/SAT/procesados
mkdir -p /work/ftp/home/SAT/errores
mkdir -p /work/ftp/home/SAT/tmp
scp -P 5555 erpdesarrollo.portalito.com:/respaldo/data/db/SAT.gz /respaldo/data/db/SAT.gz
scp -rp -P 5555 erpdesarrollo.portalito.com:/weberp/procesos/download_cert_sat.sh /weberp/procesos/download_cert_sat.sh
scp -P 5555 erpdesarrollo.portalito.com:/work/ftp/home/SAT/procesados/CSD.txt /work/ftp/home/SAT/.
echo "create database SAT" |misql
zcat /respaldo/data/db/SAT.gz |misql SAT
}

function incron()
{
headx "#" "Crontabs"
fcro="/utl/crons/conwo/cow"
mkdir /utl/crons/
mkdir /utl/crons/conwo/
cat /dev/null > /utl/crons/conwo/cow
touch /utl/crons/conwo/cow
crontab -l > $fcro
cm='"'
echo "#Portalito
0 * * * * /usr/sbin/ntpdate -s north-america.pool.ntp.org
00 00 * * * /weberp/procesos/backups/erpbackups full
30 00 * * * /weberp/procesos/backups/erpbackups codigo ; /bin/crontab  -l > /respaldo/system/crontab
00 03 * * * /weberp/procesos/backups/erpbackups capa
00 05 * * * /weberp/procesos/backups/erpbackups limpieza
00 01 * * * /weberp/procesos/download_cert_sat.sh downloadSAT ftp://ftp2.sat.gob.mx/agti_ftp/cfds_ftp CSD.txt ; /weberp/procesos/download_cert_sat.sh updateSAT" >> $fcro
crontab $fcro 
headx "#" "ejecute ./inst goxc [erp] [ip del servidor local]"
headx "#" "Se termino la preparacion de ambiente configure [linea de codigo]"
pausa "Precione [Enter] para finalizar"
}

#Fin de instalacion en servidor dedicado


function pausa()
{
#Funcion para pausar la instalacion asta recibir un enter
   read -p "$*"
}

#--Variables de entrada --Incluir directorio final ERP 	
case $1 in
	go)
	 #Entrada de respaldo completo rcodcomp (directorio erp) 
	 	insded $2 $3
	 exit 3
	 ;; 
	 goxc)
	 #Entrada de respaldo completo rcodcomp (directorio erp) 
	 	inscomp $2 $3 $4
	 exit 3
	 ;; 
	 toto)
	 	#Prueba de toto
	 	putff $2 $3
	  exit 3
	 ;;
	 *)
	 #Entrada de Modo de uso
	 	clear
	 	echo "#####################################################################################"
	 	echo "#########                Alejandro Rico E. Agosto 2016                      #########"
	 	echo "####### Servidor Dedicado:    ./inst.sh go [opcion {1-15}][erp]                #########"
	 	echo "####### Servidor Compartido:  ./inst.sh goxc [erp] ip                             ######"
	 	echo "####### {opciones de [go]}                                                     ######"
		echo "              # 1 Instala paqueteria base en servidor y horario
              # 2 instala PHP
              # 3 instala ssl y ssh
              # 4 instala smtp
              # 5 instala java
              # 6 instala Tomcat
              # 7 instala IPTABLES
              # 8 instala SNMP
              # 9 instala MYSQL
              # 10 instala FTP
              # 11 instala Ambiente de respaldos
              # 12 modifica y verifica servicios
              # 13 instala SAT
              # 14 instala Cron inicial
              # 15 Instala Todo                                                                     "
		echo "#######                                                                             #"
	 	echo "#####################################################################################"
	 	echo "-------------------------------------------------------------------------------------"
        exit 3 
		;;
esac