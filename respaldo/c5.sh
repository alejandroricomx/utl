#!/bin/bash
# Envio de respaldos por emergencia a local
#Elaborado por Alejandro Rico E. 
#///
#set -xv
#Obtencion de variables globales
dres="/respaldo/data/db"
if [ -d $dres ] ; then
	cd $dres
	find $dres -type f -mtime -1 >envio.txt
	for i in $(cat envio.txt); do
	if [ -f $i ] ; then
		echo "Archivo a subir -> $i"
		#scp -P211  erppisumma_08Sep2017.gz root@187.189.80.166:/respaldo/data/db/c5
		rsync --partial --progress --rsh="ssh -p 211" $i toor@187.189.80.166:/respaldo/data/db/c5  #Envio de archivos de respaldo xssh
		if [ ${PIPESTATUS[0]} -ne 0 ] ; then
			echo "Error con rsync $i"
		else
			echo "rsync OK $i"
    	fi
	else
		echo "No se encontro archivo $i no se envio"
 	fi
	done
 else
 	echo "No se encuentra directorio"
 fi