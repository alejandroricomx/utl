#!/bin/bash
#
#Elaborado por Alejandro Rico E. 
#///
#set -xv
#Obtencion de variables globales
my_dir=$(cd `dirname $0` && pwd)
source $my_dir"/conf.xila"
#obtencion de funciones globales
source $my_dir"/xtras.xila"
# inicio de funciones main
function rcodcomp(){
#inicio de funcion
	derp=${dorig}$1"/"
	cod=$1
	ret="1" #Variable de categoria de retorno para funcion savedb
	subir=$2
	tarf="bkc_"$cod"_"${chh}".tar.gz" #Nombre del archivo a crear
	#--creacion de carpeta olog para guardar log de respaldos
	ologcd="${ologdir}$cod$chh.inf"
	mklogd
if [ -d $derp ] ; then
		clear
		headx "#" "Iniciando respaldo de codigo"
		echo "<br>######################### Iniciando de respaldo de codigo $cod ################# $chh<br>" >> $ologcd
		headx "@" "Iniciando copiado de carpeta -> $derp"
		headx "@" "Copiando....." 
		rsync -vazh $derp ${bkcod_dir}$cod >> $ologcd
		headx "@" "Copia terminada en: ${bkdb_dir}$cod" 
		echo  "<br>#####################  	   Copia terminada 	    ################# $chh<br>" >> $ologcd
		headx "@" "Copia terminada"
		cd ${bkcod_dir}		
		ls -lthogp >> $ologcd
		echo "<br>############# Iniciando Comprecion de respaldo $tarf ############ $chh<br>" >> $ologcd
		headx "@" "Iniciando Comprecion de respaldo $tarf"
		tar -c $cod |gzip > $tarf
		#validacion para guardar respaldo por mes
		unoxmes "${bkcod_dir}$tarf" "od" "$cod"
		# unoxmes Archivo con directorio "opcion db o od" "erp o base"
		cd ${bkcod_dir}	
		ls_f="bkc_"$cod"_""*"
		grep_f="bkc_"$cod"_"
		ctu=`ls $ls_f | grep $grep_f | wc -l`
		ls -lthogp >> $ologcd
		#Subir a resguardo
		if [ -f $tarf ] ; then
			headx "#" "Archivo $tarf encontrado"
				if [ "$subir" = "S" ] ; then
					  		headx "@" "Archivo a subir -> $tarf Opcion: FTP o SFTP" 
				  			putff "$tarf" $3 
				else
				  			echo "<br><br>No se selecciono subir archivo <br> FTP" >> $ologdb
				  			headx "&" "No se selecciono subir archivo FTP"
				fi
				  		
				if [ "$subir" = "z" ] ; then
					  		headx "@" "Archivo a subir -> $tarf Opcion: SSH"
				  			putcot "$tarf" $3 
				else
				  			echo "<br><br>No se selecciono subir archivo <br> Cotemar" >> $ologdb
				  			headx "&" "No se selecciono subir archivo Cotemar"
				fi	
		else
			headx "#" "Archivo $tarf No encontrado error al subir codigo a respaldo"
		fi		
	else
		echo "<br>No, no existe; $derp favor de verificar nombre de Directorio ·No se realizo Respaldo $chh<br>" >> $ologcd	
		headx "&" "No, no existe; $derp favor de verificar nombre de Directorio ·No se realizo Respaldo $chh"
		ret="7"
		savedb "<br>No, no existe; $derp favor de verificar nombre de Directorio ·No se realizo Respaldo $chh<br> $1" "$cc" "${putres}" "$ret"
	fi
        headx "#" "Iniciando Grabando base de datos"
        if [ -f $ologcd ];
		then
		 echo "->          Iniciando... "
         cc=`cat $ologcd`	
		 #echo "paso : Var1 "$1"  var2 "$cc" var3 "${putres}" var4 "$ret" "
		 #savedb erp log infadd cat
		 savedb "Respaldo Codigo: $1 Respaldos actuales de $1: $ctu" "$cc" "${putres}" "$ret"
 		 echo "->          Guardado... "
 
		else
		 cc="->          Error General en archivo de log: $ologcd "
		 savedb "No se encontro archivo log: $ologcd del erp: $1: $ctu" "$cc" "${putres}" "$ret"
		fi
	 
#fin de funcion
}

function rcodel(){
#Inicio de funcion
	clear
		derp=$1
		#--creacion de carpeta olog para guardar log de respaldos
		ologcdel=${ologdir}$derp$chh".inf" 
		mklogd		
		ret="5"
		headx "#" "Iniciando Borrado de respaldos" 
		echo "<br>######################### Iniciando Borrado de respaldos $derp ################# $chh<br>" >> $ologcdel
		headx "@" "Borrado de archivos $chh ERP: $derp"
		echo "<br># Iniciando borrado -><br>" >> $ologcdel
		cd ${bkcod_dir} 
		headx "@" "Archivos en directorio antes de borrar: $chh"
		echo "<br>######################### Archivos antes de borrar ############################<br>" >> $ologcdel
		ls -lh >> $ologcdel
		ls -lh 
		cd ${bkcod_dir} >> $ologcdel
		rm -f bkc_$derp*
	    headx "&" "Se borraron archivos"
		headx "@" "Archivos actuales"
		echo "<br>######################### Archivos borrados archivos actuales ############## $chh<br>" >> $ologcdel
		headx "@" "Archivos en directorio despues de de borrar: $chh"
		ls -lh >> $ologcdel
		ls -lh 
		echo "<br>######################### Fin de borrado de respaldos ############## $chh<br>" >> $ologcdel
		headx "@" "Iniciando Grabando base de datos"
            cc=`cat $ologcdel`	
  			#echo "paso : Var1 "$1"  var2 "$cc" var3 "${putres}" var4 "$ret" "
			#savedb erp log infadd cat
			savedb "Borrado de respaldo Codigo: $1" "$cc" "Borrado de respaldo de codigo" "$ret"
#Fin de funcion	
}



function rdb(){
#Inicio de funcion
	clear
		db=$1
		ddbr=${bkdb_dir}	
		udb="root"
		pdb="p0rtali70s"
		dbfile="dbc_"$db"_${chh}.sql"
		rzdb="dbbkc_"$db"_"${chh}".tar.gz"
		subir=$2
		ddir=$3
		isp=$4
		ispx=$5
			#mysqldump -u root -pp0rtali70s olog_x | 7z a -si backup.sql.7z
			#--creacion de carpeta olog para guardar log de respaldos
			ologdb=${ologdir}$db$chh".inf"
			mklogd		
		ret="2"
		echo "<br>######################### Iniciando Respaldo de db: $db ################# $chh<br>" >> $ologdb
		echo "<br># Iniciando respaldo db -> $db #<br>" >> $ologdb
		headx "@" "Iniciando respaldo db -> $db"
		mysqldump --user=$udb --password=$pdb $db > $ddbr$dbfile 2>/dev/null &
				pid=$!
				i=0
				while kill -0 $pid 2>/dev/null
				do
					i=$(( (i+1) %5 ))
					printf "\rEspere....................${spin:$i:1}"
					sleep .1
				done
		if [ $? -eq 0 ] ; 
		then
			cd $ddbr
			if [ -f $dbfile ];
			then
				echo " "
				echo "<br># Respaldo completado $ddbr$dbfile #<br>" >> $ologdb
				headx "@" "Respaldo completado $ddbr$dbfile"
				headx "@" "Comprimiendo respaldo... $ddbr$dbfile" 
				echo "<br># Comprimiendo respaldo... $ddbr$dbfile #<br>"  >> $ologdb
				tar -c $dbfile |gzip > $rzdb 2>/dev/null &
				pid=$!
				i=0
				while kill -0 $pid 2>/dev/null
				do
					i=$(( (i+1) %5 ))
					printf "\rEspere....................${spin:$i:1}"
					sleep .1
				done
				echo " "
				echo "<br>Respaldo comprimido .gz completo $rzdb<br>" >> $ologdb
				headx "@" "Respaldo comprimido .gz completo $rzdb"
				ls -lh >> $ologdb
					if [ -f $rzdb ];
					then
						#validacion para guardar respaldo por mes
						unoxmes "$ddbr$rzdb" "db" "$db"
						# unoxmes Archivo con directorio "opcion db o od" "erp o base"
						cd $ddbr
						echo "<br>Buscando y borrando respaldo $dbfile<br>" >> $ologdb
						ls -lh >> $ologdb
						rm -f $dbfile
						echo "<br>Respaldo borrado respaldo $dbfile<br>" >> $ologdb
						ls -lh >> $ologdb
		          	    headx "@" "Respaldo de db: $rzdb completado"
				  		echo "<br>######################## Iniciando Log de base de datos ###########################<br>" >> $ologdb
				  		headx "@" "Iniciando Log de base de datos"
				  		echo "Fecha de respaldo $chh de Base de datos $db" >> $ologdb
				  		headx "#" "Fin de respaldo db: $db"
				  		echo "<br>############################### Fin de respaldo db: $db ####### $chh<br>" >> $ologdb
				  		headx "@" "Archivo a subir -> $rzdb Opcion: $subir" 
				  		if [ "$subir" = "S" ]; then
					  		headx "@" "Archivo a subir -> $rzdb Opcion: FTP o SFTP" 
				  			putff "$rzdb" $3 
				  		else
				  			echo "<br><br>No se selecciono subir archivo FTP<br>" >> $ologdb
				  			headx "&" "No se selecciono subir archivo FTP"
				  		fi
				  		
				  		if [ "$subir" = "z" ]; then
					  		headx "@" "Archivo a subir -> $rzdb Opcion: SSH"
					  		#putcot Archivo did[a,nau,dbr,codr] ipdservidor
					  		#directorio de envio
				  			putcot "$rzdb" "$ddir" "$isp"
				  		else
				  			echo "<br><br>No se selecciono subir archivo SSH<br>" >> $ologdb
				  			headx "&" "No se selecciono subir archivo SSH"
				  		fi

				  	else
				  		echo "<br>No, no existe respaldo .gz:<br>" $rzdb >> $ologdb
				  		headx "&" "No, no existe respaldo .gz:" $rzdb 
				  		ret="4"
				  	fi
				else
		     	  echo "<br>No, no existe: $dbfile<br>" >> $ologdb
		     	  headx "&" "No, no existe: $dbfile"
		     	  ret="4"
			fi
		else
				  echo "<br>No, no existe: $dbfile o error general<br>" >> $ologdb
				  headx "&" "No, no existe: $dbfile o error general"
		     	  ret="7"
		fi
		cc=`cat $ologdb`	
  		#echo "paso : Var1 "$1"  var2 "$cc" var3 "${putres}" var4 "$ret" "
		#savedb erp log infadd cat
		savedb "Respaldo de DB: $1" "$cc" "${putres}" "$ret"
#Fin de funcion
}


function rdeldb(){
#Inicio de funcion
		clear
		db=$1
		ddbr=${bkdb_dir}
		dbfchar="dbbkc_"$db"*"
		ologdbdel=${ologdir}$db$chh".inf"
		ret="6"
		#--creacion de carpeta olog para guardar log de respaldos
		mklogd
		headx "#" "Iniciando Borrado de respaldos"
		echo "<br>######################### Iniciando Borrado de Respaldos $db ################# $chh<br>" >> $ologdbdel
		headx "@" "Iniciando borrado -> de respaldos $db #"
		echo "<br># Iniciando borrado -><br>" >> $ologdbdel
		cd $ddbr
		echo "<br># Archivos antes de borrado ${chh} -><br>" >> $ologdbdel
		ls -lh >> ${ologdir}$db".inf"
		headx "@" "# Archivos antes de borrado ${chh} ->"
		ls -lh
		rm -f $dbfchar
	    headx "&" "Se borraron archivos"
	    echo "<br>############################## Se borraron archivos ################################# $chh<br>" >> $ologdbdel
	    echo "<br># Archivos despues de borrado ${chh} -><br>" >> $ologdbdel
		headx "@" "Archivos actuales #"
		ls -lh >> $ologdbdel
		ls -lh 
		echo "<br>######################### Fin de borrado de respaldos: $db ################# $chh<br>" >> $ologdbdel
		cc=`cat $ologdbdel`	
  		#echo "paso : Var1 "$1"  var2 "$cc" var3 "${putres}" var4 "$ret" "
		#savedb erp log infadd cat
		savedb "Borrado de Respaldo DB: $1" "$cc" "Borrado de respaldos DB" "$ret"
#Fin de funcion	
}

function upd_capa(){
#Inicio de funcion
	    clear
		db=$1
		db_cap=$2
		ddbr=${tmpcap}	
		finicio=$chh
		#Credenciales MYSQL
		u_u="root"
		p_p="p0rtali70s"
		hcapa="localhost"
		#Nombre del archivo a generar .sql 
		dbfile="dbc_"$db"_${chh}.sql"
		#Archivo Log
		ologdb=${ologdir}$db"_update"$chh".html"
		#--creacion de carpeta olog para guardar log de respaldos
		mklogd
#Comprobacion de existencia de db en Mysql
s_s=`mysql --host=$hcapa --user=$u_u --password=$p_p << END
 use $db -A;
END` 
if [ $? -eq 0 ] ;
then
		s_s=`mysql --host=$hcapa --user=$u_u --password=$p_p << END
		use $db_cap -A;
END` 

#string='My long string'
#if [[ $string == *"My long"* ]]; then
#  echo "It's there!"
#fi

if [[ $db_cap == *"CAPA"* ]] ; then
	if [ $? -eq 0 ] ; then
		#OPTIONS="--flush-logs"
		#OPTIONS="${OPTIONS} --add-drop-table"
	    #OPTIONS="${OPTIONS} --add-locks"
  		OPTIONS="${OPTIONS} --quick"
  		#OPTIONS="${OPTIONS} --extended-insert"
  		#OPTIONS="${OPTIONS} --disable-keys"
	  	#OPTIONS="${OPTIONS} --routines"
  		#  OPTIONS="${OPTIONS} --master-data"
  		OPTIONS="${OPTIONS} --single-transaction"
		#Todo OK con las db
		headx "#" "Base de datos a actualizar encontrada $db_cap"
		ret="16"
		headx "@" "Clonando Orinen db: -> $db a $db_cap"
		echo "<br># Clonando Orinen db: -> $db a $db_cap $chh<br>" >> $ologdb
		echo "<br># Iniciando Clonado db -> $db #<br>" >> $ologdb
		headx "@" "Iniciando Clonado db -> $db Generando $dbfile"
		mysqldump $OPTIONS --user=$u_u --password=$p_p $db > $ddbr$dbfile 2>/dev/null &
		pid=$!
		i=0
		while kill -0 $pid 2>/dev/null
				do
					i=$(( (i+1) %5 ))
					printf "\rEspere....................${spin:$i:1}"
					sleep .1
		done
		echo " "
		if [ $? -eq 0 ] ; 
		then
			cd $ddbr
			if [ -f $dbfile ];
			then
				echo "<br># Estructura y datos generados clonado completado $ddbr$dbfile #<br>" >> $ologdb
				headx "@" "Estructura y datos generados clonado completado $ddbr$dbfile"
				headx "@" "Generando clon... $db_cap" 
				echo "<br># Generando clon... $db_cap #<br>"  >> $ologdb
				mysql -u $u_u -p$p_p  $db_cap < $dbfile 2>/dev/null &
				pid=$!
				i=0
				while kill -0 $pid 2>/dev/null
				do
					i=$(( (i+1) %5 ))
					printf "\rEspere....................${spin:$i:1}"
					sleep .1
				done
				echo " "
					if [ $? -eq 0 ] ; 
					then
								echo "<br>Estructura y datos: $dbfile clonado en: $db_capa<br>" >> $ologdb
								headx "@" "Estructura y datos: $dbfile clonado en: $db_capa"
								if [ -f $dbfile ];
								then
									echo "<br>Buscando y borrando Estructura y datos: $dbfile<br>" >> $ologdb
									rm -f $dbfile
									echo "<br>Estructura y datos Borrados: $dbfile<br>" >> $ologdb
									headx "@" "Clonado de db: $db completado"
									headx "@" "Se creo $db_cap verificar integridad el ologX"
									echo "<br>######################## Iniciando Log de base de datos ###########################<br>" >> $ologdb
									echo "Fecha de Clonado $chh de Base de datos $db" >> $ologdb	
									echo "<br>############################### Fin de Clonado db: $db ####### $chh<br>" >> $ologdb
									ret="16"
									#Modificacion de parametros de configuracion para timbrado de documentos
									echo "update $db_capa.companies set coyname=concat('EMPRESA DE PRUEBA CAPA - ',coyname) "| mysql --user=$u_u --password=$p_p  $db_cap -A 
									if [ $? -eq 0 ] ; then
										headx "@" "Modificación de registros de DB exitoso coyname $db_cap"
										echo "Modificación de registros de DB exitoso coyname $db_cap " >> $ologdb
										ret="16"
									else
										headx "&" "Modificación de registros de DB fallido $db_cap"
										echo "Modificación de registros de DB fallido $db_cap" >> $ologdb
										ret="17"
									fi
									echo "update $db_capa.config set confvalue='FAKE' where confname='Timbrador' "| mysql --user=$u_u --password=$p_p $db_cap -A 
									if [ $? -eq 0 ] ; then
										headx "@" "Modificación de registros de DB exitoso Timbrador $db_cap"
										echo "Modificación de registros de DB exitoso Timbrador $db_cap" >> $ologdb
										ret="16"
									else
										headx "&" "Modificación de registros de DB fallido Timbrador $db_cap"
										echo "Modificación de registros de DB fallido Timbrador $db_cap" >> $ologdb
										ret="17"
									fi 
									echo "update $db_capa.config set confvalue=' ' where confname='EnviaNotificacionTren' "| mysql --user=$u_u --password=$p_p $db_cap -A 
									if [ $? -eq 0 ] ; then
										headx "@" "Modificación de registros de DB exitoso EnviaNotificacionTren $db_cap"
										echo "Modificación de registros de DB exitoso EnviaNotificacionTren $db_cap" >> $ologdb
										ret="16"
									else
										headx "&" "Modificación de registros de DB fallido EnviaNotificacionTren $db_cap"
										echo "Modificación de registros de DB fallido EnviaNotificacionTren $db_cap" >> $ologdb
										ret="17"
									fi 
									echo "update $db_capa.config set confvalue=' ' where confname='FactoryManagerEmail' "| mysql --user=$u_u --password=$p_p $db_cap -A 
									if [ $? -eq 0 ] ; then
										headx "@" "Modificación de registros de DB exitoso FactoryManagerEmail $db_cap"
										echo "Modificación de registros de DB exitoso FactoryManagerEmail $db_cap" >> $ologdb
										ret="16"
									else
										headx "&" "Modificación de registros de DB fallido FactoryManagerEmail $db_cap"
										echo "Modificación de registros de DB fallido FactoryManagerEmail $db_cap" >> $ologdb
										ret="17"
									fi 

									#Fin de la modificacion de parametros									
								else
					
									headx "&" "No se encontro archivo .SQL para borrar: $dbfile "
									echo "<br>No se encontro archivo .SQL para borrar: $dbfile<br>" >> $ologdb
									ret="17"
								fi
					else
				  					echo "<br>Error al crear .SQL archivo no creado, no se procede con clon: $dbfile<br>" >> $ologdb
				  					headx "&" "Error al crear .SQL archivo no creado, no se procede con clon: $dbfile"
				  					ret="17"
				  	fi
			else
		     	  echo "<br>Error no se encontro .SQL o error General: $dbfile<br>" >> $ologdb
		     	  headx "&" "Error no se encontro .SQL o error General: $dbfile"
		     	  ret="17"
			fi
		else
				  #no se encontro archivos .SQL antes de
				  echo "<br>Error al realizar el .SQL o error General: $dbfile<br>" >> $ologdb
				  headx "&" "Error al realizar el .SQL o error General: $dbfile"
		     	  ret="17"
		fi
	else
	#no se encontro db CAPA, se tiene que creear manual por seguridad.
	headx "&" "No se encuentra DB a Crear $db_cap: favor de crearla $? : $s_s"
	echo "<br>No se encuentra DB a Crear $db_cap: favor de crearla $? : $s_s<br>" >> $ologdb
	ret="17"  
    fi 
else
 #no se encontro db origen, verificar existencia o nombre de la db
headx "&" "No se encuentra DB a Clonar $db: favor de verificar que existe $? : $s_s" 
echo "<br>No se encuentra DB a Clonar $db: favor de verificar que existe $? : $s_s<br>" >> $ologdb
ret="17" 
fi

else #Si no la base a actualizar no es capa
headx "&" "La base a clonar; $db no es capa favor de verificar el parametro $? : $s_s" 
echo "<br>La base a clonar; $db no es capa favor de verificar el parametro $? : $s_s<br>" >> $ologdb
ret="17" 
fi
		cc=`cat $ologdb`	
  		#echo "paso : Var1 "$1"  var2 "$cc" var3 "${putres}" var4 "$ret" "
		#savedb erp log infadd cat
		ffin=`date +'%Y%m%d-%H%M%S'`
		savedb "Update de CAPA: $db a $db_cap" "$cc" "<br>$sinca<br>Fecha de Inicio:$finicio<br>Fecha de Fin: $ffin<br>" "$ret"
#Fin de funcion
}



function upd_des(){
#Inicio de funcion
#
#Actualizacion de base local a desarrollo, P1=base_remota, P2=Host_remoto, P3=base_local, P4=u_remoto, P5=p_remoto
	    clear
#variables para respaldo remoto desarrollo
	    #dbremota
			db_remota=$1
		#hostremoto
			hremo=$2
		#dblocal (la que se va a actualizar)
			db_local=$3
		#directorio temp del -sql
			ddbr=${tmpcap}	
		#Fecha de inicio del repaldo
			finicio=$chh
		#Usuario remoto
			u_urm=$4
		#Password remoto
			p_prm=$5
#fin de variables remotas

#Variables locales de desarrollo
		#host desarrollo
		hdes="Localhost"
		#credenciales Desarrollo
		u_udes="root"
		p_pdes="p0rtali70s"
#Fin de variables locales desarrollo
		#Nombre del archivo a generar .sql 
		gsql="dbc_DES_"$db_remota"_${chh}.sql"
		#Archivo Log
		ologdb=${ologdir}$db_remota"_update"$chh".html"
		#--creacion de carpeta olog para guardar log de respaldos
		mklogd
#Comprobacion de existencia de db en Mysql remoto
#dbseek host usuario password db_name
dbrm=$(dbseek $hremo $u_urm $p_prm $db_remota)
echo "db remota: $dbrm"
if [ $dbrm -eq 0 ] ;
then
 dblc=$(dbseek $hdes $u_udes $p_pdes $db_local)
 echo "db local: $dblc"
	if [ $dblc -eq 0 ] ;
	then
		#Todo OK con las db
		headx "@" "Base de datos a actualizar encontrada $db_local"
		ret="16"
		headx "#" "Clonando Orinen db: -> $db_remota a $db_local"
		echo "<br># Clonando Orinen db: -> $db_remota a $db_local $chh<br>" >> $ologdb
		echo "<br># Iniciando Clonado db -> $db_remota #<br>" >> $ologdb
		headx "@" "Iniciando Clonado db -> $db_remota Generando $gsql"
		mysqldump -h $hremo -u $u_urm -p$p_prm $db_remota > $ddbr$gsql
		if [ $? -eq 0 ] ; 
		then
			cd $ddbr
			if [ -f $gsql ];
			then
				echo "<br># Estructura y datos generados clonado completado $ddbr$gsql #<br>" >> $ologdb
				headx "@" "Estructura y datos generados clonado completado $ddbr$gsql"
				headx "@" "Generando clon... $db_local" 
				echo "<br># Generando clon... $db_local #<br>"  >> $ologdb
				mysql -u $u_udes -p$p_pdes  $db_local < $gsql
					if [ $? -eq 0 ] ; 
					then
								echo "<br>Estructura y datos: $gsql clonado en: $db_local<br>" >> $ologdb
								headx "@" "Estructura y datos: $gsql clonado en: $db_local"
								if [ -f $dbfile ];
								then
									echo "<br>Buscando y borrando Estructura y datos: $gsql<br>" >> $ologdb
									rm -f $gsql
									echo "<br>Estructura y datos Borrados: $gsql<br>" >> $ologdb
									headx "@" "Clonado de db: $db_remota completado"
									headx "#" "Se creo $db_local verificar integridad el ologX"
									echo "<br>######################## Iniciando Log de base de datos ###########################<br>" >> $ologdb
									echo "Fecha de Clonado $chh de Base de datos $db_remota" >> $ologdb	
									echo "<br>############################### Fin de Clonado db: $db_remota ####### $chh<br>" >> $ologdb
									ret="18"
									#Modificacion de parametros de configuracion para timbrado de documentos
									echo "update $db_capa.companies set coyname=concat('EMPRESA DE PRUEBA DES - ',coyname) "| mysql --user=$u_udes --password=$p_pdes  $db_local -A 
									if [ $? -eq 0 ] ; then
										headx "@" "Modificación de registros de DB exitoso coyname $db_local"
										echo "Modificación de registros de DB exitoso coyname $db_local " >> $ologdb
										ret="18"
									else
										headx "&" "Modificación de registros de DB fallido $db_local"
										echo "Modificación de registros de DB fallido $$db_local" >> $ologdb
										ret="19"
									fi
										echo "update $db_capa.config set confvalue='FAKE' where confname='Timbrador' "| mysql --user=$u_udes --password=$p_pdes $db_local -A 
									if [ $? -eq 0 ] ; then
										headx "@" "Modificación de registros de DB exitoso Timbrador $db_local"
										echo "Modificación de registros de DB exitoso Timbrador $$db_local" >> $ologdb
										ret="18"
									else
										headx "&" "Modificación de registros de DB fallido Timbrador $db_local"
										echo "Modificación de registros de DB fallido Timbrador $db_local" >> $ologdb
										ret="19"
									fi 
									#Fin de la modificacion de parametros											
								else
					
									headx "&" "No se encontro archivo .SQL para borrar: $gsql "
									echo "<br>No se encontro archivo .SQL para borrar: $gsql<br>" >> $ologdb
									ret="19"
								fi
					else
				  					echo "<br>Error al crear .SQL archivo no creado, no se procede con clon: $gsql<br>" >> $ologdb
				  					headx "&" "Error al crear .SQL archivo no creado, no se procede con clon: $$gsql"
				  					ret="19"
				  	fi
			else
		     	  echo "<br>Error no se encontro .SQL o error General: $gsql<br>" >> $ologdb
		     	  headx "&" "Error no se encontro .SQL o error General: $gsql"
		     	  ret="19"
			fi
		else
				  #no se encontro archivos .SQL antes de
				  echo "<br>Error al realizar el .SQL o error General: $gsql<br>" >> $ologdb
				  headx "&" "Error al realizar el .SQL o error General: $gsql"
		     	  ret="19"
		fi
	else
	#no se encontro db DES, se tiene que creear manual por seguridad.
	headx "&" "No se encuentra DB a Crear $db_local: favor de crearla en $hdes"
	echo "<br>No se encuentra DB a Crear $db_local: favor de crearla en $hdes<br>" >> $ologdb
	ret="19"  
    fi 
else
 #no se encontro db origen, verificar existencia o nombre de la db
headx "&" "No se encuentra DB a Clonar $db_remota: favor de verificar que existe en $hremo " 
echo "<br>No se encuentra DB a Clonar $db_remota: favor de verificar que existe en $hremo<br>" >> $ologdb
ret="19" 
fi
		cc=`cat $ologdb`	
  		#echo "paso : Var1 "$1"  var2 "$cc" var3 "${putres}" var4 "$ret" "
		#savedb erp log infadd cat
		ffin=`date +'%Y%m%d-%H%M%S'`
		savedb "Update Desarrollo: $db_local a $db_remota" "$cc" "Fecha de Inicio:$finicio<br>Fecha de Fin: $ffin<br>" "$ret"
#Fin de funcion
}

function sirix(){
#sirix [servicio]
#Inicio de funcion
clear
buscar="$1"
ubiq=$2
glog=$3
derp=${dorig}$1"/"
	cod=$1
	ologcd="${ologdir}$cod$chh".html""
	#--creacion de carpeta olog para guardar log de respaldos
	mklogd
serv=`ps -e | grep $buscar | wc -l`
#echo `ps -e | grep $buscar | wc -l`
if [ $serv == 0 ]; then
    echo "$buscar esta caido $serv"
    service $buscar restart
    /etc/init.d/$buscar restart
    savedb "Servicio: $1 Servidor: $ubiq" "Servicio caido $1 $chh se intenta reiniciar con restart" "Estado: $serv,$1" "13"
    serv2=`ps -e | grep $buscar | wc -l`
		if [ $serv2 == 0 ]; then
			service "$1" start
			/etc/init.d/$1 start
			echo "$1 esta caido $serv2"
			savedb "Servicio: $1 Servidor: $ubiq" "Servicio caido $1 $chh se intenta reiniciar con start " "Estado: $serv2,$1" "13"
			serv3=`ps -e | grep $buscar | wc -l`
			if [ $serv3 == 0 ]; then
				service "$1" stop
				/etc/init.d/$1 stop				
				service "$1" start
				/etc/init.d/$1 start
				echo "$1 esta caido $serv3"
                savedb "Servicio: $1 Servidor: $ubiq" "Servicio caido $1 $chh se intenta reiniciar con stop/start" "Estado: $serv3,$1" "13"
                serv4=`ps -e | grep $buscar | wc -l`
                	if [ $serv4 == 0 ]; then
						echo "$1 esta caido $serv4"
						savedb "Servicio: $1 Servidor: $ubiq" "¡¡¡¡¡¡¡¡Servicio caido $1 $chh Favor de verificar manualmente¡¡¡¡¡¡¡¡" "Estado: $serv4,$1" "13"
					fi
			fi
		fi
    else
    echo "Servicio $1 en ejecucion $serv "
    if [ "$glog" == "S" ]; then
    	savedb "Aux Servicio: $1 Servidor: $ubiq" "Servicio en ejecucion $1: $alice $chh " "Estado: $serv,$1" "12"
    else
    	echo "Aux Servicio: $1 Servidor: $ubiq" "Servicio en ejecucion $1: $alice $chh " "Estado: $serv,$1" "12" >> $ologcd
    fi

fi
#fin de funcion
}

function blanco(){
#blanco [servicio]
#Inicio de funcion
clear
buscar="$1"
ubiq=$2
glog=$3
	derp=${dorig}$1"/"
	cod=$1
	ologcd="${ologdir}$cod$chh".html""
	#--creacion de carpeta olog para guardar log de respaldos
	mklogd
serv=`ps aux | grep $buscar | grep -v grep | grep -v bbck.sh | wc -l`
alice=$(ps aux | grep $buscar | grep -v grep | grep -v bbck.sh)
alice=$(echo -e "Hole ###\n $alice \n####")
if [ $serv == 0 ]; then
    echo "$buscar esta caido $serv"
    /etc/init.d/QueueClients restart
    savedb "Aux Servicio: $1 Servidor: $ubiq" "Servicio caido $1: $alice $chh se intenta reiniciar con restart" "Estado: $serv,$1" "13"
    echo  "Aux Servicio: $1 Servidor: $ubiq" "Servicio caido $1: $alice $chh se intenta reiniciar con restart" "Estado: $serv,$1" "13" >> $ologcd
    serv2=`ps aux | grep $buscar | grep -v grep | grep -v bbck.sh | wc -l`
		if [ $serv2 == 0 ]; then
			/etc/init.d/QueueClients start
			echo "$1 esta caido $serv2"
			savedb "Aux Servicio: $1 Servidor: $ubiq" "Servicio caido $1: $alice $chh se intenta reiniciar con start " "Estado: $serv2,$1" "13"
			echo "Aux Servicio: $1 Servidor: $ubiq" "Servicio caido $1: $alice $chh se intenta reiniciar con start " "Estado: $serv2,$1" "13" >> $ologcd
			serv3=`ps aux | grep $buscar | grep -v grep | grep -v bbck.sh | wc -l`
			if [ $serv3 == 0 ]; then
				/etc/init.d/QueueClients stop
				/etc/init.d/QueueClients start
				echo "$1 esta caido $serv3"
                savedb "Aux Servicio: $1 Servidor: $ubiq" "Servicio caido $1: $alice $chh se intenta reiniciar con stop/start" "Estado: $serv3,$1" "13"
                echo " Aux Servicio: $1 Servidor: $ubiq" "Servicio caido $1: $alice $chh se intenta reiniciar con stop/start" "Estado: $serv3,$1" "13" >> $ologcd
                serv4=`ps aux | grep $buscar | grep -v grep | grep -v bbck.sh | wc -l`
                	if [ $serv4 == 0 ]; then
						echo "$1 esta caido $serv4"
						savedb "Aux Servicio: $1 Servidor: $ubiq" "¡¡¡¡¡¡¡¡Servicio caido $1: $alice $chh Favor de verificar manualmente¡¡¡¡¡¡¡¡" "Estado: $serv4,$1" "13"
						echo  " Aux Servicio: $1 Servidor: $ubiq" "¡¡¡¡¡¡¡¡Servicio caido $1: $alice $chh Favor de verificar manualmente¡¡¡¡¡¡¡¡" "Estado: $serv4,$1" "13" >> $ologcd
					fi
			fi
		fi
    else
    echo "Servicio $1 en ejecucion $serv "
    if [ "$glog" == "S" ]; then
    	savedb "Aux Servicio: $1 Servidor: $ubiq" "Servicio en ejecucion $1: $alice $chh " "Estado: $serv,$1" "12"
    else
    	echo "Aux Servicio: $1 Servidor: $ubiq" "Servicio en ejecucion $1: $alice $chh " "Estado: $serv,$1" "12" >> $ologcd
    fi
fi
#fin de funcion
}

function agentex()
{
#Grabar logs en Agenda desde terminal
#inicio de funcion
clear
dtw=$1
logh=$2
masinf=$3
gato=$4
  if [ ! -z "$dtw" ] && [ ! -z "$logh" ] && [ ! -z "$masinf" ] && [ ! -z "$gato" ] ;
  then
	#echo "paso : Var1 "$1"  var2 "$cc" var3 "${putres}" var4 "$ret" "
  	#savedb "erp o DTW" "log o informacion principal" "informacion adicional" "categoria de etiqueta"
  	echo "Grabando en base de datos.."
  	savedb "Ejecucion de Dataware: $dtw" "$logh" "$masinf" "$gato" 
  else
	echo "Faltan parametros use /utl/respaldo/.bbck para saber su uso"
  fi	
#fin de funcion
}

function conejotes(){
	#inicio de funcion
	#Funcion para verificar AMQ
	conejones
	#fin
	#variables
	est=""
	logamq="/data/log/amq/amqlog_conejotes.log"
	fst=`date +'%Y%m%d-%H%M%S'`
	cadt="network.jsp"
	tmpdir="/utl/tmp/"
	if [ -d $tmpdir ]; then
			headx "@" "Directorio temporal encontrado"
			cd $tmpdir
			rm -f $cadt*
			est="$est Directorio temporal encontrado <br>"
		else
			headx "&" "No se encontro directorio temporal se procede a crear"
			mkdir /utl/tmp
			cd $tmpdir
			est="$est No se encontro directorio temporal se procede a crear <br>"
	fi	
	wget --user admin --password admin http://cot-ats-sayh01p.cotemar.net:8161/admin/$cadt
	if [ ${PIPESTATUS[0]} -ne 0 ]; then
		headx "@" "Error wget archivo $cadt"
		resp=13
		est="$est Error wget archivo <br>"
	else
		headx "@" "wget de archivo $cadt correcto"
		resp=12
		est="$est wget de archivo $cadt correcto <br>"
    fi
	if [ -f $cadt ]; then
		headx "@" "Archivo encontrado"
		buscale=$(echo `less network.jsp | grep tcp://172.21.152.140:61616  | wc -l`)
		est="$est Archivo encontrado y se procede a buscar <br>"
			if [ $buscale -ge 1 ]; then 
					headx "@" "se encontraron $buscale conincidencias"
					est="$est se encontraron $buscale conincidencias <br>"
					resp=12					
				else
					headx "&" "No se encontraron coincidencias: $buscale se procede a reiniciar servicio de red"
					est="$est No se encontraron coincidencias: $buscale se procede a reiniciar servicio de red <br>"
					echo e0adc15731 | sudo -S /etc/init.d/network restart
						if [ ${PIPESTATUS[0]} -ne 0 ]; then
							headx "&" "Error al reiniciar la interfaz de red"
							est="$est Error al reiniciar la interfaz de red <br>"
								else
							headx "@" "Se reinicio Correctamente la red"
							est="$est Se reinicio Correctamente la red <br>"
							resp=13
         				fi
         			redst=$(echo `/etc/init.d/network status`)
         			est="$est Estatus general de red:==> $redst <br>"
			fi
	else
		headx "@" "No se encontro el archivo error al descargar"
		resp=13
		dirtp=$(echo `ls -lh`)
		est="$est Error al leer archivo.. o archivo no existe. $dirtp <br>"
	fi
		if [ $resp -eq 13 ]; then 
		fifi=`date +'%Y%m%d-%H%M%S'`
		savedb "Conectividad de red AMQ Tierra_PROD <--==--> Atlantis_PROD" "Estatus; $est" "Tiempo de ejecucion:$fst -> $fifi" $resp
	fi
	echo "-->Tiempo: $fst" >> $logamq
	echo "Log de proceso de servicio de RED: " >> $logamq
	echo "$est" >> $logamq
	echo "-->>fin de log $fifi" >> $logamq
	echo "-----------------------------------------------------------------------------------------------------------------------------" >> $logamq
#fin de funcion
}

function conejones(){
	#inicio de funcion
	est=""
	logjones="/data/log/amq/amqlog_conejones.log"
	#variables para reiniciar el AMQ por el momento solo se monitorean
	popp=`date +'%Y%m%d-%H%M%S'`
	#direccion para verificar en atlantis
	atpagco="http://cot-ats-sayh01p.cotemar.net:8161/admin/connections.jsp" 
	#campo a buscar: ID:cot-ats-sayh01p.cotemar.net-60510 en Connector openwire
	b_copwat="ID:cot-ats-sayh01p.cotemar.net"	
	#campo a buscar: 12024-Atlantis-QA en Network Connectors
	b_atnc="12024-Atlantis-QA"
	
	#direccion para verificar en tierra 
	tiepagco="http://cot-cdc-sayh01p.cotemar.net:8161/admin/connections.jsp"
	#campo a buscar: 12024-Atlantis-QA:localhost:outbound 
	b_tinc="12024-Atlantis-QA:localhost:outbound"
	#campo a buscar: ID:cot-cdc-sayh01p.cotemar.net-33803 en Connector openwire
	b_copwti="ID:cot-cdc-sayh01p.cotemar.net"	
	
	#Archivos de log[Paginas descargadas] para las busquedas (Se pueden repetir ya que la funcion borra los logs antes de meter los nuevos)
	cacdc="concdc.log"
	catl="conatl.log"
	#Fin de archivos de log 
	
	#conejitos "$url" "$flog" "findme" "chkorde" regresa (12=OK,13=Error)
	#Busquedas para reinicio de ACMQ
	conejitos "$atpagco" "$catl" "$b_copwat" "Busca en Connector openwire la info ID:cot-ats-sayh01p.cotemar.net" 4
	c1=$?
	conejitos "$atpagco" "$catl" "$b_atnc" "Busca en Network Connectors la info 12024-Atlantis-QA" 1
	c2=$?
	conejitos "$tiepagco" "$cacdc" "$b_copwti" "Busca en Connector openwire la info ID:cot-cdc-sayh01p.cotemar.net" 4
	c3=$?
	conejitos "$tiepagco" "$cacdc" "$b_tinc" "Busca en Network Connectors la info 12024-Atlantis-QA" 1
	c4=$?
	#Reinicio de servicios segun el error
	#Embarcaciones
	#Atlantis
	headx "#" "VARS"
	headx "@" "$c1 - $c2 <<>> $c3 - $c4"
	#obtencion de ips para reinicio de servicios
	#ip atlantis
	ba=`ifconfig -a | grep 172.22.103.138 | wc -l`
	
	ban=`ifconfig -a | grep "inet addr:"`
	#ip tierra
	bt=`ifconfig -a | grep 172.21.152.140 | wc -l`
	
	btn=`ifconfig -a | grep "inet addr:"`
	#fin ips
	#echo "Vars: $c1 - $c2 <<>> $c3 - $c4 IpsATL: $ba <<>> $ban IpsTIE: $bt <<>> $btn" >> $logjones
	if [ $c1 -eq 13 ] || [ $c2 -eq 13 ]; then
		 if [ $ba -eq 1 ]; then
			headx "@" "Se reinician servicios de AMQ Atlantis"
				cat /var/log/weberp/QueueClient.log >> /var/log/weberp/QCdel.log_z
				cat /dev/null > /var/log/weberp/QueueClient.log
				echo "Goooool!0-1" >> /var/log/weberp/QueueClient.log
				/etc/init.d/QueueClients stop
				sleep 10
				/etc/init.d/QueueClients start
				sleep 10
				echo "Goooool1-0!" >> /var/log/weberp/QueueClient.log
			pop=`date +'%Y%m%d-%H%M%S'`
			savedb "AMQ Se reinicio servicio en Atlantis" "Estatus; << $c1 - $c2 >> IP: $ban" "Tiempo de ejecucion:$popp -> $pop" "22"
			echo "##########################################Log Conejones ERROR $popp #######################################" >> $logjones
			echo "Error Atlantis" >> $logjones
			echo "$c1 - $c2 <<>> $c3 - $c4 <<>> $ba - $ban" >> $logjones
			echo "##########################################Log FIN ERROR #######################################" >> $logjones
		fi	
	else 
			headx "@" "Todo OK Atlantis"
			echo "##########################################Log Conejones OK $popp #######################################" >> $logjones
			echo "Todo OK Atlantis" >> $logjones
			echo "$c1 - $c2 <<>> $c3 - $c4 <<>> $ba - $ban" >> $logjones
			echo "##########################################Log FIN OK#######################################" >> $logjones
	fi
	#Fin de embarcaciones
	
	#Tierra
	if [ $c3 -eq 13 ] || [ $c4 -eq 13 ]; then
		if [ $bt -eq 1 ]; then
			headx "@" "Se reinician servicios de AMQ Tierra"
				cat /var/log/weberp/QueueClient.log >> /var/log/weberp/QCdel.log_z
				cat /dev/null > /var/log/weberp/QueueClient.log
				echo "Goooool!0-1" >> /var/log/weberp/QueueClient.log
				/etc/init.d/QueueClients stop
				sleep 10
				/etc/init.d/QueueClients start
				sleep 10
				echo "Goooool1-0!" >> /var/log/weberp/QueueClient.log
			pop=`date +'%Y%m%d-%H%M%S'`
			savedb "AMQ Se reinicio servicio en Central Tierra" "Estatus; << $c3 - $c4 >> IP: $btn" "Tiempo de ejecucion:$popp -> $pop" "22"
			echo "##########################################Log Conejones ERROR $popp #######################################" >> $logjones
			echo "Error Tierra" >> $$logjones
			echo "$c1 - $c2 <<>> $c3 - $c4 <<>> $bt - $btn" >> $logjones
			echo "##########################################Log FIN ERROR #######################################" >> $logjones
		fi
	else 
			headx "@" "Todo OK Tierra"
			echo "##########################################Log Conejones OK $popp #######################################" >> $logjones
			echo "Todo OK Tierra" >> $logjones
			echo "$c1 - $c2 <<>> $c3 - $c4 <<>> $bt - $btn" >> $logjones
			echo "##########################################Log FIN OK#######################################" >> $logjones
	fi
	
#fin de funcion
}

function conejitos(){
	#inicio de funcion
	#conejitos "$url" "$flog" "findme" "chkorde"
	# 21/10/16 administracion de AMQ en la parte de Connectios de ambas embarcaciones, solo monitoreo por el momento
	# parametros de funcion 
	url=$1
	flog=$2
	findme=$3
	chkorde=$4
	buscont=$5
	logamq="/data/log/amq/amqlog_conejitos.log"
	fst=`date +'%Y%m%d-%H%M%S'`
	tmpdir="/utl/tmp/"
	headx "#" "Inicia conejitos: $findme"
	est=""
	if [ -d $tmpdir ]; then
			headx "@" "Directorio temporal encontrado"
			cd $tmpdir
			rm -f $flog*
			est="$est Directorio temporal encontrado <br>"
		else
			headx "&" "No se encontro directorio temporal se procede a crear"
			mkdir /utl/tmp
			cd $tmpdir
			est="$est No se encontro directorio temporal se procede a crear <br>"
	fi	
#Extrae la pagina del parametro url
	wget --user admin --password admin $url -O $flog
	if [ ${PIPESTATUS[0]} -ne 0 ]; then
		headx "@" "Error wget archivo $flog"
		resp=13
		est="$est Error wget archivo <br>"
	else
		headx "@" "wget de archivo $flog correcto"
		resp=12
		est="$est wget de archivo $flog correcto <br>"
    fi
   
	if [ -f $flog ]; then
		headx "@" "Archivo encontrado"
		est="$est Archivo encontrado y se procede a buscar <br>"
		ttngo=$(echo `less $flog | grep $findme  | wc -l`)
		#Se procede a concatenar log en variable est para la trazabilidad del log
			if [ $ttngo -eq $buscont ]; then 
					headx "@" "se encontraron $ttngo conincidencias"
					est="$est se encontraron $ttngo conincidencias <br>"
					resp=12
				else
					headx "&" "No se encontraron coincidencias: $ttngo se procede a reiniciar AMQ"
					est="$est No se encontraron coincidencias: $ttngo se procede a reiniciar AMQ<br>"
         			queust=$(echo `ps aux | grep Queu | grep -v grep | grep -v bbck.sh | grep -v sendmail`)
         			est="$est Error se reinicio QUEUE .... $queust <br>"
         			resp=13
			fi
	else
		headx "@" "No se encontro el archivo error al descargar $flog"
		dirtp=$(echo `ls -lh`)
		est="$est Error al leer archivo.. o archivo no existe. $flog <br>"
		resp=13
	fi
	if [ $resp -eq 13 ]; then 
		fifi=`date +'%Y%m%d-%H%M%S'`
		savedb "Monitoreo de conecciones  AMQ $chkorde" "Estatus; $est" "Tiempo de ejecucion:$fst -> $fifi" $resp
		est="$est Salida con error QUEUES <br>"
		echo "################################ ==> $fst ==> Conejitos ERROR ################################ " >> $logamq
		echo "-->Tiempo: $fst" >> $logamq
		echo "Log de proceso de servicio:" >> $logamq
		echo "$est" >> $logamq
		echo "-->>fin de log $fifi" >> $logamq
		echo "######################################################################################################## " >> $logamq
		resp=13
	fi
	if [ $resp -eq 12 ]; then 
		fifi=`date +'%Y%m%d-%H%M%S'`
		est="$est Salida sin errores <br>"
		echo "################################ ==> $fst ==> Conejitos OK ################################ " >> $logamq
		echo "-->Tiempo: $fst" >> $logamq
		echo "Log de proceso de servicio:" >> $logamq
		echo "$est" >> $logamq
		echo "-->>fin de log $fifi" >> $logamq
		echo "######################################################################################################## " >> $logamq
		#savedb "Monitoreo de conecciones  AMQ $chkorde" "Estatus; $est" "Tiempo de ejecucion:$fst -> $fifi" $resp
		resp=12
	fi
#fin de funcion
return $resp
}

function dwtf(){	
#inicio de funcion
clear
Ichh=$chh
ident=$1	
zdB="olog_x"
sup="Portalito 1"
fini=`date +'%Y%m%d-%H%M%S'`
#Variables de dataware 
opz=$2
anio=`date +%Y`
mes=`date +%m`
dia=`date +%d`
y=1
#Fin variables para dataware
		headx "#" "Iniciando Ejecucion de DTW"
#Busca nombre de la base de datos del dataware con el identificador $ident
sdB=`mysql --host=$host --user=$userx --password=$pap "$zdB"<< END
 select dbdtw from dtwexec where Iden='$ident';
END` 
if [ $? -eq 0 ] ;
   then
		headx "@" "OK Base de datos DTW: $? : $sdB"  
	else
		headx "&" " NO Base de datos DTW: $? : $sdB"  
fi
sdB=${sdB:6:100}
#echo "Cadena; $zdB"
#Busca nombre de la base de datos del servidor con el identificador $ident
zserv=`mysql --host=$host --user=$userx --password=$pap "$zdB"<< END
 select Servidor from dtwexec where Iden='$ident';
END` 
if [ $? -eq 0 ] ;
   then
		headx "@" "OK Servidor: $? : $zserv"  
	else
		headx "&" "NO Servidor: $? : $zserv"  
fi
zserv=${zserv:8:100}
#echo "Cadena; $zserv"
#Busca la cadena SQL 
sql_db=`mysql --host=$host --user=$userx --password=$pap "$zdB"<< END
 select sqlQ from dtwexec where Iden='$ident';
END` 
if [ $? -eq 0 ] ;
   then
#opciones fechas dataware's
			if [ "$opz" == " " ] ; then
				#opcion default)
				if [ $mes == 1 ] ; then
       		 		anio=$(($anio - $y))
				fi
		
				if [ $dia == 1 ] ; then
					mes=$(($mes - $y))
				fi
			elif [ "$opz" == "a" ] ; then
			#opcion a)
				if [ $mes == 1 ] ; then
        			anio=$(($anio - $y))
				fi
					mes=$(($mes - $y))

				if [ $dia == 1 ] ; then
					mes=$(($mes - $y))
				fi 
				echo $mes,$anio;
				savedb "DTW EXEC. Srv: $zserv" "Se Ejecuto: $sql_db <br>Inicio: $fini Termino: $ffin" "DB: $sdB Identificador: $ident $logmad" "10"
				exit;		
			elif [ "$opz" == "b" ] ; then
			#opcion b)
				if [ $mes == 1 ] ;then
					anio=$(($anio - $y))
				fi
					mes="$(echo $mes | sed 's/0*//')"
				if [ $dia == 1 ] ; then
					mes=$(($mes - $y))
				fi
			elif [ "$opz" == "c" ] ; then
			#opcion c)
				es="$(echo $mes | sed 's/0*//')"
				dia="$(echo $dia | sed 's/0*//')"
				if [ $mes == 1 ] ; then
					if [ $dia == 1 ] ;then
	            		anio=$(($anio - $y))
						mes=13
					fi
				fi
				if [ $dia == 1 ];
				then
					mes=$(($mes - $y))
				fi
			#opcion d)	
			fi
#fin de opciones de dataware
		#@ = año ($anio)
		## = mes ($mes)
		#& = dia ($)
		logmad="Mes: $me dia: $dia año: $anio"
		conca=$(echo `expr length "$sql_db"`)
		sql_db=${sql_db:5:$conca}
		sql_db=${sql_db//@/$anio}
		sql_db=${sql_db//#/$mes}
		#Genera cadena en sql para su proceso
		#Ejecuta la cadena SQL en la base de datos desde linea de comando
		##Servidor donde reside dataware credenciales
		hostsql="localhost"
		u_ux=`mysql --host=$host --user=$userx --password=$pap "$zdB"<< END
 select us from dtwexec where Iden='$ident';
END` 
			if [ $? -eq 0 ] ;
			then
				conca=$(echo `expr length "$u_ux"`)
				u_ux=${u_ux:3:conca}
				u_ux=${u_ux//' '/''}
				#echo "#-> OK Nombre de usuario: $? : $u_ux"
				p_px=`mysql --host=$host --user=$userx --password=$pap "$zdB"<< END
 select ps from dtwexec where Iden='$ident';
END` 
					if [ $? -eq 0 ] ;
					then
						conca=$(echo `expr length "$p_px"`)
						p_px=${p_px:3:conca}  
						p_px=${p_px//' '/''}
						#echo "#-> OK password: $? : $p_px"
						exesql=`mysql --host=$hostsql --user=$u_ux --password=$p_px "$sdB"<< END
$sql_db
END` 
							if [ $? -eq 0 ] ;
							then
								headx "@" "OK ejecucion de SQL: $? :$sql_db: $exesql"  
								#Graba en agenda que se ejecuto sin problemas
								ffin=`date +'%Y%m%d-%H%M%S'`
								savedb "DTW EXEC. Srv: $zserv" "Se Ejecuto: $sql_db <br>Inicio: $fini Termino: $ffin" "DB: $sdB Identificador: $ident $logmad" "10"
							else
								headx "&" "Error en ejecucion de SLQ: $? :$sql_db: $exesql"
								#Graba en agenda que se ejecuto con problemas  
								savedb "DTW EXEC. Srv: $zserv" "Error al ejecutar: $sql_db <br>Inicio: $fini Termino: $ffin" "DB: $sdB Identificador: $ident $logmad" "11"
							fi	

					else
						headx "&" "NO se recupero password: $? : $p_px"
						savedb "DTW EXEC. Srv: $zserv" "Error al ejecutar: $sql_db " "No se encontro password: $p_px" "11"  
					fi  
			else
				headx "&" "NO se recupero contraseña: $? : $u_ux"  
				savedb "DTW EXEC. Srv: $zserv" "Error al ejecutar: $sql_db " "No se encontro Usuario: $u_ux" "11"
			fi
	else
		headx "&" "Error al extraer cadena SQL de DB: $? : $sql_db"  
		savedb "DTW EXEC. Srv: $zserv" "Error al ejecutar: $sql_db " "No se encontro SQL: $sql_db" "11"
fi

}

function phX(){	
#inicio de funcion
clear
zdB="olog_x"
tb="phxdb"
chh=`date +'%Y%m%d-%H%M%S'`
logz="/utl/log/logphp.txt"
echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx------------------$chh-----------------xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" >> $logz
#identificador para hacer la consulta del php a ejecutar
ident=$1	 #Identificador del php
#Busca nombre del archivo $ident
phB=`mysql --host=$host --user=$userx --password=$pap "$zdB"<< END
 select file from $tb where Iden='$ident';
END` 
if [ $? -eq 0 ] ; then
	    la=$(echo `expr length "$phB"`)
	    phB=${phB:5:la}
		headx "@" "OK Archivo: $phB"  
	else
		headx "&" "NOOK Archivo: $phB"  
fi
#Busca si esta activo el php a ejecutar
onfB=`mysql --host=$host --user=$userx --password=$pap "$zdB"<< END
 select onoff from $tb where Iden='$ident';
END` 
if [ $? -eq 0 ] ; then
	    lot=$(echo `expr length "$onfB"`)
	    onfB=${onfB:6:lot}
		headx "@" "OK Activo: $onfB"  
	else
		headx "&" "NOOK Activo: $onfB"  
fi
#Busca directorio $ident
drB=`mysql --host=$host --user=$userx --password=$pap "$zdB"<< END
 select dir from $tb where Iden='$ident';
END` 
if [ $? -eq 0 ] ; then
	   	le=$(echo `expr length "$drB"`)
	    drB=${drB:4:le}
		headx "@" "OK Directorio: $drB"  
	else
		headx "&" "NOOK Directorio: $drB"  
fi
#Busca nombre de la base de datos del servidor con el identificador $ident
zserv=`mysql --host=$host --user=$userx --password=$pap "$zdB"<< END
 select Servidor from $tb where Iden='$ident';
END` 
if [ $? -eq 0 ] ; then
	    li=$(echo `expr length "$zserv"`)
	    zserv=${zserv:9:li}
		headx "@" "OK Servidor: $zserv"  
	else
		headx "&" "NOok Servidor : $zserv"  
fi

if [ "$onfB" == "1" ] ; then
 headx "@" "Directorio: $drB"
 cd $drB
 echo "PHP: $chh ejecucion de php $drB""/"$phB" en servidor $zserv " >> $logz
	if [ -f $phB ]; then
	    	cd $drB ; php $phB 2>> $logz
				xpw=$(echo `less $logz | grep "Warning" | wc -l`)
			xpe=$(echo `less $logz | grep "error" | wc -l`)
			echo " "
			#Graba en agenda que se ejecuto sin problemas
				if [ $xpe -ge 1 ] ; then
					headx "&" "Error se ejecuto con error: Warrning=$xpw - Error=$xpe"
					#Graba en agenda que se ejecuto con problemas  
					headx "@" "PHP EXEC: $ident Servidor: $zserv Se Ejecuto: $phB Finicio: $Ichh Ffin: $chh Estado:$xpe 15"
					xphp="Error o Warning"
					savedb "PHP EXEC: $ident Servidor: $zserv" "Error al ejecutar: $phB Finicio: $Ichh Ffin: $chh" "Estado: $xpe" "15"
					echo "PHP EXEC: $ident Servidor: $zserv Se Ejecuto: $phB Finicio: $Ichh Ffin: $chh Estado:$xpe 15" >> $logz
				elif [ $xpw -eq 0 ] && [ $xpe -eq 0 ] ; then
					headx "@" "Se ejecuto correctamente: $drB$phB"  
					headx "@" "PHP EXEC: $ident Servidor: $zserv Se Ejecuto: $phB Finicio: $Ichh Ffin: $chh Estado:Warning:$xpw Error:$xpe 14"
					savedb "PHP EXEC: $ident Servidor: $zserv" "Se Ejecuto: $phB Finicio: $Ichh Ffin: $chh" "Estado:Warning:$xpw Error:$xpe" "14"
					echo "PHP EXEC: $ident Servidor: $zserv Se Ejecuto: $phB Finicio: $Ichh Ffin: $chh Estado:Warning:$xpw Error:$xpe 14" >> $logz
				elif [ $xpw -ge 1 ] ; then
					headx "@" "Se ejecuto correctamente: $drB$phB pero con Warning"  
					headx "@" "PHP EXEC: $ident Servidor: $zserv Se Ejecuto: $phB Finicio: $Ichh Ffin: $chh Estado: Warning:$xpw 14"
					savedb "PHP EXEC: $ident Servidor: $zserv pero con Warning" "Se Ejecuto: $phB Finicio: $Ichh Ffin: $chh" "Estado: Warning:$xpw" "14"
					echo "PHP EXEC: $ident Servidor: $zserv Se Ejecuto: $phB Finicio: $Ichh Ffin: $chh Estado: Warning:$xpw 14" >> $logz
				fi
				
			
	else
			headx "&" "Error no se encuentra el archivo : $drB$phB"
			#Graba en agenda que se ejecuto con problemas  
			xphp="No se encontro el archivo"
			headx "@" "PHP EXEC: $ident Servidor: $zserv Se Ejecuto: $phB Finicio: $Ichh Ffin: $chh Estado:$xphp 15"
			echo "PHP EXEC: $ident Servidor: $zserv Se Ejecuto: $phB Finicio: $Ichh Ffin: $chh Estado:$xphp 15" >> $logz
			savedb "PHP EXEC: $ident Servidor: $zserv" "Error al ejecutar: $phB Finicio: $Ichh Ffin: $chh" "Estado: $xphp" "15"
	fi	
else
	headx "&" "Esta desactivada la ejecucion del PHP: $phB"
	xphp="PHP desactivado."
	headx "@" "PHP EXEC: $ident Servidor: $zserv Se Ejecuto: $phB Finicio: $Ichh Ffin: $chh Estado:$xphp 15"
	echo "PHP EXEC: $ident Servidor: $zserv Se Ejecuto: $phB Finicio: $Ichh Ffin: $chh Estado:$xphp 15" >> $logz
	savedb "PHP EXEC: $ident Servidor: $zserv" "Error al ejecutar: $phB Finicio: $Ichh Ffin: $chh" "Estado: $xphp" "15"
fi
}



function pdb()
{
	#Inicio de funcion para enviar el peso de las bases de datos a olog_x
	dbxl=$1
	dbxl="olog_x"
	comz="'"
	dgraph "$dbxl"
}

function dgraph(){
#Inicio de funcion
#sqlrun host usuario password erp
#Funcion para crear las bases de datos por host
db_us=$1
id_serv=$2
comz="'"
h_i="localhost"
#Credenciales para el servidor
uui=`mysql --host=${host} --user=${userx} --password=${pap} ${db_x} -A<< END 
Select ologus from slist where idserv = $id_serv;
END`
uuic=$(echo `expr length "$uui"`)
u_ui=${uui:7:uuic} 
ppi=`mysql --host=${host} --user=${userx} --password=${pap} ${db_x} -A<< END 
Select ologpas from slist where idserv = $id_serv;
END`
ppic=$(echo `expr length "$ppi"`)
p_pi=${ppi:8:ppic} 
headx "#" "$u_ui or $p_pi"
#Fin de credenciales
db_na=`mysql --host=$h_i --user=$u_ui --password=$p_pi $db_us -A<< END 
SELECT table_schema "N_DB" FROM  information_schema.TABLES where TABLE_SCHEMA =$comz$db_us$comz GROUP BY table_schema;
END` 
db_sz=`mysql --host=$h_i --user=$u_ui --password=$p_pi $db_us -A<< END 
SELECT sum( data_length + index_length ) / 1024 / 1024 "MB_X" FROM information_schema.TABLES where TABLE_SCHEMA=$comz$db_us$comz GROUP BY table_schema;
END`
up1=$(echo `expr length "$db_na"`)
up1=${db_na:5:up1} 
up2=$(echo `expr length "$db_sz"`)
up2=${db_sz:5:up2} 
#db_ins=$("INSERT into DBSZ (db_nam,db_siz) VALUES ($db_na,$db_sz)" | mysql --user=$userx --password=$pap  $db_x -A) 
db_ins=`mysql --host=${host} --user=${userx} --password=${pap} ${db_x} -A<< END 
INSERT into DBSZ (ref,db_nam,db_siz) VALUES (' ','$up1',$up2);
END`
if [ $? -eq 0 ] ;
then
	headx "#" "Se ejecuto correctamente la consulta: $db_ins"
	headx "#" "$up1 or $db_na"
	headx "#" "$up2 or $db_sz"
else
	headx "&" "Error en la consulta: $db_ins"
	headx "#" "$up1 or $db_na"
	headx "#" "$up2 or $db_sz"
fi
#fin de funcion
}

function hdsp(){
#Inicio de funcion
#funcion para medir carpeta de companies
imp=$1
id_serv=$2
comz="'"
h_i="localhost"
#Directorio de archivos de cliente
dcomp="/var/www/html/erpdistribucion/companies/"
#Procesa la info de la(s) carpetas para regresar solo tamaño en MB
dtra=$dcomp$imp
if [ -d $dtra ] ; then
		fsp=$(echo `du -bs $dtra`)
		fcs=$(echo `du -bhs $dtra`)
		headx "#" "$fsp"
		headx "#" "$fcs"
		fs1=$(echo `expr index "$fsp" "$dtra"`)
		fsp=${fsp:0:fs1-2} 
		#mb=$(bc($fsp/1024/1024))
		mb=$(echo "scale=6;$fsp/1024/1024" | bc)
		#echo "${mb}"
		#Credenciales para el servidor
uui=`mysql --host=${host} --user=${userx} --password=${pap} ${db_x} -A<< END 
Select ologus from slist where idserv = $id_serv;
END`
		uuic=$(echo `expr length "$uui"`)
		u_ui=${uui:7:uuic} 
ppi=`mysql --host=${host} --user=${userx} --password=${pap} ${db_x} -A<< END 
Select ologpas from slist where idserv = $id_serv;
END`
		ppic=$(echo `expr length "$ppi"`)
		p_pi=${ppi:8:ppic} 
		headx "#" "$u_ui or $p_pi"
		#Fin de credenciales
		#ingresa en la base de datos los datos de la carpeta de la implementacion
db_ins=`mysql --host=${host} --user=${userx} --password=${pap} ${db_x} -A<< END 
INSERT into HDSZ (ref,cp_nam,cp_siz) VALUES (' ','$imp',$mb);
END`
			if [ $? -eq 0 ] ; then
				headx "#" "Se ejecuto correctamente la consulta: $db_ins"
				headx "#" "$imp or $db_ins"
				headx "#" "$mb or $db_ins"
			else
				headx "&" "Error en la consulta: $db_ins"
				headx "#" "$imp or $db_ins"
				headx "#" "$mb or $db_ins"
			fi
	else
	headx "@" "No existe el directorio de la implementacion: $dtra "	
fi
#fin de funcion
}


function kronv()
{
	#Funcion para visualizar las actividades del cron con las bases de datos (Respaldos,Updates[CAPA,Desarrollo])
	#inicio de funcion
	#variables
#up1=$(echo `expr length "$db_na"`)
#up1=${db_na:5:up1} 
crp="RSPDBPRO@" #Identificador para respaldo de DB produccion
crcap="UPDCAP@" #Identificador para actualizacion de DB CAPA
crdes="UPDDES@" #Identificador para actualizacion de DB Desarrollo
headx "#" "Inicio de envio de datos a db para visualizar actividad de crontab"
crpr=$(echo `crontab -l | grep "$crp"`)
headx "@" "$crp"
headx "@" "$crpr"

#Fin de funcion	
}



function zzooxx()
{
#Funcion para 
#inicio de funcion
headx "#" "Funcion demo"	
#Fin de funcion	
}
function totops(){
	#inicio de funcion
vz1=$1
vz2=$2
vz3=$3
	headx "@" "1-> $vz1"
	 sleep 10
	headx "@" "2-> $vz2"
	 sleep 10
	headx "@" "3-> $vz3"
	
 #fin de funcion
}

function lamigra(){
#Inicio de funcion
#
#Actualizacion de base local con algun remoto
#P1=base_remota, P2=base_local, P3=ip_remota, P4=u_remota, P5=p_remoto, P6 u_local,P7 p_local
#
	    clear
#variables para respaldo remoto desarrollo
	    #dbremota
			db_remota=$1
		#dblocal (la que se va a actualizar)
			db_local=$2
		#ip de servidor remoto
			ip_rem=$3
		#directorio temp del -sql
			ddbr=${tmpcap}	
		#Usuario remoto
			u_urm=$4
		#Password remoto
			p_prm=$5
#fin de variables remotas
#Fecha de inicio de la actualizacion
			fini=$chh
#Variables locales 
		hoth="localhost"
		infhost=`ifconfig -a | grep 'addr'`
		#credenciales Desarrollo
		u_loc=$6
		p_loc=$7
		ronoff=$8
if [ -z "$ronoff"]  ; then
	ronoff="x"		
fi	
echo "x-"$ronoff
#Fin de variables locales desarrollo
		#Nombre del archivo a generar .sql 
		gsql="dbvolc_migra_"$db_remota"_$fini.sql"
		#Archivo Log
		ologdb=${ologdir}$db_remota"_updRem_Loc"$chh".html"
			#--creacion de carpeta olog para guardar log de respaldos
		mklogd
#Comprobacion de existencia de db en Mysql remoto
#dbseek host usuario password db_name
dbrm=$(dbseek $ip_rem $u_urm $p_prm $db_remota)
headx "#" "db remota: $dbrm"
if [ $dbrm -eq 0 ] ; then
#Comprobacion de existencia de db en Mysql local
#dbseek host usuario password db_name
 dblc=$(dbseek $hoth $u_loc $p_loc $db_local)
 headx "#" "db local: $dblc"
	if [ $dblc -eq 0 ] ; then
		#Todo OK con las db
		headx "@" "Base de datos a actualizar encontrada $db_local"
		if [ $ronoff == "R" ] ; then
			headx "@" "Se realizara respaldo"
			echo " []-->Se selecciono realizar respaldo antes de la actualizacion de db $db_local" >> $ologdb
			headx "@" "Respaldo en proceso"
			/utl/respaldo/bbck.sh cdb "$db_local"	
		else
			echo " --[]>No se mando parametro de respaldo $db_local" >> $ologdb
			headx "&" "No se envio el parametro de respaldo"
		fi	
		ret="16"
		headx "#" "Clonando Origen db: -> $db_remota a $db_local"
		echo "<br># Clonando Origen db: -> $db_remota a $db_local $chh<br>" >> $ologdb
		echo "<br># Iniciando Clonado db -> $db_remota #<br>" >> $ologdb
		headx "@" "Iniciando Clonado db -> $db_remota Generando $gsql"
		mysqldump -h $ip_rem -u $u_urm -p$p_prm $db_remota > $ddbr$gsql 2>/dev/null &
			pid=$! 
			i=0
			while kill -0 $pid 2>/dev/null
			do
				i=$(( (i+1) %5 ))
				printf "\rEspere....................${spin:$i:1}"
				sleep .1
			done
		if [ $? -eq 0 ] ; 
		then
			cd $ddbr
			if [ -f $gsql ];
			then
				echo " "
				echo "<br># Estructura y datos generados clonado completado $ddbr$gsql #<br>" >> $ologdb
				headx "@" "Estructura y datos generados clonado completado $ddbr$gsql"
				headx "@" "Generando clon... $db_local" 
				echo "<br># Generando clon... $db_local #<br>"  >> $ologdb
				mysql -u $u_loc -p$p_loc  $db_local < $gsql 2>/dev/null &
				pid=$!
				i=0
				while kill -0 $pid 2>/dev/null
				do
					i=$(( (i+1) %5 ))
					printf "\rEspere....................${spin:$i:1}"
					sleep .1
				done
					if [ $? -eq 0 ] ; 
					then
						echo " "
								echo "<br>Estructura y datos: $gsql clonado en: $db_local<br>" >> $ologdb
								headx "@" "Estructura y datos: $gsql clonado en: $db_local"
								if [ -f $dbfile ];
								then
									echo "<br>Buscando y borrando Estructura y datos: $gsql<br>" >> $ologdb
									rm -f $gsql
									echo "<br>Estructura y datos Borrados: $gsql<br>" >> $ologdb
									headx "@" "Clonado de db: $db_remota completado"
									headx "#" "Se creo $db_local verificar integridad el ologX"
									echo "<br>######################## Iniciando Log de base de datos ###########################<br>" >> $ologdb
									echo "Fecha de Clonado $chh de Base de datos $db_remota" >> $ologdb	
									echo "<br>############################### Fin de Clonado db: $db_remota ####### $chh<br>" >> $ologdb
									ret="25"										
								else
					
									headx "&" "No se encontro archivo .SQL para borrar: $gsql "
									echo "<br>No se encontro archivo .SQL para borrar: $gsql<br>" >> $ologdb
									ret="24"
								fi
					else
				  					echo "<br>Error al crear .SQL archivo no creado, no se procede con clon: $gsql<br>" >> $ologdb
				  					headx "&" "Error al crear .SQL archivo no creado, no se procede con clon: $$gsql"
				  					ret="24"
				  	fi
			else
		     	  echo "<br>Error no se encontro .SQL o error General: $gsql<br>" >> $ologdb
		     	  headx "&" "Error no se encontro .SQL o error General: $gsql"
		     	  ret="24"
			fi
		else
				  #no se encontro archivos .SQL antes de
				  echo "<br>Error al realizar el .SQL o error General: $gsql<br>" >> $ologdb
				  headx "&" "Error al realizar el .SQL o error General: $gsql"
		     	  ret="24"
		fi
	else
	#no se encontro db DES, se tiene que creear manual por seguridad.
	headx "&" "No se encuentra DB a Crear $db_local: favor de crearla en $hdes"
	echo "<br>No se encuentra DB a Crear $db_local: favor de crearla en $hdes<br>" >> $ologdb
	ret="24"  
    fi 
else
 #no se encontro db origen, verificar existencia o nombre de la db
headx "&" "No se encuentra DB a Clonar $db_remota: favor de verificar que existe en $hremo " 
echo "<br>No se encuentra DB a Clonar $db_remota: favor de verificar que existe en $hremo<br>" >> $ologdb
ret="24" 
fi
		cc=`cat $ologdb`	
  		#echo "paso : Var1 "$1"  var2 "$cc" var3 "${putres}" var4 "$ret" "
		#savedb erp log infadd cat
		ffin=`date +'%Y%m%d-%H%M%S'`
		savedb "Migracion de servidor $db_local a $db_remota" "$cc ->  de DBs de $ip_rem -> $infhost : " "Fecha de Inicio:$finicio<br>Fecha de Fin: $ffin<br>" "$ret"
#Fin de funcion
}

function tarft()
{
#inicio de funcion
#funcion para empaquetar y enviar
#empenv P1 identificador del archivo, P2 Directorio a comprimir,P3 limbo, P4 Subir(S,z) ,P5 Servidor remoto
#variables
lfile=$1 #nombre del archivo
dirx=$2 #directorio a comprimir
diry=$3 #directorio destino limbo
subir=$4 #Variable para metodo de envio 
srvr=$5 #ip de servidor remoto
#log
ologcomp=${ologdir}"tartaro_"$chh".inf" 
mklogd	
tarf="bk_"$lfile"_"${chh}".tar.gz" #Nombre del archivo a crear
if [ -d $dirx ] ; then
		clear
		headx "#" "Iniciando empaquetado"
		echo "<br>######################### Iniciando comprecion de carpeta ################# $chh<br>" >> $ologcomp
		headx "@" "Iniciando compresion de carpeta -> $derp"
		echo "<br>############# Iniciando Comprecion de respaldo $tarf ############ $chh<br>" >> $ologcomp
		headx "@" "Iniciando Comprecion de respaldo $tarf"
		dirz=$(echo `expr length "$dirx"`)
		raiz=${dirx:0:1} 
		dirx=${dirx:1:dirz} 
		cd  $raiz
		tar -c $dirx | gzip > $tarf 2>/dev/null &
				pid=$!
				i=0
				while kill -0 $pid 2>/dev/null
				do
					i=$(( (i+1) %5 ))
					printf "\rEspere....................${spin:$i:1}"
					sleep .1
				done
		#Subir a resguardo
		ls -lh $tarf >> $ologcomp
		if [ -f $tarf ] ; then
			echo ""
			headx "#" "Archivo $tarf encontrado"
				if [ "$subir" = "S" ] ; then
					  		headx "@" "Archivo a subir -> $tarf Opcion: FTP o SFTP" 
				  			putff "$tarf" $srvr
				else
				  			echo "<br><br>No se selecciono subir archivo <br> FTP" >> $ologcomp
				  			headx "&" "No se selecciono subir archivo FTP"
				fi
				  		
				if [ "$subir" = "z" ] ; then
							#putcot Archivo did[a,nau,dbr,codr] ipdservidor
					  		headx "@" "Archivo a subir -> $tarf Opcion: SSH"
				  			putcot "$tarf" $diry $srvr 
				else
				  			echo "<br><br>No se selecciono subir archivo SSH" >> $ologcomp
				  			headx "&" "No se selecciono subir archivo SSH"
				fi	
		else
			headx "#" "Archivo $tarf No encontrado error al subir codigo a respaldo"
		fi		
	else
		echo "<br>No, no existe; $dirx favor de verificar nombre de Directorio ·No se realizo Respaldo $chh<br>" >> $ologcomp	
		headx "&" "No, no existe; $dirx favor de verificar nombre de Directorio ·No se realizo Respaldo $chh"
		ret="7"
		savedb "<br>No, no existe; $dirx favor de verificar nombre de Directorio ·No se realizo Respaldo $chh<br> $1" "$cc" "${putres}" "$ret"
	fi
        headx "#" "Iniciando Grabando base de datos"
        if [ -f $ologcomp ] ; then
			 echo "->          Iniciando... "
			 cc=`cat $ologcomp`	
			 savedb "Respaldo Codigo: $1 Respaldos actuales de $1: $ctu" "$cc" "${putres}" "$ret"
			 echo "->          Guardado... "
 		else
		 	cc="->          Error General en archivo de log: $ologcd "
		 	savedb "No se encontro archivo log: $ologcd del erp: $1: $ctu" "$cc" "${putres}" "$ret"
		fi
	#fin de funcion
}

function termc()
{
	#Inicio de funcion
#-- termc [nombre de erp [DB o Codigo] [Linea de codigo] [Password Mysql root] []
#/utl/respaldo/bbck.sh delsec erpborrar erpdistribucion p0rtali70s
erpcom=$1
lin=$2
extc="_CAPA"
#-- Datos de coneccion local [Por seguridad no se cre remoto]
stermser="localhost"
teu_u="root"
pap_p=$3
#-- Directorio de documentos del cliente [Produccion y CAPA]
dirs[0]="/var/www/html/"$lin"/companies/"$erpcom
dirs[1]="/var/www/html/"$lin"/companies/"$erpcom$extc
dirs[2]="/var/www/html/"$lin"_CAPA/companies/"$erpcom
dirs[3]="/var/www/html/"$lin"_CAPA/companies/"$erpcom$extc
#-- Bases de datos
dirs[4]="/var/lib/mysql/"$erpcom
dirs[5]="/var/lib/mysql/"$erpcom$extc
cmdrm="DROP DATABASE "$erpcom
cmdrmc="DROP DATABASE "$erpcom$extc
#-- Dir respaldos
dirs[6]="/respaldo/data/"$lin"/companies/"$erpcom
dirs[7]="/respaldo/data/"$lin"/companies/"$erpcom$extc
#Filtrar con find por que estan todos los respaldos de todas las implementaciones
dirs[8]="/respaldo/data/db"
#-- Var LOGS
lowl="/var/www/html/BITDEL_$erpcom"_"$chh.bit"
#Comprobar que la base exista
#archivos temporales
echo "Inicio de bitacora de borrado $erpcom $chh"
#Comprobacion de Existencia de bases de datos
#dbseek host usuario password db_name
dbrmv=$(dbseek $stermser $teu_u $pap_p $erpcom)
headx "@" "Precione para continuar borrado de produccion" 
pausa
headx "#" "Validacion de bases de datos y borrado"
if [ $dbrmv -eq 0 ] ; then
	headx "@" "Base $erpcom si existe se procede a borrar" >> $lowl
uui=`mysql --host=$stermser --user=root --password=$pap_p $erpcom -A<< END 
$cmdrm
END` 2>/dev/null &
		pid=$!
		i=0
		while kill -0 $pid 2>/dev/null
			do
			i=$(( (i+1) %5 ))
			printf "\rEspere borrando base produccion: $item........${spin:$i:1}"
			sleep .1
			done
		echo " "
		if [ $? -eq 0 ] ; then
				headx "#" "Se ejecuto correctamente el borrado $erpcom" >> $lowl
			else
				headx "&" "Error en la consulta de borrado $erpcom" >> $lowl
			fi
else
	headx "&" "Base $erpcom no existe" >> $lowl
	pausa
fi
headx "@" "Precione para continuar con capa" pausa
dbrmv=$(dbseek $stermser $teu_u $pap_p $erpcom$extc)
if [ $dbrmv -eq 0 ] ; then
	headx "@" "Base $erpcom$extc si existe se procede a borrar" >> $lowl
uui=`mysql --host=$stermser --user=root --password=$pap_p $erpcom$extc -A<< END 
$cmdrmc
END` 2>/dev/null &
		pid=$!
		i=0
		while kill -0 $pid 2>/dev/null
			do
			i=$(( (i+1) %5 ))
			printf "\rEspere Borrando DB $item........${spin:$i:1}"
			sleep .1
		done
		echo " "
			if [ $? -eq 0 ] ; then
				headx "#" "Se ejecuto correctamente el borrado $erpcom$extc" >> $lowl
			else
				headx "&" "Error en la consulta de borrado $erpcom$extc" >> $lowl
			fi
else
	headx "&" "Base $erpcom$extc no existe"
	pausa
fi

headx "@" "Precione para continuar listado de directorios"
pausa
#Comprobacion de Existencia de directorios
headx "#" "Comprobando directorios"
ftmp="/utl/log/tldel$chh$erpcom.txt"
echo "" >> $ftmp
cat /dev/null > $ftmp
cont=0
for item in ${dirs[*]}
do
	if [ -d $item ] ; then
		headx "@" "D Si existe; $item" >> $lowl
		#Inicio de guardado de archivos para borrar con shred de manera segura
		#Llena ftmp con los datos de produccion
		if [ $cont -le 7 ] ; then
			find $item"/" -name "*" >> $ftmp 2>/dev/null &
			pid=$!
			i=0
			while kill -0 $pid 2>/dev/null
				do
				i=$(( (i+1) %5 ))
				printf "\rEspere creando lista $item........${spin:$i:1}"
				sleep .1
				done
		echo " "
		else
			find $item"/" -name "*"$erpcom"*" >> $ftmp 2>/dev/null &
			pid=$!
			i=0
			while kill -0 $pid 2>/dev/null
				do
				i=$(( (i+1) %5 ))
				printf "\rEspere creando lista Respaldos $item........${spin:$i:1}"
				sleep .1
				done
		echo " "
		fi		
	else
		headx "&" "Veridficar directorios" >> $lowl
		headx "@" "D Error; $item" >> $lowl
		sleep 2
	fi
cont=$((cont+1))
done
#Borrado de archivos y directorios
headx "@" "Precione para continuar borrado de directorios" 
pausa
if [ -f $ftmp ] ; then
	headx "@" "Borrando directorios" >> $lowl
	listf=`grep -v "#" $ftmp`
	for delz in $listf;
	do
		headx "&" "${delz}"
		shred -n 7 -v -z ${delz} >> $lowl 2>/dev/null &
			pid=$!
			i=0
			while kill -0 $pid  2>/dev/null
				do
				i=$(( (i+1) %5 ))
				printf "\rEspere eliminando -->shred<-- $item........${spin:$i:1}"
				sleep .1
				done
		echo " "
		remover=`echo rm -rf $delz` >> $lowl
		remfile=`echo rm -f $delz` >> $lowl
		remseg=`echo dd if=/dev/zero of=${delz}/../f1.txt bs=1024 count=1048576` >> $lowl
		remseg2=`echo rm –f ${delz}/../f1.txt` >> $lowl
		echo "Salida de directorios: $remover $remseg $remseg2" >> $lowl
	done
else
	headx "&" "No se encontro el archivo de lista" >> $lowl
fi
	#Fin de funcion 

}

function enviox()
{
#Inicio de funcion
ori=$1
des=$2
optr=$3
ab=$4
ac=$5
logss="/utl/log/essh_$chh.txt"
#obtencion de numero de servidores
clear
echo "[]---------}-------->>Envio de archivos a servidores $optr;$ab;$ac;$chh <<---------{-----------[]" >> $logss
headx "#" "Enviando archivos $ori a destino $des"
#comprobar si el directorio a actualizar existe
if [ -d $ori ] ; then
rex=`mysql --host=${ip_extser} --user=${u_uex} --password=${p_pex} ${db_extra} -A<< END
select count(*) from ${tb_servs};
END`
if [ $? -eq 0 ] ; then
		headx "@" "Se ejecuto correctamente"
		echo "Se ejecuto correctamente la consulta de base ${db_extra} "  >> $logss
	else
		headx "&" "Error en la consulta ${db_extra} "
		echo "[]---------}-------->>Error de consulta $optr;$ab;$ac;$chh <<---------{-----------[]" >> $logss
		exit 1
fi
#fin de obtencion de registros solo cuenta los renglones
#Recorre toda la tabla para extraer la coneccion a los servidores
regconc=$(echo `expr length "$rex"`)
rex=${rex:9:regconc}
#Si se ingresa la opcion uno solo se ejecuta uno de los rengroles tomando como id el inicio y el final no es por ID
	if [ "$optr" == "uno" ]; then
		g="$ab"
		rex="$ac"
	else
		g="1"
	fi
	while [ $g -le $rex ]
	do
#Extrae los datos del servidor en cuestion ip,passwordssh,puertossh
ipser=`mysql --host=${ip_extser} --user=${u_uex} --password=${p_pex} ${db_extra} -A<< END 
Select ip from ${tb_servs} where server_id = $g;
END`
	ipserc=$(echo `expr length "$ipser"`)
	ipser=${ipser:3:ipserc} 

pasw=`mysql --host=${ip_extser} --user=${u_uex} --password=${p_pex} ${db_extra} -A<< END 
Select password from ${tb_servs} where server_id = $g;
END`
	paswc=$(echo `expr length "$pasw"`)
	pasw=${pasw:9:paswc} 

sshp=`mysql --host=${ip_extser} --user=${u_uex} --password=${p_pex} ${db_extra} -A<< END 
Select ssh_port from ${tb_servs} where server_id = $g;
END`
	sshpc=$(echo `expr length "$sshp"`)
	sshp=${sshp:9:sshpc} 
#inicia la copia de los archivos con rsync
	headx "@" "1;$ipser   2;$pasw   3;$sshp"
	headx "@" "Sincronizando $ori en servidor $ipser al directorio $des"
	echo "[]---------}-------->>Datos de envio; 1;$ipser   2;$pasw   3;$sshp <<---------{-----------[]" >> $logss
	cf=$(sshpass -p ${pasw} rsync  --partial --progress -ravuWh -e "ssh -p ${sshp}" ${ori} root@$ipser:$des)
		if [ ${PIPESTATUS[0]} -ne 0 ]; then
			echo "[]---------}-------->>TTY Error; ${cf}<<---------{-----------[]" >> $logss
			headx "@" "Error while"
		else
			echo "[]---------}-------->>TTY OK; ${cf}<<---------{-----------[]" >> $logss
			headx "@" "Successful"
		fi
		echo "$cf"
	g=$(( $g + 1 ))
	done
echo "[]---------}-------->> Fin de reg. $chh <<---------{-----------[]" >> $logss
else 
#si no se encuentra el directorio
echo "[]---------}-------->> Error no se encontro el directorio $chh <<---------{-----------[]" >> $logss
headx "&" "Error de directorio"
fi
	#Fin de funcion
}


###########################################################Comprobacion de que el Mysql slave esta activo
function slave()
{
## Tool to unstick MySQL Replicators.
## 
 to run from cron once a minute.

# */1 * * * * /usr/local/bin/whipSlave.mysql.sh > /dev/null 2>&1

# Last updated: MM/DD/YYYY

COMMANDS="mysql grep awk logger"

export PATH='/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin'

for i in $COMMANDS
do
        X=`echo $i | tr '[a-z]' '[A-Z]'`
        export $X=`type -p $i`
done

# Define variables
USERNAME=dbuser
PASSWORD=password

# Define Functions
## Obtain MwSQL slave server status
function SLAVE()
{
        STATUS=`$MYSQL -u $USERNAME -p$PASSWORD -e \
                "SHOW SLAVE STATUS \G" |
                $GREP Seconds_Behind_Master |
                $AWK '{print $2}'`
}

## Skip errors
function UNSTICK()
{
        $MYSQL -u $USERNAME -p$PASSWORD -e \
                "STOP SLAVE; SET GLOBAL SQL_SLAVE_SKIP_COUNTER = 1; START SLAVE;"
        sleep 5
        # Check everything again
        CHECK
}

## Decide what to do...
function CHECK()
{
        # Obtain status
        SLAVE
        if [ $STATUS = NULL ]
        then
                # I think the replicator is broken
                echo "MySQL Slave database is not replicating. Fixing..." | $LOGGER
                UNSTICK
        else
                # Everything should be fine
                echo "MySQL Slave is $STATUS seconds behind its Master." | $LOGGER
        fi
}

## Are we running?
function ALIVE()
{
        UP=`$MYSQL -u $USERNAME -p$PASSWORD -e \
                "SHOW SLAVE STATUS \G" |
                $GREP Slave_IO_Running |
                $AWK '{print $2}'`

        if [ $UP = Yes ]
        then
                # Let's check if everything is good, then...
                CHECK
        else
                # Uh oh...let's not do anything.
                echo "MySQL Slave IO is not running!" | $LOGGER
                exit 1
        fi
}

# How is everything?
ALIVE

#EoF
exit 0
	
}
#[]--------------------------------------------------------------  Main () --------------------------------------------------------------[]
#--Variables de entrada --Incluir directorio final ERP 	
case $1 in
	cod)
	 #Entrada de respaldo completo rcodcomp (directorio erp) 
	 rcodcomp $2 $3 $4
	 exit 3
	 ;;
	manto)
	 #Entrada de mantenimiento de BD 
	 mant_o $2 $3 $4 $5 $6
	 exit 3
	 ;; 
	cdel) 
	 #Entrada de borrado de respaldos (directorio erp) 
	  rcodel  $2
	  exit 3
	 ;; 
	 cdb)
	 #Entrada de respaldo completo Base de datos (directorio erp) 
	  rdb $2 $3 $4 $5 $6
	  exit 3
	 ;; 
	 dbdl)
	 #Entrada de borrar respaldos de Base de datos (directorio erp) 
	  rdeldb $2
	  exit 3
	 ;; 
	  agente)
	  #Grabar logs en Agenda
	  agentex $2 $3 $4 $5
	  exit 3
	 ;; 
	 siri) 
	 #Revisa el estado del servicio mencionado en parametro 
	  sirix  $2 $3
	  exit 3
	 ;; 
	  conejo) 
	 #Revisa el estado del servicio mencionado en parametro 
	  blanco  $2 $3
	  exit 3
	 ;; 
	 datw)
	 #Ejecuta SQL para dataware desde base de datos con el identificador  
	  dwtf $2 $3
	  exit 3
	 ;;
	 hpx)
	 #Ejecuta PHP desde base de datos con el identificador  
	  phX $2
	  exit 3
	 ;;
	 ucap)
	 #Ejecuta actualiza base de capa en erp
	  upd_capa $2 $3
	  exit 3
	 ;;
	 udes)
	 #Ejecuta actualiza bases de desarrollo
	 #P1=base_remota, P2=Host_remoto, P3=base_local, P4=u_remoto, P5=p_remoto
	  upd_des $2 $3 $4 $5 $6
	  exit 3
	 ;;
	 runq)
	 #Graficas de DB Demo
	  dgraph $2 $3
	  exit 3
	 ;;
	 rback)
	 #restaura una base de datos de respaldo .gz o .sql
	 antz $2 $3 $4 $5 $6 $7
	 #antz respaldo base_destino tipo de respaldo host usuario password
	 exit 3
	 ;; 
	 hdc)
	 #Graba espacio ocupado por archivos de pcompañias
	 	hdsp $2 $3
	  exit 3
	 ;;
	 cotred)
	 #Conejos Comprueba conecciones de activeMQ y la red de Tierra
	 	conejotes $2 $3
	 exit 3
	 ;;
	 toto)
	 	#Parametro para realizar pruebas de funciones directas
	 	totops $2 $3 $4
	  exit 3
	 ;;
	 migdb)
	 #Ejecuta actualiza bases de desarrollo
	 #P1=base_remota, P2=base_local, P3=ip_remota, P4=u_remota, P5=p_remoto, P6 u_local,P7 p_local,P8 R [Respaldo de base local]
	  lamigra $2 $3 $4 $5 $6 $7 $8 $9
	  exit 3
	  ;;
	 kronv)
	 	#Backend del panel de respaldos
	 	kronv
	  exit 3
	 ;;
	  empenv)
	 	#Modo de uso
	 	#empenv P1 identificador del archivo, P2 Directorio a comprimir,P3 limbo, P4 Subir(S,z) ,P5 Servidor remoto
	 	tarft $2 $3 $4 $5 $6 $7
	  exit 3
	 ;;	 
	 delsec)
	 	#Modo de uso
	 	#Borrado seguro de empresas termc
	 	termc $2 $3 $4 $5 $6 $7
	  exit 3
	 ;;	 
	 copicon)
	 	#Modo de uso
	 	#Registro de archivos modificados
	 	#Parametros 1=Directorio origen 2=Servidor(IP) 3=Destino 4=Exclusion de archivos o patron 
	 	sync_code_mig $2 $3 $4 $5 $6 $7
	  exit 3
	 ;;
	 tomtom)
	 	#Modo de uso
	 	#Registro de archivos modificados
	 	#Parametros 1=Directorio origen 2=Servidor(IP) 3=Destino 4=Exclusion de archivos o patron 
	 	tomtest $2 $3 $4 $5 $6 $7
	  exit 3
	 ;;	
	
	 txsend)
	 	#Modo de uso
	 	#Borrado seguro de empresas termc
	 	enviox $2 $3 $4 $5 $6 $7
	  exit 3
	 ;;	
	 sshot)
	 	#Modo de uso
	 	#Registro de archivos modificados
	 	snaps $2 $3 $4 $5 $6 $7
	  exit 3
	 ;;	
	 gum)
	 	#Modo de uso
	 	#Registro de archivos modificados
	 	ball $2 $3 $4 $5 $6 $7
	  exit 3
	 ;;	
	  ayuda)
	 	#Modo de uso
	 	use
	  exit 3
	 ;;
	 *)
	 #Entrada de Modo de uso
	 	use 
      exit 3 
	  ;;

esac
