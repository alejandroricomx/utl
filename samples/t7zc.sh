#!/bin/sh
#
#Elaborado por Alejandro Rico E. 2017
#///zzzzz
#set -xv
chh=`date +'%Y%m%d-%H%M%S'`
lstdir="/var/www/html/ap_grp/"
dest7="/respaldo/"
fdest="test.7z"
book="change.txt"
fdec=$dest7$fdest
fdeci=$dest7$fdest"_"$chh
fill=$lstdir$book
if [ -f $fdec ] ; then
	find $lstdir -type f -mtime -1 > $fill
	for line in $(cat $fill); 	
	do 
		7z u $fdeci $line  
	done
else
	7z u $fdec $lstdir
fi

